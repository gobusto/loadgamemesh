/**
@file
@brief Functions and structures used when exporting various things.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "export.h"

/*
[PUBLIC] Write a single-precision floating point value to a file.
*/

lgm_bool_t meshPutFloat32(FILE *out, float value)
{
  if (!out) { return LGM_FALSE; }
  fwrite(&value, 4, 1, out);
  return LGM_TRUE;
}

/*
[PUBLIC] Write an unsigned, little-endian INT16 value to a file.
*/

lgm_bool_t meshPutUI16_LE(FILE *out, unsigned short value)
{
  if (!out) { return LGM_FALSE; }
  fputc((value >> 0) & 0xFF, out);
  fputc((value >> 8) & 0xFF, out);
  return LGM_TRUE;
}

/*
[PUBLIC] Write an unsigned, little-endian INT32 value to a file.
*/

lgm_bool_t meshPutUI32_LE(FILE *out, unsigned long value)
{
  if (!out) { return LGM_FALSE; }
  fputc((value >> 0 ) & 0xFF, out);
  fputc((value >> 8 ) & 0xFF, out);
  fputc((value >> 16) & 0xFF, out);
  fputc((value >> 24) & 0xFF, out);
  return LGM_TRUE;
}

/*
[PUBLIC] Write a signed, little-endian INT16 value to a file.
*/

lgm_bool_t meshPutSI16_LE(FILE *out, short value)
{
  return meshPutSI16_LE(out, *((unsigned short*)&value));
}

/*
[PUBLIC] Write a signed, little-endian INT32 value to a file.
*/

lgm_bool_t meshPutSI32_LE(FILE *out, long value)
{
  return meshPutSI32_LE(out, *((unsigned long*)&value));
}
