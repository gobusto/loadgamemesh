/**
@file
@brief Handles Stereo-Lithographic .STL files.

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "export.h"

/*
[PUBLIC] Export a mesh in STL format.
*/

lgm_bool_t meshSaveSTL(const mesh_s *mesh, lgm_index_t frame, const char *file_name)
{
  FILE *out;
  lgm_index_t g, t, i;

  if (!mesh || !file_name || frame < 0 || frame >= mesh->num_frames) { return LGM_FALSE; }
  out = fopen(file_name, "wb");
  if (!out) { return LGM_FALSE; }

  for (i = 0; i < 80; ++i) { fputc(0, out); }
  meshPutUI32_LE(out, meshTriangleCount(mesh));

  for (g = 0; g < mesh->num_groups; ++g)
  {
    const mesh_group_s *s = mesh->group[g];
    if (!s) { continue; }

    for (t = 0; t < s->num_tris; ++t)
    {
      /* Face normal. Set to [0, 0, 0] so that it's calculated when loaded. */
      for (i = 0; i < 3 * 4; ++i) { fputc(0, out); }

      /* Triangle vertex positions. Notice that these are in reverse order. */
      for (i = 0; i < 3; ++i)
      { fwrite(&mesh->vertex[frame][s->tri[t]->v_id[2-i] * 3], 4, 3, out); }

      /* Flags. These will generally always be zero. */
      fputc(0, out);
      fputc(0, out);
    }
  }

  fclose(out);
  return LGM_TRUE;
}
