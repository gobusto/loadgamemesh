/**
@file
@brief Handles export of MilkShape3D MS3D files.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "export.h"

/*
[PUBLIC] Export a mesh as a MilkShape3D MS3D file.
*/

lgm_bool_t meshSaveMS3D(const mesh_s *mesh, lgm_index_t frame, const char *file_name)
{
  FILE *out;
  lgm_index_t g, i;
  lgm_count_t num_tris;

  if (!mesh || !file_name || frame < 0 || frame >= mesh->num_frames) { return LGM_FALSE; }

  /* MilkShape has explicit limits on the total number of things it can have: */
  if      (mesh->num_verts         > 65534) return LGM_FALSE;
  else if (meshTriangleCount(mesh) > 65534) return LGM_FALSE;
  else if (mesh->num_groups        > 255  ) return LGM_FALSE;
  else if (mesh->mtrl     && mesh->mtrl->num_items    > 128) return LGM_FALSE;
  else if (mesh->tag_list && mesh->tag_list->num_tags > 128) return LGM_FALSE;

  out = fopen(file_name, "wb");
  if (!out) { return LGM_FALSE; }

  fwrite("MS3D000000", 1, 10, out);
  meshPutUI32_LE(out, 4);

  meshPutUI16_LE(out, (unsigned short)mesh->num_verts);
  for (i = 0; i < mesh->num_verts; ++i)
  {
    fputc(0, out);  /* Flags */
    meshPutFloat32(out, mesh->vertex[frame][(i * 3) + 0]);
    meshPutFloat32(out, mesh->vertex[frame][(i * 3) + 1]);
    meshPutFloat32(out, mesh->vertex[frame][(i * 3) + 2]);
    fputc(-1, out);  /* TODO: Bone ID */
    fputc(0, out);  /* TODO: Reference count */
  }

  meshPutUI16_LE(out, meshTriangleCount(mesh));
  for (g = 0; g < mesh->num_groups; ++g)
  {
    const mesh_group_s *group = mesh->group[g];
    if (!group) { continue; }
    for (i = 0; i < group->num_tris; ++i)
    {
      const mesh_tri_s *tri = group->tri[i];
      int v;

      meshPutUI16_LE(out, 0);  /* Flags */

      for (v = 2; v >= 0; --v) { meshPutUI16_LE(out, tri->v_id[v]); }
      for (v = 2; v >= 0; --v)
      {
        meshPutFloat32(out, mesh->normal[frame][(tri->n_id[v] * 3) + 0]);
        meshPutFloat32(out, mesh->normal[frame][(tri->n_id[v] * 3) + 1]);
        meshPutFloat32(out, mesh->normal[frame][(tri->n_id[v] * 3) + 2]);
      }
      for (v = 2; v >= 0; --v) { meshPutFloat32(out, 0.0 + mesh->texcoord[(tri->t_id[v] * 2) + 0]); }
      for (v = 2; v >= 0; --v) { meshPutFloat32(out, 1.0 - mesh->texcoord[(tri->t_id[v] * 2) + 1]); }

      fputc((g % 32) + 1, out);  /* Smoothing group (1-32). */
      fputc(g, out);  /* Group. */
    }
  }

  meshPutUI16_LE(out, mesh->num_groups);
  for (num_tris = 0, g = 0; g < mesh->num_groups; ++g)
  {
    const mesh_group_s *group = mesh->group[g];
    if (!group) { continue; }

    fputc(0, out);  /* Flags */

    if (group->name)
    {
      lgm_count_t len = (lgm_count_t)strlen(group->name);
      for (i = 0; i < 32; ++i)
        fputc(i < len && i != 31 ? group->name[i] : 0, out);
    }
    else for (i = 0; i < 32; ++i) { fputc(0, out); }

    meshPutUI16_LE(out, group->num_tris);
    for (i = 0; i < group->num_tris; ++i, ++num_tris) { meshPutUI16_LE(out, i); }

    /* Misfit Model 3D complains if the material index is out-of-range: */
    if (mesh->mtrl && group->mat_id < mesh->mtrl->num_items)
      fputc(group->mat_id, out);
    else
      fputc(-1, out);
  }

  meshPutUI16_LE(out, mesh->mtrl ? mesh->mtrl->num_items : 0);
  for (i = 0; mesh->mtrl && i < mesh->mtrl->num_items; ++i)
  {
    const mtrl_s *mtrl = mesh->mtrl->item[i];
    int a;

    /* Note: Relies on this being at least 31 bytes long... */
    fwrite(mtrl->name, 1, 31, out); fputc(0, out);

    for (a = 0 ; a < 4; ++a) { meshPutFloat32(out, mtrl->ambient[a]); }
    for (a = 0 ; a < 4; ++a) { meshPutFloat32(out, mtrl->diffuse[a]); }
    for (a = 0 ; a < 4; ++a) { meshPutFloat32(out, mtrl->specular[a]); }
    for (a = 0 ; a < 4; ++a) { meshPutFloat32(out, mtrl->emission[a]); }
    meshPutFloat32(out, mtrl->shininess);
    meshPutFloat32(out, mtrl->alpha);

    fputc(0, out);  /* Mode; unused. */

    /* Note: Relies on these being at least 127 bytes long... */
    fwrite(mtrl->diffusemap, 1, 127, out); fputc(0, out);
    fwrite(mtrl->alphamap, 1, 127, out); fputc(0, out);
  }

  meshPutFloat32(out, 24);  /* Anmation FPS.  */
  meshPutFloat32(out, 0);   /* Current frame. */
  meshPutUI32_LE(out, 1);   /* Total frames.  */

  meshPutUI16_LE(out, 0); /* TODO: Joints */

  fclose(out);
  return LGM_TRUE;
}
