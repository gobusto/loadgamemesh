/**
@file
@brief Various functions related to importing meshes.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "import.h"

/**
@brief Generate "smooth" normals for a mesh.

NOTE: If meshCreateBuffers() has been called before this is called, they will
need to be deleted and re-created to ensure that they're the right size.

@todo Doesn't (yet) normalise the generated vectors.

@todo Perhaps also add another function for flat-face normals?

@param mesh The mesh to generate normals for.
@return Zero on success, or non-zero on failure.
*/

static int meshGenerateNormals(mesh_s *mesh)
{

  mesh_group_s *grp = NULL;

  float vecA[3];
  float vecB[3];

  short fail = 0;

  long f, g, t, i;

  /* Ensure that the mesh exists and does not already have normal data. */

  if (!mesh || mesh->normal) { return 1; }

  /* Allocate memory. */

  mesh->normal = (float**)malloc(sizeof(float*) * mesh->num_frames);

  if (!mesh->normal) { return -2; }

  mesh->num_norms = mesh->num_verts;

  /* Loop through all frames of animation. */

  for (f = 0; f < mesh->num_frames; f++)
  {

    /* Allocate memory. */

    mesh->normal[f] = (float*)malloc(sizeof(float) * mesh->num_norms * 3);

    if (mesh->normal[f])
    {

      memset(mesh->normal[f], 0, sizeof(float) * mesh->num_norms * 3);

      /* Loop through all triangles. */

      for (g = 0; g < mesh->num_groups; g++)
      {

        grp = mesh->group[g];
        if (!grp) { continue; }

        /* Generate cross-products for each triangle in this frame. */

        for (t = 0; t < grp->num_tris; t++)
        {

          for (i = 0; i < 3; i++)
          {

            grp->tri[t]->n_id[i] = grp->tri[t]->v_id[i];

            vecA[i] = mesh->vertex[f][(grp->tri[t]->v_id[1]*3)+i]
                    - mesh->vertex[f][(grp->tri[t]->v_id[0]*3)+i];
            vecB[i] = mesh->vertex[f][(grp->tri[t]->v_id[2]*3)+i]
                    - mesh->vertex[f][(grp->tri[t]->v_id[0]*3)+i];

          }

          for (i = 0; i < 3; i++)
          {

            mesh->normal[f][(grp->tri[t]->n_id[i]*3)+0] -=
                                  vecA[1]*vecB[2] - vecA[2]*vecB[1];
            mesh->normal[f][(grp->tri[t]->n_id[i]*3)+1] -=
                                  vecA[2]*vecB[0] - vecA[0]*vecB[2];
            mesh->normal[f][(grp->tri[t]->n_id[i]*3)+2] -=
                                  vecA[0]*vecB[1] - vecA[1]*vecB[0];

          }

        }

      }

    } else { fail = 1; }

  }

  /* Check failure flag. */

  if (fail)
  {

    /* TODO: Free the whole normals array again... */

  }

  return 0;

}

/**
@brief Re-calculate the AABB information of a mesh.

This only re-calculates the base AABB data; skeletal AABB data is not updated.

@param mesh The mesh structure to update.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

static lgm_bool_t meshCalculateAABB(mesh_s *mesh)
{
  lgm_index_t v;

  if (!mesh) { return LGM_FALSE; }

  for (v = 0; v < mesh->num_verts; ++v)
  {
    int i;
    for (i = 0; i < 3; ++i)
    {
      const float p = mesh->vertex[0][(v * 3) + i];
      if (v == 0 || mesh->xyzmin[i] > p) { mesh->xyzmin[i] = p; }
      if (v == 0 || mesh->xyzmax[i] < p) { mesh->xyzmax[i] = p; }
    }
  }

  return LGM_TRUE;
}

/**
@brief Re-calculate the AABB information of a mesh skeleton.

@todo Explain more here...

@param mesh The mesh structure to update.
@return Zero on success, or non-zero on error.
*/

static int meshCalculateTagAABB(mesh_s *mesh)
{
  long t, v, axis, got_one;

  /* The mesh must associate vertices with tags in order for this to work... */
  if (!mesh || !mesh->vert_tag) { return -2; }

  for (t = 0; t < mesh->tag_list->num_tags; t++)
  {
    for (axis = 0; axis < 3; axis++)
    {
      mesh->tag_list->tag[t]->xyzmin[axis] = mesh->tag_list->tag[t]->position[axis];
      mesh->tag_list->tag[t]->xyzmax[axis] = mesh->tag_list->tag[t]->position[axis];

      /* Try to obtain the min/max vertex positions associated with the tag. */
      for (got_one = 0, v = 0; v < mesh->num_verts; v++)
      {
        float value = mesh->vertex[0][(v*3) + axis];

        if (mesh->vert_tag[v] != t) { continue; }

        if (!got_one || value < mesh->tag_list->tag[t]->xyzmin[axis]) { mesh->tag_list->tag[t]->xyzmin[axis] = value; }
        if (!got_one || value > mesh->tag_list->tag[t]->xyzmax[axis]) { mesh->tag_list->tag[t]->xyzmax[axis] = value; }

        got_one = 1;
      }
    }
  }

  return 0;
}

/*
[PUBLIC] Load a mesh from string.
*/

mesh_s *meshLoadText(const char *text)
{
  mesh_s *mesh = meshLoadMD5(text);

  if (!mesh) { mesh = meshLoadOBJ(text); }
  if (!mesh) { mesh = meshLoadOFF(text); }

  if (mesh)
  {
    meshGenerateNormals(mesh);
    meshCalculateAABB(mesh);
    meshCalculateTagAABB(mesh);
    mtrlUnixPaths(mesh->mtrl);
  }

  return mesh;
}

/*
[PUBLIC] Load a mesh from a data buffer.
*/

mesh_s *meshLoadData(size_t length, const unsigned char *data)
{
  mesh_s *mesh = meshLoad3DS(length, data);

  if (!mesh) { mesh = meshLoadBIN(length, data); }
  if (!mesh) { mesh = meshLoadBSP(length, data); }
  if (!mesh) { mesh = meshLoadLWO(length, data); }
  if (!mesh) { mesh = meshLoadMD2(length, data); }
  if (!mesh) { mesh = meshLoadMD3(length, data); }
  if (!mesh) { mesh = meshLoadMDL(length, data); }
  if (!mesh) { mesh = meshLoadMM3D(length, data); }
  if (!mesh) { mesh = meshLoadMS3D(length, data); }
  if (!mesh) { mesh = meshLoadPMD(length, data); }
  if (!mesh) { mesh = meshLoadRFC(length, data); }
  if (!mesh) { mesh = meshLoadRFL(length, data); }
  if (!mesh) { mesh = meshLoadSTL(length, data); }

  if (mesh)
  {
    meshGenerateNormals(mesh);
    meshCalculateAABB(mesh);
    meshCalculateTagAABB(mesh);
    mtrlUnixPaths(mesh->mtrl);
  }

  return mesh;
}

/*
[PUBLIC] Load a mesh mesh from a file.
*/

mesh_s *meshLoadFile(const char *file_name)
{
  mesh_s *result;
  unsigned char *data;
  long length = 0;

  data = txtLoadFile(file_name, &length);

  result = meshLoadData((size_t)length, data);
  if (!result) { result = meshLoadText((const char*)data); }

  free(data);
  return result;
}
