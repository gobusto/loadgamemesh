/**
@file
@brief Handles Lightwave .LWO files.

<http://en.wikipedia.org/wiki/Interchange_File_Format>

<http://paulbourke.net/dataformats/lightwave/>

<http://web.archive.org/web/20010423140254/http://members.home.net/erniew2/lwsdk/docs/filefmts/lwo2.html>

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* LWO 4CCs. Usually given as text, but defined in hex for switch statements. */

#define LWO_VERSION_1 0x4C574F42  /**< @brief LWO: "LWOB" FORM type of v1. */
#define LWO_VERSION_2 0x4C574F32  /**< @brief LWO: "LWO2" FORM type of v2. */

/* Python code: print("0x"+"".join(hex(ord(x)).upper()[2:] for x in "ABCD")) */

#define ID_LWO_MAIN     0x464F524D  /**< @brief LWO: "FORM" Root IFF node. */
#define ID_LWO_VERTICES 0x504E5453  /**< @brief LWO: "PNTS" Vertices data. */
#define ID_LWO_POLYGONS 0x504F4C53  /**< @brief LWO: "POLS" Polygons data. */
#define ID_LWO_VMAP     0x564D4150  /**< @brief LWO: "VMAP" Vertex params. */

#define LWO_POLY_FACE   0x46414345  /**< @brief LWO: "FACE" Face data type. */

#define LWO_VMAP_COORDS 0x54585556  /**< @brief LWO: "TXUV" Texcoords data. */

/**
@brief The internal structure used for parsing text-based formats.

This is used to keep track of the current "parser state" through multiple calls
to per-line callback functions which would otherwise have to rely on statically
allocated variables. Since it is possible for a multi-threaded program to parse
multiple files at the same time, static variables are not an ideal solution.
*/

typedef struct
{

  unsigned long flags;  /**< @brief LWO chunk ID. */

  mesh_s *mesh; /**< @brief The mesh being created/constructed. */

  long num_tris;    /**< @brief Number of triangles [in total/read so far]. */

} lwo_parse_s;

/**
@brief Handle an LWO chunk.

Work-in-progress.

@param state writeme.
@param length The total size of the data[] array.
@param data An array of raw byte values.
@return Zero on success, or non-zero on failure.
*/

static int meshLoadLWO_chunk(lwo_parse_s *state, unsigned long length, const unsigned char *data)
{
  unsigned long chunk_size, offset;
  unsigned long num_items, i;

  if (!state || length < 8 || !data) { return 666; }

  /* The first 4 bytes of the main FORM chunk identify the IFF data type. */

  offset = 0;

  if (state->flags == ID_LWO_MAIN)
  {

    if      (binGetUI32_BE(&data[offset]) == LWO_VERSION_2) { }
/*    else if (binGetUI32_BE(&data[offset]) == LWO_VERSION_1) { } */
    else                                                    { return 666; }

    offset = 4;

  }

  /* Loop through each sub-chunk within the current chunk. */

  for (; offset <= length - 8; offset += chunk_size + (chunk_size % 2))
  {

    chunk_size = 8 + binGetUI32_BE(&data[offset + 4]);
    if (chunk_size < 8 || chunk_size > length-offset) { return 666; }

    switch (binGetUI32_BE(&data[offset]))
    {

      case ID_LWO_VERTICES:
        if (state->flags != ID_LWO_MAIN) { return 666; }
        fprintf(stderr, "[LWO] %lu vertices found.\n", (chunk_size-8)/12);
      break;

      case ID_LWO_POLYGONS:
        if (state->flags != ID_LWO_MAIN) { return 666; }

        if (binGetUI32_BE(&data[offset+8]) == LWO_POLY_FACE)
        {

          /* NOTE: The highest 6 bits are flags, and should be ignored! */

          for (i = 12; i <= chunk_size - 8; i += 2 + (num_items*2))
          {
            num_items = binGetUI16_BE(&data[offset+i]) & 1023;
            state->num_tris += num_items - 2; /* 3 verts = 1 tri, 4 = 2 tris. */
          }

          fprintf(stderr, "[LWO] %lu triangles found.\n", state->num_tris);

        }

      break;

      default:
/*
        fprintf(stderr, "[DEBUG] Unknown LWO Chunk ID: %c%c%c%c\n",
            (int)(binGetUI32_BE(&data[offset]) >> 24) & 0xFF,
            (int)(binGetUI32_BE(&data[offset]) >> 16) & 0xFF,
            (int)(binGetUI32_BE(&data[offset]) >>  8) & 0xFF,
            (int)(binGetUI32_BE(&data[offset]) >>  0) & 0xFF);
*/
      break;

    }

  }

  /* No problems found; report success. */
  return 0;
}

/*
[PUBLIC] Load a mesh from a Lightwave .LWO data buffer.
*/

mesh_s *meshLoadLWO(size_t length, const unsigned char *data)
{
  lwo_parse_s state;
  unsigned long data_size = 0;

  if      (length <= 8 || !data)               { return NULL; }
  else if (binGetUI32_BE(data) != ID_LWO_MAIN) { return NULL; }

  data_size = 8 + binGetUI32_BE(&data[4]);
  if (data_size > (unsigned long)length) { return NULL; }

  memset(&state, 0, sizeof(lwo_parse_s));
  state.flags = ID_LWO_MAIN;

  if (meshLoadLWO_chunk(&state, data_size-8, &data[8]) != 0)
  { state.mesh = meshDelete(state.mesh); }

  return state.mesh;
}
