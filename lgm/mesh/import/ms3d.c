/**
@file
@brief Handles MilkShape3D .MS3D files.

<https://web.archive.org/web/20120206055629/http://chumbalum.swissquake.ch/ms3d/ms3dspec.txt>

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* MilkShape MS3D constants. */

#define MS3D_SIZE_VERT  15  /**< @brief MS3D: Size of a single vertex.      */
#define MS3D_SIZE_FACE  70  /**< @brief MS3D: Size of a single face.        */
#define MS3D_SIZE_GROUP 36  /**< @brief MS3D: BASIC size of a single group. */
#define MS3D_SIZE_MTRL  361 /**< @brief MS3D: Size of a single material.    */
#define MS3D_SIZE_BONE  93  /**< @brief MS3D: BASIC size of a single joint. */

#define MS3D_SIZE_ANIM_ROTATION 16  /**< @brief MS3D: Rotation frame size. */
#define MS3D_SIZE_ANIM_POSITION 16  /**< @brief MS3D: Position frame size. */

/*
[PUBLIC] Load a mesh from a Milkshape 3D .MS3D data buffer.
*/

mesh_s *meshLoadMS3D(size_t length, const unsigned char *data)
{

  mesh_s *mesh = NULL;

  long local_faces, num_tris,  num_groups,  num_mtrls,  num_verts, num_joints;
  long total_faces, offs_tris, offs_groups, offs_mtrls, offs, offs_joints, t, a, i, j;

  /* Ensure that the buffer is a valid MS3D file. */

  if (length < 36 || !data) { return NULL; }

  i = binGetUI32_LE(&data[10]);

  if (memcmp(data, "MS3D000000", 10) || i < 3 || i > 4) { return NULL; }

  /* Get the number of vertices. */

  offs = 14;

  num_verts = binGetUI16_LE(&data[offs]);
  offs += 2;
  offs += MS3D_SIZE_VERT * num_verts;

  /* Get the number of triangles and their offset. */

  num_tris = binGetUI16_LE(&data[offs]);
  offs += 2;
  offs_tris = offs;
  offs += MS3D_SIZE_FACE * num_tris;

  /* Get the number of groups and their offset. NOTE: Group size varies. */

  num_groups = binGetUI16_LE(&data[offs]);
  offs += 2;
  offs_groups = offs;

  for (i = 0; i < num_groups; i++)
  { offs += MS3D_SIZE_GROUP + (binGetUI16_LE(&data[offs+33]) * 2); }

  /* Get the number of materials and their offset. */

  num_mtrls = binGetUI16_LE(&data[offs]);
  offs += 2;
  offs_mtrls = offs;
  offs += MS3D_SIZE_MTRL * num_mtrls;

  /* Skip some skeletal animation data. */

  offs += 12;

  /* Get the number of joints and their offset. */

  num_joints = binGetUI16_LE(&data[offs]);
  offs += 2;
  offs_joints = offs;

  /* Allocate a mesh structure (Normals and Coords are stored in triangles). */

  mesh = meshCreate(1, num_verts, num_tris * 3, num_tris * 3, num_groups);

  if (!mesh) { return NULL; }

  mesh->mtrl = mtrlCreateGroup(num_mtrls);

  /* Create a vertex -> joint association array. */

  mesh->vert_tag = (long*)malloc(sizeof(long) * num_verts);

  /* Load vertices. */

  for (offs = 16, i = 0; i < num_verts; i++, offs += MS3D_SIZE_VERT)
  {
    memcpy(&mesh->vertex[0][i*3], &data[offs + 1], sizeof(float) * 3);
    if (mesh->vert_tag) { mesh->vert_tag[i] = (signed char)data[offs + 13]; }
  }

  /* Load groups. */

  offs        = offs_groups;
  total_faces = 0;

  for (i = 0; i < num_groups; i++)
  {

    /* Get number of triangles (It's after the flags byte and name string). */

    local_faces = binGetUI16_LE(&data[offs+33]);

    /* Create group. */

    mesh->group[i] = meshGroupCreate((char*)&data[offs+1],
                                  data[offs+MS3D_SIZE_GROUP+(local_faces*2)-1],
                                  local_faces);

    if (mesh->group[i])
    {

      /* Loop through every face within this group. */

      for (t = 0; t < local_faces; t++)
      {

        /* Get the offset of the triangle data within the file. */

        j = offs_tris + (binGetUI16_LE(&data[offs+35+(t*2)]) * MS3D_SIZE_FACE);

        /* Copy the vertex, texcoord, and normal data. */

        for (a = 0; a < 3; a++)
        {

          mesh->group[i]->tri[t]->v_id[a] = binGetUI16_LE(&data[j+2+(a*2)]);
          mesh->group[i]->tri[t]->n_id[a] = ((total_faces+t) * 3) + a;
          mesh->group[i]->tri[t]->t_id[a] = ((total_faces+t) * 3) + a;

          memcpy(&mesh->normal[0][mesh->group[i]->tri[t]->n_id[a]*3],
                &data[j+8+(a*4*3)], 4*3);

          memcpy(&mesh->texcoord[(mesh->group[i]->tri[t]->t_id[a]*2)+0],
                &data[j+44+(a*4)], 4);

          memcpy(&mesh->texcoord[(mesh->group[i]->tri[t]->t_id[a]*2)+1],
                &data[j+56+(a*4)], 4);

          mesh->texcoord[(mesh->group[i]->tri[t]->t_id[a]*2)+1] = 1.0f -
          mesh->texcoord[(mesh->group[i]->tri[t]->t_id[a]*2)+1];

        }

      }

    } else { /* TODO: Flag this as an error. */ }

    /* Move on to the next group and increment the total_faces counter. */

    offs += MS3D_SIZE_GROUP + (local_faces*2);

    total_faces += local_faces;

  }

  /* Load mesh material data. */

  offs = offs_mtrls;

  if (mesh->mtrl)
  {
    for (i = 0; i < num_mtrls; ++i, offs += MS3D_SIZE_MTRL)
    {
      mtrl_s *m = mesh->mtrl->item[i];

      strncpy(m->name, (char*)&data[offs + 0], 32);
      memcpy(m->ambient,    &data[offs + 32 ], sizeof(float) * 3);
      memcpy(m->diffuse,    &data[offs + 48 ], sizeof(float) * 3);
      memcpy(m->specular,   &data[offs + 64 ], sizeof(float) * 3);
      memcpy(m->emission,   &data[offs + 80 ], sizeof(float) * 3);
      memcpy(&m->shininess, &data[offs + 96 ], sizeof(float) * 1);
      memcpy(&m->alpha,     &data[offs + 100], sizeof(float) * 1);
      /* data[offs + 104] is a "mode" flag that isn't used any more. */
      strncpy(m->diffusemap, (char*)&data[offs + 105], 128);
      strncpy(m->alphamap,   (char*)&data[offs + 233], 128);
    }
  }

  /* Load joints. NOTE: Currently assumes "parents" are before child joints. */

  mesh->tag_list = meshTagSetCreate(num_joints);

  if (mesh->tag_list)
  {

    for (offs = offs_joints, i = 0; i < num_joints; i++, offs += MS3D_SIZE_BONE)
    {

      unsigned short num_rotation_animation_frames = 0;
      unsigned short num_position_animation_frames = 0;

      char parent[33];

      /* Load the joint name and position data. */

      memcpy(mesh->tag_list->tag[i]->name, &data[offs + 1], 32);
      memcpy(mesh->tag_list->tag[i]->rotation, &data[offs + 65], sizeof(float) * 3);
      memcpy(mesh->tag_list->tag[i]->position, &data[offs + 77], sizeof(float) * 3);

      /* Find the "parent" of this joint, of one exists. */

      memset(parent, 0, 33);
      memcpy(parent, &data[offs + 33], 32);

      for (j = 0; j < i; j++)
      {
        if (!strcmp(parent, mesh->tag_list->tag[j]->name)) { mesh->tag_list->tag[i]->parent = j; }
      }

      /* Skip past the joint animation key-frame data. */

      num_rotation_animation_frames = binGetUI16_LE(&data[offs + 89]);
      num_position_animation_frames = binGetUI16_LE(&data[offs + 91]);

      offs += num_rotation_animation_frames * MS3D_SIZE_ANIM_ROTATION;
      offs += num_position_animation_frames * MS3D_SIZE_ANIM_POSITION;

    }

  }

  /* Return the result. */

  meshTagSetMakeAbsolute(mesh->tag_list);
  meshReverseFaces(mesh);
  meshInvertNormals(mesh);

  return mesh;

}
