/**
@file
@brief Handles Quake III .MD3 files.

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* Quake 3 MD3 constants. */

#define MD3_XYZ_SCALE (1.0/64.0)  /**< @brief MD3: Vertex XYZ scaling factor. */

#define MD3_SIZE_HEAD         108 /**< @brief MD3: File HEADER size in bytes. */
#define MD3_HEAD_IDP3         0   /**< @brief MD3: Offset to magic number.    */
#define MD3_HEAD_VERSION      4   /**< @brief MD3: Offset to format version.  */
#define MD3_HEAD_NAME         8   /**< @brief MD3: Offset to model name.      */
#define MD3_HEAD_FLAGS        72  /**< @brief MD3: Offset to flags. Ignored.  */
#define MD3_HEAD_NUM_FRAMES   76  /**< @brief MD3: Offset to frame count.     */
#define MD3_HEAD_NUM_TAGS     80  /**< @brief MD3: Offset to tag count.       */
#define MD3_HEAD_NUM_SURFACES 84  /**< @brief MD3: Offset to surface count.   */
#define MD3_HEAD_NUM_UNUSED   88  /**< @brief MD3: Legacy skin count. Unused. */
#define MD3_HEAD_POS_FRAMES   92  /**< @brief MD3: Offset to frame offset.    */
#define MD3_HEAD_POS_TAGS     96  /**< @brief MD3: Offset to tag offset.      */
#define MD3_HEAD_POS_SURFACES 100 /**< @brief MD3: Offset to surface offset.  */
#define MD3_HEAD_POS_END      104 /**< @brief MD3: Offset to end-of-file.     */

#define MD3_SIZE_SURF         108 /**< @brief MD3: Surface HEADER size.       */
#define MD3_SURF_IDP3         0   /**< @brief MD3: Offset to magic number.    */
#define MD3_SURF_NAME         4   /**< @brief MD3: Offset to Surface name.    */
#define MD3_SURF_FLAGS        68  /**< @brief MD3: Offset to flags. Ignored.  */
#define MD3_SURF_NUM_FRAMES   72  /**< @brief MD3: Offset to frame count.     */
#define MD3_SURF_NUM_SKINS    76  /**< @brief MD3: Offset to skin count.      */
#define MD3_SURF_NUM_VERTS    80  /**< @brief MD3: Offset to vertex count.    */
#define MD3_SURF_NUM_FACES    84  /**< @brief MD3: Offset to triangle count.  */
#define MD3_SURF_POS_FACES    88  /**< @brief MD3: Offset to triangle offset. */
#define MD3_SURF_POS_SKINS    92  /**< @brief MD3: Offset to skin offset.     */
#define MD3_SURF_POS_COORDS   96  /**< @brief MD3: Offset to texcoord offset. */
#define MD3_SURF_POS_VERTS    100 /**< @brief MD3: Offset to vertex offset.   */
#define MD3_SURF_POS_END      104 /**< @brief MD3: Offset to surf-end offset. */

#define MD3_SIZE_FRAME        56  /**< @brief MD3: Frame chunk size in bytes. */
#define MD3_FRAME_NAME        40  /**< @brief MD3: Rel. offset to frame name. */

#define MD3_SIZE_TAG          112 /**< @brief MD3: Tag chunk size in bytes.   */
#define MD3_TAG_NAME          0   /**< @brief MD3: Rel. offset to tag name.   */
#define MD3_TAG_ORIGIN        64  /**< @brief MD3: Rel. offset to tag origin. */
#define MD3_TAG_MATRIX        76  /**< @brief MD3: Rel. offset to tag matrix. */

/*
[PUBLIC] Load a mesh from a Quake III .MD3 data buffer.
*/

mesh_s *meshLoadMD3(size_t length, const unsigned char *data)
{

  mesh_s *mesh = NULL;
  long num_groups, num_frames, num_verts, num_faces;
  long total_verts, offs, sub_offs, i, j, k, frame;
  float lat, lng;

  if (!data) { return NULL; }

  /* Validate header. */

  if (length < MD3_SIZE_HEAD) { return NULL; }

  if      (memcmp(data, "IDP3", 4)                  != 0 ) { return NULL; }
  else if (binGetUI32_LE(&data[MD3_HEAD_VERSION]) != 15) { return NULL; }

  num_frames = binGetUI32_LE(&data[MD3_HEAD_NUM_FRAMES]);
  num_groups = binGetUI32_LE(&data[MD3_HEAD_NUM_SURFACES]);

  /* Count the number of vertices required by this mesh. */

  num_verts = 0;
  offs      = binGetUI32_LE(&data[MD3_HEAD_POS_SURFACES]);

  for (i = 0; i < num_groups; i++)
  {
    num_verts += binGetUI32_LE(&data[offs + MD3_SURF_NUM_VERTS]);
    offs      += binGetUI32_LE(&data[offs + MD3_SURF_POS_END  ]);
  }

  /* Attempt to allocate memory for the basic mesh structure. */

  mesh = meshCreate(num_frames, num_verts, num_verts, num_verts, num_groups);
  if (!mesh) { return NULL; }

  /* Load the name of each animation frame. */

  offs = binGetUI32_LE(&data[MD3_HEAD_POS_FRAMES]);

  for (i = 0; i < num_frames; i++)
  {

    mesh->name[i] = (char*)malloc(17);
    if (mesh->name[i])
    {
      memset(mesh->name[i], 0,                            17);
      memcpy(mesh->name[i], &data[offs + MD3_FRAME_NAME], 16);
    }

    offs += MD3_SIZE_FRAME;

  }

  /* Load MD3 tags. */

  mesh->tag_list = meshTagSetCreate(binGetUI32_LE(&data[MD3_HEAD_NUM_TAGS]));

  if (mesh->tag_list)
  {

    offs = binGetUI32_LE(&data[MD3_HEAD_POS_TAGS]);

    for (i = 0; i < mesh->tag_list->num_tags; i++)
    {

      /* FIXME: This assumes the name is >= 64 bytes long - It MAY be less! */

      memcpy(mesh->tag_list->tag[i]->name,   &data[offs + MD3_TAG_NAME  ], 64   );
      memcpy(mesh->tag_list->tag[i]->position, &data[offs + MD3_TAG_ORIGIN], 4 * 3);

      /* TODO: Load the 3x3 matrix data here. */

      offs += MD3_SIZE_TAG;

    }

  } else { /* TODO: Flag this as an error. */ }

  /* Initialise materials list. */

  mesh->mtrl = mtrlCreateGroup(num_groups);

  if (!mesh->mtrl) { /* TODO: Flag this as an error. */ }

  /* Load each group in turn. */

  total_verts = 0;

  offs = binGetUI32_LE(&data[MD3_HEAD_POS_SURFACES]);

  for (i = 0; i < num_groups; i++)
  {

    if (mesh->mtrl && binGetUI32_LE(&data[offs + MD3_SURF_NUM_SKINS]) > 0)
    {

      /* FIXME: This assumes diffusemap is >= 64 bytes long - It MAY be less! */

      sub_offs = binGetUI32_LE(&data[offs + MD3_SURF_POS_SKINS]);

      memcpy(mesh->mtrl->item[i]->diffusemap, &data[offs + sub_offs], 64);

    }

    /* FIXME: This assumes that name[] is >= 64 bytes long - It MAY be less! */

    num_faces = binGetUI32_LE(&data[offs + MD3_SURF_NUM_FACES]);

    mesh->group[i] = meshGroupCreate((const char*)&data[offs + MD3_SURF_NAME], i, num_faces);

    if (mesh->group[i])
    {

      /* Load triangles. */

      sub_offs = binGetUI32_LE(&data[offs + MD3_SURF_POS_FACES]);

      for (j = 0; j < num_faces; j++)
      {

        for (k = 0; k < 3; k++)
        {

          mesh->group[i]->tri[j]->v_id[k] = binGetUI32_LE(&data[offs + sub_offs]);
          mesh->group[i]->tri[j]->v_id[k] += total_verts;

          mesh->group[i]->tri[j]->n_id[k] = mesh->group[i]->tri[j]->v_id[k];
          mesh->group[i]->tri[j]->t_id[k] = mesh->group[i]->tri[j]->v_id[k];

          sub_offs += 4;

        }

      }

      /* Load vertices and normals. */

      num_verts = binGetUI32_LE(&data[offs + MD3_SURF_NUM_VERTS]);
      sub_offs  = binGetUI32_LE(&data[offs + MD3_SURF_POS_VERTS]);

      for (frame = 0; frame < num_frames; frame++)
      {

        for (j = 0; j < num_verts; j++)
        {

          /* Get the XYZ vertex position. */

          for (k = 0; k < 3; k++)
          {

            mesh->vertex[frame][(total_verts * 3) + (j * 3) + k] =
                MD3_XYZ_SCALE * (float)binGetSI16_LE(&data[offs + sub_offs]);

            sub_offs += 2;

          }

          /* Decode the normal (convert "lat"/"lng" from 0-255 into radians). */

          lng = (double)data[offs + sub_offs + 0] / 255.0 * (2.0 * 3.141593);
          lat = (double)data[offs + sub_offs + 1] / 255.0 * (2.0 * 3.141593);

          mesh->normal[frame][(total_verts * 3) + (j * 3) + 0] = cos(lat) * sin(lng);
          mesh->normal[frame][(total_verts * 3) + (j * 3) + 1] = sin(lat) * sin(lng);
          mesh->normal[frame][(total_verts * 3) + (j * 3) + 2] = cos(lng);

          sub_offs += 2;

        }

      }

      /* Load texcoords. */

      sub_offs = binGetUI32_LE(&data[offs + MD3_SURF_POS_COORDS]);

      for (j = 0; j < num_verts; j++)
      {

        memcpy(&mesh->texcoord[(total_verts * 2) + (j * 2) + 0], &data[offs + sub_offs + 0], 4);
        memcpy(&mesh->texcoord[(total_verts * 2) + (j * 2) + 1], &data[offs + sub_offs + 4], 4);

        mesh->texcoord[(total_verts * 2) + (j*2) + 1] =
            1.0f - mesh->texcoord[(total_verts * 2) + (j * 2) + 1];

        sub_offs += 8;

      }

    } else { /* TODO: Flag this as an error. */ }

    total_verts += binGetUI32_LE(&data[offs + MD3_SURF_NUM_VERTS]);
    offs        += binGetUI32_LE(&data[offs + MD3_SURF_POS_END  ]);

  }

  /* Return result. */

  return mesh;

}
