/**
@file
@brief Handles Princeton .OFF files.

<http://shape.cs.princeton.edu/benchmark/documentation/off_format.html>

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/*
[PUBLIC] Load a mesh from a Princeton .OFF text string.
*/

mesh_s *meshLoadOFF(const char *text)
{

  mesh_s *mesh = NULL;
  char *text_end = NULL;
  const char *face_start = NULL;
  long num_verts, num_faces, num_edges, num_tris, i, pass;
  int error = 0;

  if (!text) { return NULL; }

  /* The file needs to start with "OFF" in order to be an OFF file. */

  if (strncmp(text, "OFF", 3) != 0) { return NULL; }
  if (!isspace(text[3])) { return NULL; }
  text += 4;

  /* The first 3 integers represent numVertices, numFaces, and numEdges. */

  num_verts = strtol(text, &text_end, 0);
  if (!text_end) { return NULL; } else { text = text_end; }

  num_faces = strtol(text, &text_end, 0);
  if (!text_end) { return NULL; } else { text = text_end; }

  num_edges = strtol(text, &text_end, 0);
  if (!text_end || num_edges != 0) { return NULL; } else { text = text_end; }

  /* Allocate memory for the main mesh structure. */

  mesh = meshCreate(1, num_verts, 0, 0, 1);
  if (!mesh) { return NULL; }

  /* Load vertices. */

  for (i = 0; i < num_verts * 3; i++)
  {
    mesh->vertex[0][i] = strtod(text, &text_end);
    if (!text_end) { error = 1; } else { text = text_end; }
  }

  /* Load faces. These might not be triangles, so conversions are necessary. */

  for (face_start = text, pass = 0; pass < 2; pass++, text = face_start)
  {

    for (num_tris = 0, i = 0; i < num_faces; i++)
    {

      long points, j;

      /* Get the number of points in the face. */

      points = strtol(text, &text_end, 0);
      if (!text_end || points < 3) { error = 1; } else { text = text_end; }

      for (j = 0; j < points; j++)
      {

        /* Get the vertex index of this point. */

        long v_id = strtol(text, &text_end, 0);
        if (!text_end) { error = 1; } else { text = text_end; }

        /* Determine if this is a basic triangle, or a quad/etc. */

        if (j < 3)
        {
          if (mesh->group[0]) { mesh->group[0]->tri[num_tris]->v_id[j] = v_id; }
        }
        else
        {

          /* Every extra vertex (after the third) requires a new triangle. */

          num_tris++;

          if (mesh->group[0])
          {
            mesh->group[0]->tri[num_tris]->v_id[0] = mesh->group[0]->tri[num_tris-1]->v_id[0];
            mesh->group[0]->tri[num_tris]->v_id[1] = mesh->group[0]->tri[num_tris-1]->v_id[2];
            mesh->group[0]->tri[num_tris]->v_id[2] = v_id;
          }

        }

      }

      /* Move on to the next triangle. */

      num_tris++;

    }

    /* At the end of the first pass, allocate memory to store the triangles. */

    if (pass == 0)
    {
      mesh->group[0] = meshGroupCreate(NULL, -1, num_tris);
      if (!mesh->group) { error = 1; }
    }

  }

  /* If any errors were encountered, free the partially-loaded mesh data. */

  if (error != 0) { return meshDelete(mesh); }

  /* Reverse the face-winding order, generate normals and return the result. */

  meshReverseFaces(mesh);
  return mesh;

}
