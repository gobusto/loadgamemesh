/**
@file
@brief Handles Wavefront .OBJ files.

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/**
@brief [INTERNAL] The structure used for parsing text-based formats.

This is used to keep track of the current "parser state" through multiple calls
to per-line callback functions which would otherwise have to rely on statically
allocated variables. Since it is possible for a multi-threaded program to parse
multiple files at the same time, static variables are not an ideal solution.
*/

typedef struct
{

  mesh_s *mesh; /**< @brief The mesh being created/constructed. */

  long num_groups;  /**< @brief Number of groups [in total/read so far].    */
  long num_coords;  /**< @brief Number of texcoords [in total/read so far]. */
  long num_verts;   /**< @brief Number of vertices [in total/read so far].  */
  long num_norms;   /**< @brief Number of normals [in total/read so far].   */
  long num_tris;    /**< @brief Number of triangles [in total/read so far]. */

  long mat_id;      /**< @brief The current material index.      */

} parse_obj_s;

/**
@brief [INTERNAL] Per-line callback used when parsing OBJ files.

State is stored in a structure that is passed through to each call as userdata.

@param line Single line of pre-processed text (extra spaces/tabs removed, etc).
@param data Userdata: Used to pass through the "mesh_parse_s" state object.
@return Zero on success, or non-zero on error.
*/

static int meshReadLineOBJ(const char *line, void *data)
{

  parse_obj_s *pso = NULL;
  mesh_group_s *grp = NULL;
  char          *str = NULL;
  char          *tmp = NULL;

  long v_i, t_i, n_i, i;

  /* Get the parse state object. */

  pso = (parse_obj_s*)data;

  if (!pso) { return -1; }

  /* Count things. Faces BEFORE the first group are in the "default" group. */

  if (!strncmp (line, "g ", 2) || (!strncmp (line, "f ", 2) && pso->num_groups < 1))
  {

    if (pso->mesh)
    {

      /* Get the name of the NEXT group. */

      grp = pso->mesh->group[pso->num_groups];

      if (grp && line[0] == 'g')
      {
        if (grp->name) { free(grp->name); }
        grp->name = (char*)malloc(strlen(&line[2]) + 1);
        if (grp->name) { strcpy(grp->name, &line[2]); }
      }

      /* Create the PREVIOUS group. */

      if (pso->num_groups > 0)
      {

        if (!pso->mesh->group[pso->num_groups-1])
        {

          pso->mesh->group[pso->num_groups-1] = meshGroupCreate (NULL, -1, pso->num_tris);

        }

      }

    }

    /* Increment the group counter and reset the per-group triangle counter. */

    pso->num_tris = 0;
    pso->num_groups++;

  }
  else if (!strncmp (line, "v ",  2))
  {

    /* If the mesh has been created, copy the data. */

    if (pso->mesh)
    {

      sscanf (&line[2], "%f %f %f", &pso->mesh->vertex[0][(pso->num_verts*3)+0],
                                    &pso->mesh->vertex[0][(pso->num_verts*3)+1],
                                    &pso->mesh->vertex[0][(pso->num_verts*3)+2]);

    }

    /* Increment the vertex counter. */

    pso->num_verts++;

  }
  else if (!strncmp (line, "vt ", 3))
  {

    /* If the mesh has been created, copy the data. */

    if (pso->mesh)
    {

      sscanf (&line[3], "%f %f", &pso->mesh->texcoord[(pso->num_coords*2)+0],
                                 &pso->mesh->texcoord[(pso->num_coords*2)+1]);

    }

    /* Increment the texcoord counter. */

    pso->num_coords++;

  }
  else if (!strncmp (line, "vn ", 3))
  {

    /* If the mesh has been created, copy the data. */

    if (pso->mesh)
    {

      sscanf (&line[3], "%f %f %f", &pso->mesh->normal[0][(pso->num_norms*3)+0],
                                    &pso->mesh->normal[0][(pso->num_norms*3)+1],
                                    &pso->mesh->normal[0][(pso->num_norms*3)+2]);

      /* Invert the normals, as we will reverse all faces later on. */

      for (i = 0; i < 3; i++)
      {

        pso->mesh->normal[0][(pso->num_norms*3)+i] =
        -pso->mesh->normal[0][(pso->num_norms*3)+i];

      }

    }

    /* Increment the normal counter. */

    pso->num_norms++;

  }

  /* The next part of the code relies on the mesh already being allocated. */

  if (!pso->mesh) { return 0; }

  /* Check for material data. */

  if (pso->mesh->mtrl)
  {

    if (!strncmp (line, "usemtl ", 7))
    {

      pso->mat_id = mtrlFind (pso->mesh->mtrl, &line[7]);

    }

  }
  else if (!strncmp (line, "mtllib ", 7))
  {

    pso->mesh->mtrl = mtrlLoadFile (&line[7]);

  }

  /* Check for face data. */

  if (!strncmp (line, "f ", 2))
  {

    /* Count the number of triangles in the current face. */

    str = (char*)line;
    i   = 0;

    while (str)
    {

      str = strstr (&str[1], " ");

      if (str)
      {

        /* Initialise the vertex, texture, and normal indices. */

        v_i = 0;
        t_i = 0;
        n_i = 0;

        /* Get the vertex index. */

        if (sscanf (&str[1], "%ld", &v_i) == 1)
        {

          /* Check for other indices. */

          tmp = strstr (&str[1], "/");

          if (tmp)
          {

            /* Get the texture coordinate index. */

            if (tmp[1] != '/')
            {

              if (sscanf (&tmp[1], "%ld", &t_i) != 1) { t_i = 0; }

            }

            /* Get the normal index. */

            tmp = strstr (&tmp[1], "/");

            if (tmp)
            {

              if (sscanf (&tmp[1], "%ld", &n_i) != 1) { n_i = 0; }

            }

          }

          /* OBJ files start from index one, not index zero. */

          v_i--;
          t_i--;
          n_i--;

          /* Copy the face indices, if the group exists. */

          grp = pso->mesh->group[pso->num_groups-1];

          if (grp)
          {

            /* Some tools put "usemtl" before "g abc" and some put it after. */

            grp->mat_id = pso->mat_id;

            /* Set triangle indices. */

            if (i < 3)
            {

              grp->tri[pso->num_tris]->v_id[i] = v_i;
              grp->tri[pso->num_tris]->n_id[i] = n_i;
              grp->tri[pso->num_tris]->t_id[i] = t_i;

            }
            else
            {

              grp->tri[pso->num_tris+i-2]->v_id[0] =
              grp->tri[pso->num_tris+i-3]->v_id[0];
              grp->tri[pso->num_tris+i-2]->v_id[1] =
              grp->tri[pso->num_tris+i-3]->v_id[2];
              grp->tri[pso->num_tris+i-2]->v_id[2] = v_i;

              grp->tri[pso->num_tris+i-2]->n_id[0] =
              grp->tri[pso->num_tris+i-3]->n_id[0];
              grp->tri[pso->num_tris+i-2]->n_id[1] =
              grp->tri[pso->num_tris+i-3]->n_id[2];
              grp->tri[pso->num_tris+i-2]->n_id[2] = n_i;

              grp->tri[pso->num_tris+i-2]->t_id[0] =
              grp->tri[pso->num_tris+i-3]->t_id[0];
              grp->tri[pso->num_tris+i-2]->t_id[1] =
              grp->tri[pso->num_tris+i-3]->t_id[2];
              grp->tri[pso->num_tris+i-2]->t_id[2] = t_i;

            }

          }

          /* Increment vertex counter */

          i++;

        }

      }

    }

    /* Increment the face counter. */

    if (i >= 3) { pso->num_tris += i - 2; }

  }

  return 0;

}

/*
[PUBLIC] Load a mesh from a Wavefront .OBJ text string.
*/

mesh_s *meshLoadOBJ(const char *text)
{

  parse_obj_s state;
  int i = 0;

  if (!text) { return NULL; }

  /* Loop through the text 3 times, loading different data on each pass. */

  state.mesh = NULL;

  for (i = 0; i < 3; i++)
  {

    mesh_s *temp = state.mesh;
    memset(&state, 0, sizeof(parse_obj_s));
    state.mesh = temp;

    state.mat_id = -1;

    txtEachLine(text, "#", meshReadLineOBJ, &state);

    if (!state.mesh)
    {

      /* Create the main mesh structure at the end of the first pass. */

      state.mesh = meshCreate (1, state.num_verts,  state.num_norms,
                                   state.num_coords, state.num_groups);

      if (!state.mesh) { return NULL; }

    }
    else if (!state.mesh->group[state.num_groups-1])
    {

      /* Create the FINAL group at the end of the second pass. */

      state.mesh->group[state.num_groups-1] = meshGroupCreate (NULL, -1, state.num_tris);

    }

  }

  /* Reverse the faces BEFORE generating normals! */

  meshReverseFaces(state.mesh);

  /* Return the loaded mesh. */

  return state.mesh;

}
