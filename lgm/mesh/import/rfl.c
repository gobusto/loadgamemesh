/**
@file
@brief Handles Red Faction .RFL files.

Copyright (C) 2015 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* Constants. This list is incomplete, since there are loads of chunk types. */

#define RFL_4CC 0xD4BADA55 /**< @brief Magic number for Volition RFL files. */

#define RFL_CHUNK_MESH 0x00000100 /**< @brief Contains a single mesh. */

/**
@brief Read a 16-bit (little-endian) Pascal-style string from a data buffer.

This is used by Red Faction RFL files, but may be useful elsewhere too...

@param src_len The maximum length of the `src` buffer.
@param src A pointer to the start of the P-string data within a binary buffer.
@param out_len The maximum length of the `out` buffer, if present.
@param out A pointer to an output buffer. This may be NULL if not required.
@return The TOTAL length of the P-String, including the UI16 length prefix.
*/

static size_t binGetPascalString16_LE(size_t src_len, const unsigned char *src,
                                      size_t out_len,                char *out)
{
  size_t value;

  if (src_len < 2 || !src) { return 0; }
  value = binGetUI16_LE(src);
  if (src_len - 2 < value) { return 0; }

  if (out && out_len)
  {
    memset(out, 0, out_len);
    memcpy(out, &src[2], (value < out_len - 1) ? value : out_len - 1);
  }

  return 2 + value;
}

/*
[PUBLIC] Load a mesh from a Red Faction .RFL data buffer.
*/

mesh_s *meshLoadRFL(size_t length, const unsigned char *data)
{
  unsigned long version;
  size_t chunk_offs;

  if (length < 32 || !data) { return NULL; }
  if (binGetUI32_LE(data) != 0xD4BADA55) { return NULL; }

  version = binGetUI32_LE(&data[4]);
  switch (version)
  {
    /* These RFL versions are used by the original Red Faction game on PS2: */
    case 156: case 157: case 158: case 174: case 175: break;
    /* Unknown versions may be from Red Faction 2, or may be invalid files: */
    default:
      fprintf(stderr, "[RFL] Unknown version: %lu\n", version);
    return NULL;
  }

  if (version >= 174)
  {
    /* This "maximum size" is based on a 28-byte header + 8-byte NULL chunk. */
    chunk_offs = binGetPascalString16_LE(length - 28 + 8, &data[28], 0, NULL);
    if (chunk_offs == 0) { return NULL; } else { chunk_offs += 28; }
  }
  else { chunk_offs = 20; }

  while (chunk_offs < length)
  {
    unsigned long chunk_type = binGetUI32_LE(&data[chunk_offs + 0]);
    unsigned long chunk_size = binGetUI32_LE(&data[chunk_offs + 4]);

    if (chunk_type == RFL_CHUNK_MESH)
    {
      mesh_s *mesh = NULL;
      int pass;

      for (pass = 0; pass < 2; ++pass)
      {
        unsigned long num_mtrls, num_verts, num_polys, num_coords, num_xxxx, i;
        size_t offs, start_of_polygon_data;
        int subpass;

        offs = chunk_offs + 8;

        /* These bytes always seem to be zero; panic/give up if they're not... */
        if (binGetUI16_LE(&data[offs + 0]) != 0) { return NULL; }
        if (binGetUI32_LE(&data[offs + 2]) != 0) { return NULL; }

        num_mtrls = binGetUI32_LE(&data[offs + 6]);
        for (offs += 10, i = 0; i < num_mtrls; ++i)
        {
          offs += binGetPascalString16_LE(chunk_size - offs, &data[offs], MTRL_MAXSTRLEN,
              mesh && mesh->mtrl ? mesh->mtrl->item[i]->diffusemap : NULL);
          if (mesh && mesh->mtrl) { sprintf(mesh->mtrl->item[i]->name, "texture%lu", i); }
        }

        /* These bytes always seem to be zero; panic/give up if they're not... */
        if (binGetUI32_LE(&data[offs]) != 0) { return NULL; }

        /* This list isn't relevant, but we need to skip past it... */
        num_xxxx = binGetUI32_LE(&data[offs + 4]);
        for (offs += 8, i = 0; i < num_xxxx; ++i)
        {
          unsigned long flags = binGetUI32_LE(&data[offs + (8 * 4)]);
          offs += 10 * 4;

          if (flags & 1)
          {
            offs += 8;
            offs += binGetPascalString16_LE(chunk_size - offs, &data[offs], 0, NULL);
            offs += (9 * 4) + 1;
          }

          if (flags & 256) { offs += 4; }
        }

        /* This list isn't relevant, but we need to skip past it... */
        num_xxxx = binGetUI32_LE(&data[offs]);
        for (offs += 4, i = 0; i < num_xxxx; ++i)
        { offs += 8 + (binGetUI32_LE(&data[offs + 4]) * 4); }

        /* This list isn't relevant, but we need to skip past it... */
        offs += 4 + (binGetUI32_LE(&data[offs]) * 8 * 4);

        num_verts = binGetUI32_LE(&data[offs]);
        if (mesh) { memcpy(mesh->vertex[0], &data[offs + 4], 4*3 * num_verts); }
        offs += 4 + (4 * 3 * num_verts);

        /*
        There are two slight complications when loading the polygon data:

        + First: We need to group them by texture, but they're not sorted. This
          is solved by looping over the data for each texture, finding only the
          polygons with a matching texture ID.
        + Second: Polygons may have any number of points, but we use triangles;
          we need to break them down. Also: Each point has its own texcoord, so
          we need to update the global texture coordinate counter accordingly.

        NOTE: Some polygons include extra texcoords, but I don't know why yet.
        */

        num_polys = binGetUI32_LE(&data[offs]);
        start_of_polygon_data = offs + 4;

        for (subpass = 0; subpass < 2; ++subpass)
        {
          unsigned long m = 0;
          for (num_coords = 0; m < num_mtrls; ++m)
          {
            mesh_group_s *group = mesh ? mesh->group[m] : NULL;
            unsigned long num_tris = 0;
            for (offs = start_of_polygon_data, i = 0; i < num_polys; ++i)
            {
              unsigned long texture_id, num_points, p;
              long unknown_id;

              if (version >= 174) { offs += 4 * 4; }

              texture_id = binGetUI32_LE(&data[offs + 0]);
              unknown_id = binGetSI32_LE(&data[offs + 4]);
              num_points = binGetUI32_LE(&data[offs + 36]);

              for (offs += 40, p = 0; p < num_points; ++p, offs += (unknown_id < 0 ? 3 : 5) * 4)
              {
                if (texture_id != m || !group) { continue; }
                /* Break the polygon down into triangles. */
                if (p < 3)
                {
                  group->tri[num_tris]->v_id[p] = binGetUI32_LE(&data[offs]);
                  group->tri[num_tris]->t_id[p] = num_coords + p;
                }
                else
                {
                  const unsigned long t = num_tris + p - 2;

                  group->tri[t]->v_id[0] = group->tri[num_tris]->v_id[0];
                  group->tri[t]->t_id[0] = group->tri[num_tris]->t_id[0];

                  group->tri[t]->v_id[1] = group->tri[t-1]->v_id[2];
                  group->tri[t]->t_id[1] = group->tri[t-1]->t_id[2];

                  group->tri[t]->v_id[2] = binGetUI32_LE(&data[offs]);
                  group->tri[t]->t_id[2] = num_coords + p;
                }
                /* Load texture coordinates. */
                memcpy(&mesh->texcoord[(num_coords+p)*2], &data[offs+4], 2*4);
              }

              if (texture_id == m)
              {
                num_tris += num_points - 2;
                num_coords += num_points;
              }
            }
            /*  Allocate a group, so we can store data on the second pass. */
            if (mesh && subpass == 0)
            {
              /* NOTE: This might "fail" if no triangles use the material. */
              mesh->group[m] = meshGroupCreate(NULL, m, num_tris);
            }
          }
        }

        if (pass == 0)
        {
          mesh = meshCreate(1, num_verts, 0, num_coords, num_mtrls);
          if (!mesh) { return NULL; }
          mesh->mtrl = mtrlCreateGroup(num_mtrls);
        }
      }

      meshReverseFaces(mesh);
      return mesh;
    }

    chunk_offs += 8 + chunk_size;
  }

  return NULL;
}
