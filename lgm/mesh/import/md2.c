/**
@file
@brief Handles Quake II .MD2 files.

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/*
[PUBLIC] Load a mesh from a Quake II .MD2 data buffer.
*/

mesh_s *meshLoadMD2(size_t length, const unsigned char *data)
{

  mesh_s *mesh = NULL;  /* The mesh object to be returned. */

  float scale[3];             /* Vertex compression data: Scaling.     */
  float trans[3];             /* Vertex compression data: Translation. */

  long offs, i, j, k;

  /* Validate header. */

  if (!data || length < 68) { return NULL; }

  if (memcmp (data, "IDP2", 4) != 0) { return NULL; }
  if (binGetUI32_LE (&data[4])  != 8) { return NULL; }

  /* Create mesh and triangle group. */

  mesh = meshCreate (binGetUI32_LE (&data[40]),      /* Anim. Frames.  */
                      binGetUI32_LE (&data[24]), 0,   /* Verts/Norms.   */
                      binGetUI32_LE (&data[28]), 1);  /* Coords/Groups. */

  if (!mesh) { return NULL; }

  mesh->group[0] = meshGroupCreate ("MD2", 0, binGetUI32_LE (&data[32]));

  if (!mesh->group[0])
  {

    meshDelete (mesh);

    return NULL;

  }

  /* Get texture. MD2 files may contain multiple skins - just use the first. */

  if (binGetUI32_LE (&data[20]) > 0)
  {

    mesh->mtrl = mtrlCreateGroup(1);

    if (mesh->mtrl)
    {

      memcpy (mesh->mtrl->item[0]->diffusemap,
              &data[binGetUI32_LE (&data[44])], 64);

    }

  }

  /* Get animation data. */

  offs = binGetUI32_LE (&data[56]);

  for (i = 0; i < mesh->num_frames; i++)
  {

    /* Get vertex decompression information. */

    memcpy (scale, &data[offs], 4*3); offs += 4*3;
    memcpy (trans, &data[offs], 4*3); offs += 4*3;

    /* Get frame name. */

    mesh->name[i] = (char*)malloc(17);

    if (mesh->name[i])
    {
      memset(mesh->name[i], 0,           17);
      memcpy(mesh->name[i], &data[offs], 16);
    }

    offs += 16;

    /* Get vertices. Skip the unused Quake 2 normal index. */

    for (j = 0; j < mesh->num_verts; j++)
    {

      for (k = 0; k < 3; k++)
      {

        mesh->vertex[i][(j*3)+k] = ((float)data[offs] * scale[k]) + trans[k];

        offs++;

      }

      offs++;

    }

  }

  /* Get texcoord data. */

  offs = binGetUI32_LE (&data[48]);
  i    = binGetUI32_LE (&data[ 8]);
  j    = binGetUI32_LE (&data[12]);

  for (k = 0; k < mesh->num_coords; k++)
  {

    mesh->texcoord[(k*2)+0] = (0 + data[offs+0]+(data[offs+1]*256)) / (float)i;
    mesh->texcoord[(k*2)+1] = (j - data[offs+2]+(data[offs+3]*256)) / (float)j;

    offs += 4;

  }

  /* Load triangle data. */

  offs = binGetUI32_LE (&data[52]);

  for (i = 0; i < mesh->group[0]->num_tris; i++)
  {

    /* Get vertex indices. */

    for (j = 0; j < 3; j++)
    {

      mesh->group[0]->tri[i]->v_id[j] = data[offs]+(data[offs+1]*256);

      offs += 2;

    }

    /* Get texcoord indices. */

    for (j = 0; j < 3; j++)
    {

      mesh->group[0]->tri[i]->t_id[j] = data[offs]+(data[offs+1]*256);

      offs += 2;

    }

  }

  /* Return the mesh. */

  return mesh;

}
