/**
@file
@brief Handles Doom 3 / Quake 4 .MD5mesh files.

<http://tfc.duke.free.fr/coding/md5-specs-en.html>

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* Quake 4 MD5 constants. */

#define MD5_VERSION "MD5Version 10" /**< @brief MD5: MD5mesh version string. */

/* Parse state flags. */

#define MESH_PARSE_FLAG_MD5JOINTS 0x01 /**< @brief MD5: Set if parsing tags. */

/**
@brief [INTERNAL] Defines a single vertex weight in a skeletally animated mesh.

This is currently only used when loading Doom 3 / Quake 4 *.md5mesh files.
*/

typedef struct
{

  long      joint_id; /**< @brief The index of the tag that the weight uses. */
  float     bias;     /**< @brief Influence of the weight/tag on the vertex. */
  vector3_t xyz;      /**< @brief The position of this vertex weight.        */

} mesh_weight_s;

/**
@brief [INTERNAL] The structure used for parsing text-based formats.

This is used to keep track of the current "parser state" through multiple calls
to per-line callback functions which would otherwise have to rely on statically
allocated variables. Since it is possible for a multi-threaded program to parse
multiple files at the same time, static variables are not an ideal solution.
*/

typedef struct
{

  unsigned long flags;  /**< @brief State flags. */

  mesh_s *mesh; /**< @brief The mesh being created/constructed. */

  long num_groups;  /**< @brief Number of groups [in total/read so far].    */
  long num_joints;  /**< @brief Number of joints [in total/read so far].    */
  long num_verts;   /**< @brief Number of vertices [in total/read so far].  */
  long num_tris;    /**< @brief Number of triangles [in total/read so far]. */

  long offs_vertex; /**< @brief The current vertex index offset. */

  long next_weight;       /**< @brief The current joint index. */
  long num_weights;       /**< @brief The number of weights.   */
  mesh_weight_s **weight; /**< @brief Array of vertex weights. */

} parse_md5_s;

/**
@brief Convert three XYZ euler angles into a quaternion.

This differs from the "standard" version because the multiplication order needs
to be YZX for .MD5mesh files, whereas everything else (including MS3D files) is
in ZYX order.

@param output The quaternion to be set.
@param x The X euler angle in radians.
@param y The Y euler angle in radians.
@param z The Z euler angle in radians.
@return Zero on success, or non-zero on failure.
*/

static int quatFromEulerForMD5mesh(quaternion_t output, float x, float y, float z)
{
  quaternion_t qx, qy, qz, temp;

  if (!output) { return -1; }

  quatFromAngle(qx, QUAT_X, x);
  quatFromAngle(qy, QUAT_Y, y);
  quatFromAngle(qz, QUAT_Z, z);

  quatMultiply(temp, qy, qz);
  quatMultiply(output, temp, qx);

  return 0;
}

/**
@brief [INTERNAL] Per-line callback function for parsing Doom 3 .md5mesh files.

This function is called before a mesh has been allocated, in order to determine
how much memory will be required. It also builds a list of weight information.

@param line The current line of text.
@param data Points to the current parser state object.
@return Zero on success, or non-zero on error.
*/

static int meshLoadMD5_pass1(const char *line, void *data)
{

  parse_md5_s *state = (parse_md5_s*)data;

  if      (!state     ) { return -1; }
  else if (state->mesh) { return -2; }

  if (strncmp(line, "numweights ", 11) == 0)
  {

    const long old_count = state->num_weights;
    void *tmp = NULL;

    state->num_weights += atoi(&line[11]);

    tmp = realloc(state->weight, sizeof(mesh_weight_s**) * state->num_weights);
    if (!tmp) { return 666; }
    state->weight = (mesh_weight_s**)tmp;
    memset(&state->weight[old_count], 0, sizeof(mesh_weight_s**) * (state->num_weights - old_count));

  }
  else if (strncmp(line, "weight ", 7) == 0)
  {

    int local_weight_number = 0;

    if (state->next_weight >= state->num_weights) { return -3; }

    state->weight[state->next_weight] = (mesh_weight_s*)malloc(sizeof(mesh_weight_s));
    if (!state->weight[state->next_weight]) { return 666; }
    memset(state->weight[state->next_weight], 0, sizeof(mesh_weight_s));

    sscanf(&line[7], "%d %ld %f (%f %f %f)", &local_weight_number,
        &state->weight[state->next_weight]->joint_id,
        &state->weight[state->next_weight]->bias,
        &state->weight[state->next_weight]->xyz[0],
        &state->weight[state->next_weight]->xyz[1],
        &state->weight[state->next_weight]->xyz[2]);

    state->next_weight++;

  }
  else if (strncmp(line, "numJoints ", 10) == 0) { state->num_joints += atoi(&line[10]); }
  else if (strncmp(line, "numMeshes ", 10) == 0) { state->num_groups += atoi(&line[10]); }
  else if (strncmp(line, "numverts ",   9) == 0) { state->num_verts  += atoi(&line[ 9]); }

  return 0;

}

/**
@brief [INTERNAL] Per-line callback function for parsing Doom 3 .md5mesh files.

This function is called once a basic mesh structure has been allocated, and is
used to fill in the (initially blank) structure with the mesh information.

@param line The current line of text.
@param data Points to the current parser state object.
@return Zero on success, or non-zero on error.
*/

static int meshLoadMD5_pass2(const char *line, void *data)
{

  parse_md5_s *state = (parse_md5_s*)data;
  int temp = 0;

  if      (!state      ) { return -1; }
  else if (!state->mesh) { return -2; }

  if      (strncmp(line, "joints", 6) == 0) { state->flags |= MESH_PARSE_FLAG_MD5JOINTS; }
  else if (line[0] == '}'                 ) { state->flags = 0;                           }
  else if (state->flags & MESH_PARSE_FLAG_MD5JOINTS)
  {
    mesh_tag_s *tag = state->mesh->tag_list->tag[state->num_joints];
    quaternion_t quat;

    if (state->num_joints >= state->mesh->tag_list->num_tags) { return 1; }

    sscanf(line, "\"%[^\"]\" %ld ( %f %f %f ) ( %f %f %f )",
      tag->name, &tag->parent,
      &tag->position[0], &tag->position[1], &tag->position[2],
      &quat[QUAT_X], &quat[QUAT_Y], &quat[QUAT_Z]
    );

    quat[QUAT_W] = quatCalculateW(quat[QUAT_X], quat[QUAT_Y], quat[QUAT_Z]);
    quatAsEuler(quat, &tag->rotation[0], &tag->rotation[1], &tag->rotation[2]);

    ++state->num_joints;
  }
  else if (strncmp(line, "numtris ", 8) == 0)
  {

    /* If no group is declared (or the group already exists), it's an error. */
    if      (state->num_groups <= 0                 ) { return 1; }
    else if (state->mesh->group[state->num_groups-1]) { return 2; }

    /* Otherwise, create a new (empty) triangle group. */
    state->mesh->group[state->num_groups-1] = meshGroupCreate(NULL, state->num_groups-1, atof(&line[8]));
    if (!state->mesh->group[state->num_groups-1]) { return 666; }

    /* Reset the (per-group) triangle counter. */
    state->num_tris = 0;

  }
  else if (strncmp(line, "tri ", 4) == 0)
  {

    mesh_group_s *group = NULL;

    /* Attempt to get the current triangle group. */
    if (state->num_groups <= 0) { return 1; }
    group = state->mesh->group[state->num_groups-1];

    /* If the group doesn't exist or is already at max capacity, report it. */
    if      (!group                            ) { return 2; }
    else if (state->num_tris >= group->num_tris) { return 3; }

    /* Get the vertex indices for this triangle. */
    sscanf(&line[4], "%d %ld %ld %ld", &temp,
        &group->tri[state->num_tris]->v_id[0],
        &group->tri[state->num_tris]->v_id[1],
        &group->tri[state->num_tris]->v_id[2]);

    /* The vertex indices are local to this mesh, so correct them. */
    group->tri[state->num_tris]->v_id[0] += state->offs_vertex;
    group->tri[state->num_tris]->v_id[1] += state->offs_vertex;
    group->tri[state->num_tris]->v_id[2] += state->offs_vertex;

    /* The texcoord IDs are the same as the vertex IDs, so just copy them. */
    group->tri[state->num_tris]->t_id[0] = group->tri[state->num_tris]->v_id[0];
    group->tri[state->num_tris]->t_id[1] = group->tri[state->num_tris]->v_id[1];
    group->tri[state->num_tris]->t_id[2] = group->tri[state->num_tris]->v_id[2];

    /* Increment the triangle counter. */
    state->num_tris++;

  }
  else if (strncmp(line, "vert ", 5) == 0)
  {

    long weight_start, weight_count, i;

    if (state->num_verts >= state->mesh->num_verts) { return 1; }

    /* Copy the texture coordinate and vertex weight index data. */
    sscanf(&line[5], "%d ( %f %f ) %ld %ld", &temp,
        &state->mesh->texcoord[(state->num_verts*2)+0],
        &state->mesh->texcoord[(state->num_verts*2)+1], &weight_start, &weight_count);

    /* TODO: Check that weight_start and weight_count are within range... */
    for (i = 0; i < weight_count; ++i)
    {
      const mesh_weight_s *weight = state->weight[state->next_weight + weight_start + i];
      const mesh_tag_s *joint  = state->mesh->tag_list->tag[weight->joint_id];

      quaternion_t final_position, original_position, joint_quaternion;

      original_position[QUAT_W] = 0;
      original_position[QUAT_X] = weight->xyz[0];
      original_position[QUAT_Y] = weight->xyz[1];
      original_position[QUAT_Z] = weight->xyz[2];

      quatFromEulerForMD5mesh(joint_quaternion, joint->rotation[0], joint->rotation[1], joint->rotation[2]);
      quatRotate(final_position, original_position, joint_quaternion);

      state->mesh->vertex[0][(state->num_verts*3)+0] += (final_position[QUAT_X] + joint->position[0]) * weight->bias;
      state->mesh->vertex[0][(state->num_verts*3)+1] += (final_position[QUAT_Y] + joint->position[1]) * weight->bias;
      state->mesh->vertex[0][(state->num_verts*3)+2] += (final_position[QUAT_Z] + joint->position[2]) * weight->bias;
    }

    /* Increment the vertex/texture coordinate counter. */
    state->num_verts++;

  }
  else if (strncmp(line, "mesh", 4) == 0)
  {
    if (state->num_groups >= state->mesh->num_groups) { return 1; }
    state->num_groups++;  /* <-- Note that this stores the NEXT group index! */
  }
  else if (strncmp(line, "numverts ",    9) == 0) { state->offs_vertex = state->num_verts; }
  else if (strncmp(line, "numweights ", 11) == 0) { state->next_weight += atoi(&line[11]); }

  /* Report success - no problems found. */
  return 0;

}

/*
[PUBLIC] Load a mesh from a Doom 3 .md5mesh text string.
*/

mesh_s *meshLoadMD5(const char *text)
{

  parse_md5_s state;
  int error = 0;
  long i = 0;

  if (!text) { return NULL; }
  if (strncmp(text, MD5_VERSION, strlen(MD5_VERSION)) != 0) { return NULL; }

  /* FIRST PASS: Determine how much memory will be required and allocate it. */

  memset(&state, 0, sizeof(parse_md5_s));
  error = txtEachLine(text, "//", meshLoadMD5_pass1, &state);

  if (error == 0)
  {

    state.mesh = meshCreate(1, state.num_verts, 0, state.num_verts, state.num_groups);

    if (state.mesh)
    {

      state.mesh->tag_list = meshTagSetCreate(state.num_joints);

      if (state.mesh->tag_list)
      {

        /* SECOND PASS: Load the data into the allocated mesh structure. */

        state.num_joints = 0;
        state.num_groups = 0;
        state.num_verts = 0;
        state.next_weight = 0;

        error = txtEachLine(text, "//", meshLoadMD5_pass2, &state);

      } else { error = 66; }

    } else { error = 99; }

  }

  /* FINALLY: Free the weights array, as it's no longer needed. */

  if (state.weight)
  {

    for (i = 0; i < state.num_weights; i++)
    {
      if (state.weight[i]) { free(state.weight[i]); }
    }

    free(state.weight);

  }

  /* If any errors were encountered, free the partially-loaded mesh data. */

  if (error != 0) { return meshDelete(state.mesh); }

  /* Generate normals and return the result. */

  return state.mesh;

}
