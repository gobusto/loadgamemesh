/**
@file
@brief Functions used internally to load meshes.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_IMPORT_MESH_H__
#define __LGM_IMPORT_MESH_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../mesh.h"
#include "../../shared/import.h"

/**
@brief Load a mesh from a 3D Studio .3DS data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoad3DS(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a Dark Engine .BIN data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadBIN(size_t length, const unsigned char *data);

/**
@brief Load a mesh from an id or Valve .BSP data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadBSP(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a Lightwave .LWO data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadLWO(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a Quake II .MD2 data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadMD2(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a Quake III .MD3 data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadMD3(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a Doom 3 .md5mesh text string.

@param text The text buffer to parse.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadMD5(const char *text);

/**
@brief Load a mesh from a Quake .MDL data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadMDL(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a Misfit Model 3D .MM3D data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadMM3D(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a Milkshape 3D .MS3D data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadMS3D(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a Wavefront .OBJ text string.

@param text The text buffer to parse.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadOBJ(const char *text);

/**
@brief Load a mesh from a Princeton .OFF text string.

@param text The text buffer to parse.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadOFF(const char *text);

/**
@brief Load a mesh from a MikuMikuDance .PMD data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadPMD(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a Red Faction .RFM or .RFC data buffer.

TODO: Support Red Faction 2 files, not just models from the original game.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadRFC(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a Red Faction .RFL data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadRFL(size_t length, const unsigned char *data);

/**
@brief Load a mesh from a .STL data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

mesh_s *meshLoadSTL(size_t length, const unsigned char *data);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_IMPORT_MESH_H__ */
