/**
@file
@brief Handles Red Faction .RFM and .RFC files.

Copyright (C) 2015 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* Constants. */

#define RFC_4CC 0x87128712 /**< @brief Magic number for Volition RFC files. */

#define RFC_CHUNK_BONE 0x424f4e45 /**< @brief Describes the bones hierarchy. */
#define RFC_CHUNK_DUMB 0x44554d42 /**< @brief Not sure. Maybe MD3-like tags? */
#define RFC_CHUNK_DATA 0x87251110 /**< @brief Contains the actual mesh data. */
#define RFC_CHUNK_SKIN 0x11133344 /**< @brief Lists the textures file names. */
#define RFC_CHUNK_CSPH 0x43535048 /**< @brief Collision sphere descriptions. */
#define RFC_CHUNK_NULL 0x00000000 /**< @brief Indicates EOF. Length is zero. */

/*
[PUBLIC] Load a mesh from a Red Faction .RFM or .RFC data buffer.
*/

mesh_s *meshLoadRFC(size_t length, const unsigned char *data)
{
  unsigned long version;
  size_t chunk_offs;

  if (length < 32 || !data) { return NULL; }
  if (binGetUI32_LE(data) != RFC_4CC) { return NULL; }

  version = binGetUI32_LE(&data[4]);
  if (version != 0 && version != 1) { return NULL; }

  for (chunk_offs = 32; chunk_offs < length;)
  {
    unsigned long chunk_type = binGetUI32_LE(&data[chunk_offs + 0]);
    unsigned long chunk_size = binGetUI32_LE(&data[chunk_offs + 4]);

    /* Note: Files with more than one LOD seem to have the best one first... */
    if (chunk_type == RFC_CHUNK_DATA)
    {
      unsigned long num_xxxx = binGetUI32_LE(&data[chunk_offs + 20]);
      unsigned long data_len = binGetUI32_LE(&data[chunk_offs + 64]);
      unsigned long data_offs = chunk_offs + 68;
      unsigned long head_offs = data_offs + data_len;
      unsigned short num_groups = binGetUI16_LE(&data[head_offs]);

      mesh_s *mesh = NULL;
      int pass;

      unsigned long size_xxxx = num_xxxx;
      while (size_xxxx % 16) { ++size_xxxx; }

      for (pass = 0; pass < 2; ++pass)
      {
        unsigned long total_verts = 0;
        unsigned long total_polys = 0;
        unsigned short g;

        for (g = 0; g < num_groups; ++g)
        {
          unsigned short num_verts = binGetUI16_LE(&data[head_offs + 2 + (g * 10) + 0]);
          unsigned short num_polys = binGetUI16_LE(&data[head_offs + 2 + (g * 10) + 2]);

          if (pass == 1)
          {
            unsigned long p;

            mesh->group[g] = meshGroupCreate(NULL, -1, num_polys);
            if (!mesh->group[g]) { return meshDelete(mesh); }

            memcpy(&mesh->vertex[0][total_verts*3], &data[data_offs], 4*3*num_verts);
            data_offs += binGetUI16_LE(&data[head_offs + 2 + (g * 10) + 4]);

            memcpy(&mesh->normal[0][total_verts*3], &data[data_offs], 4*3*num_verts);
            data_offs += binGetUI16_LE(&data[head_offs + 2 + (g * 10) + 4]);

            for (p = 0; p < num_polys; ++p, data_offs += 80)
            {
              unsigned long v;
              for (v = 0; v < 3; ++v)
              {
                long t = ((total_polys + p) * 3) + v;

                mesh->group[g]->tri[p]->v_id[v] = binGetUI32_LE(&data[data_offs + (v*4)]) + total_verts;
                mesh->group[g]->tri[p]->n_id[v] = mesh->group[g]->tri[p]->v_id[v];
                mesh->group[g]->tri[p]->t_id[v] = t;

                memcpy(&mesh->texcoord[t*2], &data[data_offs + 32 + (v*16)], 4*2);
              }
            }

            data_offs += binGetUI16_LE(&data[head_offs + 2 + (g * 10) + 8]);
            if (version == 1) { data_offs += size_xxxx; }
          }

          total_verts += num_verts;
          total_polys += num_polys;
        }

        if (pass == 0)
        {
          mesh = meshCreate(1, total_verts, total_verts, total_polys * 3, num_groups);
          if (!mesh) { return NULL; }
        }
        else
        {
          /* TODO: LOAD MATERIALS */
        }
      }

      meshReverseFaces(mesh);
      meshInvertNormals(mesh);
      return mesh;
    }

    chunk_offs += 8 + chunk_size;
  }

  return NULL;
}
