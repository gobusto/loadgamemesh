/**
@file
@brief Handles Stereo-Lithographic .STL files.

Copyright (C) 2008-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "import.h"

/*
[PUBLIC] Load a mesh from a .STL data buffer.
*/

mesh_s *meshLoadSTL(size_t length, const unsigned char *data)
{
  mesh_s *mesh = NULL;
  lgm_index_t t, i;

  /* Validate header. */
  if (!data || length < 134) { return NULL; }
  t = binGetUI32_LE(&data[80]);
  if (length != (size_t)((t * 50) + 84)) { return NULL; }

  /* Create a new mesh structure with one frame and one group. */
  mesh = meshCreate(1, t * 3, 0, 0, 1);
  if (!mesh) { return NULL; }

  /* Create surface. */
  mesh->group[0] = meshGroupCreate("STL", -1, t);
  if (!mesh->group[0]) { return meshDelete(mesh); }

  /* Read vertices/triangles. NOTE: This assumes 32-bit IEEE float values. */
  for (t = 0; t < mesh->group[0]->num_tris; t++)
  {
    for (i = 0; i < 3; i++)
    {
      mesh->group[0]->tri[t]->v_id[2-i] = (t * 3) + i;
      memcpy(&mesh->vertex[0][((t*3)+i)*3], &data[96+(t*50)+(i*12)], 12);
    }
  }

  return mesh;
}
