/**
@file
@brief Handles various kinds of .BSP file.

<http://hlbsp.sourceforge.net/index.php?content=bspdef>

<http://www.flipcode.com/archives/Quake_2_BSP_File_Format.shtml>

<http://www.mralligator.com/q3/>

<https://developer.valvesoftware.com/wiki/Source_BSP_File_Format>

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* BSP version numbers. */

#define BSP_HALFLIFE  30  /**< @brief Half Life. Does not have a 4cc. */
#define BSP_QUAKE     29  /**< @brief Quake 1. Does not have a 4cc.   */
#define BSP_QUAKE2    38  /**< @brief Quake 2. Preceded by IBSP 4cc.  */
#define BSP_QUAKE3    46  /**< @brief Quake 3. Preceded by IBSP 4cc.  */

/* Half Life 1 / Quake 1 Header Lump list. */

#define Q1BSP_HEAD_ENTITIES   4   /**< @brief Entities header entry offset.  */
#define Q1BSP_HEAD_PLANES     12  /**< @brief Planes header entry offset.    */
#define Q1BSP_HEAD_MIPTEX     20  /**< @brief Miptex header entry offset.    */
#define Q1BSP_HEAD_VERTICES   28  /**< @brief Vertices header entry offset.  */
#define Q1BSP_HEAD_VISIBILITY 36  /**< @brief VisiList header entry offset.  */
#define Q1BSP_HEAD_NODES      44  /**< @brief Nodes header entry offset.     */
#define Q1BSP_HEAD_TEXINFO    52  /**< @brief TexInfo header entry offset.   */
#define Q1BSP_HEAD_FACES      60  /**< @brief Faces header entry offset.     */
#define Q1BSP_HEAD_LIGHTMAPS  68  /**< @brief Lightmaps header entry offset. */
#define Q1BSP_HEAD_CLIPNODES  76  /**< @brief ClipNodes header entry offset. */
#define Q1BSP_HEAD_LEAVES     84  /**< @brief Leaves header entry offset.    */
#define Q1BSP_HEAD_LEAFFACE   92  /**< @brief LeafFace header entry offset.  */
#define Q1BSP_HEAD_EDGES      100 /**< @brief Edges header entry offset.     */
#define Q1BSP_HEAD_EDGELIST   108 /**< @brief EdgeList header entry offset.  */
#define Q1BSP_HEAD_MODELS     116 /**< @brief Models header entry offset.    */
#define Q1BSP_SIZEOF_HEADER   124 /**< @brief Length of Quake 1 BSP header.  */

/* Quake 2 Header Lump list. */

#define Q2BSP_HEAD_ENTITIES   8   /**< @brief about */
#define Q2BSP_HEAD_PLANES     16  /**< @brief about */
#define Q2BSP_HEAD_VERTICES   24  /**< @brief about */
#define Q2BSP_HEAD_VISIBILITY 32  /**< @brief about */
#define Q2BSP_HEAD_NODES      40  /**< @brief about */
#define Q2BSP_HEAD_TEXINFO    48  /**< @brief about */
#define Q2BSP_HEAD_FACES      56  /**< @brief about */
#define Q2BSP_HEAD_LIGHTMAPS  64  /**< @brief about */
#define Q2BSP_HEAD_LEAVES     72  /**< @brief about */
#define Q2BSP_HEAD_LEAFFACE   80  /**< @brief about */
#define Q2BSP_HEAD_LEAFBRUSH  88  /**< @brief about */
#define Q2BSP_HEAD_EDGES      96  /**< @brief about */
#define Q2BSP_HEAD_EDGELIST   104 /**< @brief about */
#define Q2BSP_HEAD_MODELS     112 /**< @brief about */
#define Q2BSP_HEAD_BRUSHES    120 /**< @brief about */
#define Q2BSP_HEAD_BRUSHSIDES 128 /**< @brief about */
#define Q2BSP_HEAD_POP        136 /**< @brief about */
#define Q2BSP_HEAD_AREAS      144 /**< @brief about */
#define Q2BSP_HEAD_PORTALS    152 /**< @brief about */
#define Q2BSP_SIZEOF_HEADER   160 /**< @brief about */

/* Half Life 1 / Quake 1 Mipmap Texture structure. */

#define Q1BSP_MIPTEX_NAME     0   /**< @brief ASCII texture name string.      */
#define Q1BSP_MIPTEX_W        16  /**< @brief Texture width in pixels.        */
#define Q1BSP_MIPTEX_H        20  /**< @brief Texture height in pixels.       */
#define Q1BSP_MIPTEX_OFFSETS  24  /**< @brief Offsets to embedded image data. */

/* Half Life 1 / Quake 1 TexInfo structure. */

#define Q1BSP_TEXINFO_S       0   /**< @brief U coordinate XYZ+offset value.  */
#define Q1BSP_TEXINFO_T       16  /**< @brief V coordinate XYZ+offset value.  */
#define Q1BSP_TEXINFO_MIPTEX  32  /**< @brief Index of the associated MipTex. */
#define Q1BSP_TEXINFO_FLAGS   36  /**< @brief Texture animation flags.        */
#define Q1BSP_SIZEOF_TEXINFO  40  /**< @brief Length of Quake 1 TexInfo data. */

/* Quake 2 TexInfo structure. */

#define Q2BSP_TEXINFO_S       0   /**< @brief U coordinate XYZ+offset value.  */
#define Q2BSP_TEXINFO_T       16  /**< @brief V coordinate XYZ+offset value.  */
#define Q2BSP_TEXINFO_FLAGS   32  /**< @brief Texture animation flags.        */
#define Q2BSP_TEXINFO_VALUE   36  /**< @brief Unknown.                        */
#define Q2BSP_TEXINFO_NAME    40  /**< @brief ASCII texture name string.      */
#define Q2BSP_TEXINFO_NEXT    72  /**< @brief Next TexInfo structure.         */
#define Q2BSP_SIZEOF_TEXINFO  76  /**< @brief Length of Quake 2 TexInfo data. */

/* Half Life 1 / Quake 1 / Quake 2 Face structure. */

#define Q1BSP_FACE_PLANEID    0   /**< @brief about */
#define Q1BSP_FACE_SIDE       2   /**< @brief about */
#define Q1BSP_FACE_LEDGEID    4   /**< @brief about */
#define Q1BSP_FACE_LEDGENUM   8   /**< @brief about */
#define Q1BSP_FACE_TEXINFOID  10  /**< @brief about */
#define Q1BSP_FACE_LIGHTFLAGS 12  /**< @brief about */
#define Q1BSP_FACE_LIGHTMAP   16  /**< @brief about */
#define Q1BSP_SIZEOF_FACE     20  /**< @brief about */

/**
@brief [INTERNAL] Load a mesh from a Quake or Quake II .BSP data buffer.

This function also handles Half-Life 1 BSP files, as they're almost identical.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

static mesh_s *meshLoadBSP_Quake(size_t length, const unsigned char *data)
{

  mesh_s *mesh = NULL;
  unsigned long version;
  unsigned long info_offs, info_size;
  unsigned long face_offs, face_size;
  unsigned long list_offs, edge_offs;
  int pass, sub_pass;

  /* Determine whether this is a Quake 2 BSP or a Quake/Half-Life BSP. */

  if (length < 8 || !data) { return NULL; }

  if (memcmp(data, "IBSP", 4) == 0)
  {
    version = binGetUI32_LE(&data[4]);
    if (version != BSP_QUAKE2        ) { return NULL; }
    if (length <= Q2BSP_SIZEOF_HEADER) { return NULL; }
  }
  else
  {
    version = binGetUI32_LE(data);
    if (version != BSP_QUAKE && version != BSP_HALFLIFE) { return NULL; }
    if (length <= Q1BSP_SIZEOF_HEADER                  ) { return NULL; }
  }

  /* Get the offset to/length of various lumps. */

  if (version == BSP_QUAKE2)
  {

    info_offs = binGetUI32_LE(&data[Q2BSP_HEAD_TEXINFO + 0]);
    info_size = binGetUI32_LE(&data[Q2BSP_HEAD_TEXINFO + 4]);

    face_offs = binGetUI32_LE(&data[Q2BSP_HEAD_FACES + 0]);
    face_size = binGetUI32_LE(&data[Q2BSP_HEAD_FACES + 4]);

    list_offs = binGetUI32_LE(&data[Q2BSP_HEAD_EDGELIST]);
    edge_offs = binGetUI32_LE(&data[Q2BSP_HEAD_EDGES]);

  }
  else
  {

    info_offs = binGetUI32_LE(&data[Q1BSP_HEAD_TEXINFO + 0]);
    info_size = binGetUI32_LE(&data[Q1BSP_HEAD_TEXINFO + 4]);

    face_offs = binGetUI32_LE(&data[Q1BSP_HEAD_FACES + 0]);
    face_size = binGetUI32_LE(&data[Q1BSP_HEAD_FACES + 4]);

    list_offs = binGetUI32_LE(&data[Q1BSP_HEAD_EDGELIST]);
    edge_offs = binGetUI32_LE(&data[Q1BSP_HEAD_EDGES]);

  }

  /* We want to put all faces with the same MipTex into the same group... */

  for (pass = 0; pass < 2; ++pass)
  {

    unsigned long num_groups = 0;
    unsigned long num_coords = 0;
    unsigned long g;

    /* Unused MipTex's will have zero faces, so group by TexInfo's instead. */

    for (g = info_offs; g < info_offs + info_size; g += version == BSP_QUAKE2 ? Q2BSP_SIZEOF_TEXINFO : Q1BSP_SIZEOF_TEXINFO)
    {

      char name[33];
      unsigned long tex_id, tex_w, tex_h;

      /* Ignore a TexInfo if the MipTex it references has already been seen. */

      if (version == BSP_QUAKE2)
      {

        unsigned long i;

        /* For Quake 2, check whether this filename has already been handled. */

        memset(name, 0, 33);
        memcpy(name, &data[g+Q2BSP_TEXINFO_NAME], 32);

        for (i = info_offs; i < g; i += Q2BSP_SIZEOF_TEXINFO)
        {
          if (strncmp(name, (const char*)&data[i+Q2BSP_TEXINFO_NAME], 32) == 0) { break; }
        }

        if (i < g) { continue; }

        /* TODO: Check the WAL/TGA file to get the actual width/height... */

        tex_w = 128;
        tex_h = 128;

      }
      else
      {

        unsigned long mtex_offs, miptex_data, i;

        /* For Quake/Half Life, get the index of the embedded MipTex instead. */

        tex_id = binGetUI32_LE(&data[g + Q1BSP_TEXINFO_MIPTEX]);

        for (i = info_offs; i < g; i += Q1BSP_SIZEOF_TEXINFO)
        {
          if (binGetUI32_LE(&data[i+Q1BSP_TEXINFO_MIPTEX]) == tex_id) { break; }
        }

        if (i < g) { continue; }

        /* Get the name and width/height of the embedded MipTex image. */

        mtex_offs = binGetUI32_LE(&data[Q1BSP_HEAD_MIPTEX]);
        miptex_data = mtex_offs + binGetUI32_LE(&data[mtex_offs + 4 + (tex_id*4)]);

        memset(name, 0, 17);
        memcpy(name, &data[miptex_data+Q1BSP_MIPTEX_NAME], 16);

        tex_w = binGetUI32_LE(&data[miptex_data+Q1BSP_MIPTEX_W]);
        tex_h = binGetUI32_LE(&data[miptex_data+Q1BSP_MIPTEX_H]);

      }

      /* If this is the second pass, add this material to the list. */

      if (mesh)
      {

        if (mesh->mtrl)
        {
          strcpy(mesh->mtrl->item[num_groups]->name, name);
          sprintf(mesh->mtrl->item[num_groups]->diffusemap, "textures/%s.tga", name);
        }

      }

      /* Loop through all faces that use this MipTex value. */

      for (sub_pass = 0; sub_pass < 2; ++sub_pass)
      {

        unsigned short num_tris = 0;
        unsigned long f;

        for (f = face_offs; f < face_offs + face_size; f += Q1BSP_SIZEOF_FACE)
        {

          unsigned long edge_list_id = binGetUI32_LE(&data[f+Q1BSP_FACE_LEDGEID]);
          unsigned short num_edges = binGetUI16_LE(&data[f+Q1BSP_FACE_LEDGENUM]);
          unsigned long texinfo = binGetUI16_LE(&data[f+Q1BSP_FACE_TEXINFOID]);

          /* Only count Faces whose TexInfo has the same MipTex ID. */

          if (version == BSP_QUAKE2)
          {
            texinfo = (texinfo * Q2BSP_SIZEOF_TEXINFO) + info_offs;
            if (strncmp(name, (const char*)&data[texinfo + Q2BSP_TEXINFO_NAME], 32) != 0) { continue; }
          }
          else
          {
            texinfo = (texinfo * Q1BSP_SIZEOF_TEXINFO) + info_offs;
            if (binGetUI32_LE(&data[texinfo + Q1BSP_TEXINFO_MIPTEX]) != tex_id) { continue; }
          }

          /* If this is the SECOND pass, copy the data into memory. */

          if (mesh)
          {

            mesh_group_s *group = mesh->group[num_groups];

            if (group)
            {

              unsigned long e = 0;

              for (e = 0; e < num_edges; ++e)
              {

                float xyz[3];
                float s_xyzw[4];
                float t_xyzw[4];

                long edge_id = binGetSI32_LE(&data[list_offs + (4 * (edge_list_id + e))]);
                unsigned long offs = ((abs(edge_id)) * 4) + (edge_id < 0 ? 2 : 0);
                unsigned short vertex_id = binGetUI16_LE(&data[edge_offs + offs]);

                if (e >= 3)
                {

                  group->tri[num_tris+e-2]->v_id[0] = group->tri[num_tris    ]->v_id[0];
                  group->tri[num_tris+e-2]->v_id[1] = group->tri[num_tris+e-3]->v_id[2];
                  group->tri[num_tris+e-2]->v_id[2] = vertex_id;

                  group->tri[num_tris+e-2]->t_id[0] = group->tri[num_tris    ]->t_id[0];
                  group->tri[num_tris+e-2]->t_id[1] = group->tri[num_tris+e-3]->t_id[2];
                  group->tri[num_tris+e-2]->t_id[2] = num_coords + e;

                }
                else
                {
                  group->tri[num_tris]->v_id[e] = vertex_id;
                  group->tri[num_tris]->t_id[e] = num_coords + e;
                }

                /* Get texture coordinates. Should use Q2* here, but it's OK. */

                memcpy(xyz, &mesh->vertex[0][vertex_id * 3], sizeof(float) * 3);
                memcpy(s_xyzw, &data[texinfo + Q1BSP_TEXINFO_S], 16);
                memcpy(t_xyzw, &data[texinfo + Q1BSP_TEXINFO_T], 16);

                mesh->texcoord[((num_coords + e) * 2) + 0] = 0 + (xyz[0] * s_xyzw[0] + xyz[1] * s_xyzw[1] + xyz[2] * s_xyzw[2] + s_xyzw[3]) / tex_w;
                mesh->texcoord[((num_coords + e) * 2) + 1] = 0 - (xyz[0] * t_xyzw[0] + xyz[1] * t_xyzw[1] + xyz[2] * t_xyzw[2] + t_xyzw[3]) / tex_h;

              }

            }

          }

          /* Increment the triangles counter for this group. */

          num_tris += num_edges - 2;

          if (sub_pass > 0) { num_coords += num_edges; }

        }

        if (mesh && sub_pass == 0 && num_tris > 0)
        {
          mesh->group[num_groups] = meshGroupCreate(name, num_groups, num_tris);
          if (!mesh->group[num_groups]) { return meshDelete(mesh); }
        }

      }

      /* Increment the group counter. */

      ++num_groups;

    }

    /* At the end of the FIRST pass, create a basic mesh structure. */

    if (pass == 0)
    {

      unsigned long vert_offs, vert_size;

      if (version == BSP_QUAKE2)
      {
        vert_offs = binGetUI32_LE(&data[Q2BSP_HEAD_VERTICES + 0]);
        vert_size = binGetUI32_LE(&data[Q2BSP_HEAD_VERTICES + 4]);
      }
      else
      {
        vert_offs = binGetUI32_LE(&data[Q1BSP_HEAD_VERTICES + 0]);
        vert_size = binGetUI32_LE(&data[Q1BSP_HEAD_VERTICES + 4]);
      }

      mesh = meshCreate(1, vert_size / 12, 0, num_coords, num_groups);
      if (!mesh) { return NULL; }
      mesh->mtrl = mtrlCreateGroup(num_groups);

      memcpy(mesh->vertex[0], &data[vert_offs], vert_size);

    }

  }

  return mesh;

}

/**
@brief [INTERNAL] Load a mesh from a Quake III Arena .BSP data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

static mesh_s *meshLoadBSP_Quake3(size_t length, const unsigned char *data)
{

  mesh_s *mesh = NULL;

  if (length < 8 || !data) { return NULL; }

  if      (memcmp(data, "IBSP", 4) != 0         ) { return NULL; }
  else if (binGetUI32_LE(&data[4]) != BSP_QUAKE3) { return NULL; }

  fprintf(stderr, "[FIXME] Quake 3 BSP support is UNIMPLEMENTED!\n");

  return mesh;

}

/**
@brief [INTERNAL] Load a mesh from a Source Engine .BSP data buffer.

@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return A valid mesh on success, or NULL on failure.
*/

static mesh_s *meshLoadBSP_Source(size_t length, const unsigned char *data)
{

  mesh_s *mesh = NULL;

  unsigned long version = 0;

  if (length < 4 || !data) { return NULL; }

  /* Games based on the Source engine by Valve software. */

  if (memcmp(data, "VBSP", 4) != 0) { return NULL; }
  version = binGetUI32_LE(&data[4]);
  if (version != 19 && version != 20) { return NULL; }

  fprintf(stderr, "[FIXME] Source BSP support is UNIMPLEMENTED!\n");

  return mesh;

}

/*
[PUBLIC] Load a mesh from an id or Valve .BSP data buffer.
*/

mesh_s *meshLoadBSP(size_t length, const unsigned char *data)
{

  mesh_s *mesh = meshLoadBSP_Quake(length, data);

  if (!mesh) { mesh = meshLoadBSP_Quake3(length, data); }
  if (!mesh) { mesh = meshLoadBSP_Source(length, data); }

  return mesh;

}
