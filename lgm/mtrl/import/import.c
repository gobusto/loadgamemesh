/**
@file
@brief Various functions related to importing materials.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "import.h"

/*
[PUBLIC] Load a material group from a text string.
*/

mtrl_group_s *mtrlLoadText(const char *text)
{
  mtrl_group_s *group = mtrlLoadMTL(text);
  if (!group) { group = mtrlLoadSKIN(text); }

  mtrlUnixPaths(group);
  return group;
}

/*
[PUBLIC] Load a material group from a file.
*/

mtrl_group_s *mtrlLoadFile(const char *file_name)
{
  mtrl_group_s *group = NULL;
  char         *text  = NULL;

  text = (char*)txtLoadFile(file_name, NULL);
  group = mtrlLoadText(text);
  if (text) { free(text); }

  return group;
}
