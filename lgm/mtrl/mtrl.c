/**
@file
@brief General material functions.

Copyright (C) 2008-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mtrl.h"

/*
[PUBLIC] Create a new material group.
*/

mtrl_group_s *mtrlCreateGroup(lgm_count_t num_items)
{
  mtrl_group_s *group = NULL;
  lgm_index_t i;

  if (num_items < 1) { return NULL; }

  /* Create the main list structure. */
  group = (mtrl_group_s*)malloc(sizeof(mtrl_group_s));
  if (!group) { return NULL; }
  memset(group, 0, sizeof(mtrl_group_s));

  group->num_items = num_items;

  group->item = (mtrl_s**)malloc(group->num_items * sizeof(mtrl_s*));
  if (!group->item) { return mtrlFreeGroup(group); }
  memset(group->item, 0, group->num_items * sizeof(mtrl_s*));

  /* Create the individual list items. */
  for (i = 0; i < group->num_items; ++i)
  {
    group->item[i] = (mtrl_s*)malloc(sizeof(mtrl_s));
    if (!group->item[i]) { return mtrlFreeGroup(group); }
    memset(group->item[i], 0, sizeof(mtrl_s));

    /* NOTE: These values are based on what OpenGL defaults to. */
    group->item[i]->ambient[0] = 0.2;
    group->item[i]->ambient[1] = 0.2;
    group->item[i]->ambient[2] = 0.2;

    group->item[i]->diffuse[0] = 0.8;
    group->item[i]->diffuse[1] = 0.8;
    group->item[i]->diffuse[2] = 0.8;

    group->item[i]->specular[0] = 0.0;
    group->item[i]->specular[1] = 0.0;
    group->item[i]->specular[2] = 0.0;

    group->item[i]->emission[0] = 0.0;
    group->item[i]->emission[1] = 0.0;
    group->item[i]->emission[2] = 0.0;

    group->item[i]->shininess = 0.0;

    group->item[i]->alpha = 1.0;
  }

  return group;
}

/*
[PUBLIC] Free a previously created material group.
*/

mtrl_group_s *mtrlFreeGroup(mtrl_group_s *group)
{
  lgm_index_t i;

  if (!group) { return NULL; }

  if (group->item)
  {
    for (i = 0; i < group->num_items; ++i) { free(group->item[i]); }
    free(group->item);
  }

  free(group);
  return NULL;
}

/*
[PUBLIC] Search for a material by name.
*/

lgm_index_t mtrlFind(const mtrl_group_s *group, const char *name)
{
  lgm_index_t i;

  if (!name || !group) { return -1; }

  for (i = 0; i < group->num_items; ++i)
  {
    if (strcmp(name, group->item[i]->name) == 0) { return i; }
  }

  return -1;
}

/*
[PUBLIC] Replace any DOS/Windows-style backslash path separators.
*/

lgm_bool_t mtrlUnixPaths(mtrl_group_s *group)
{
  lgm_index_t m;

  if (!group) { return LGM_FALSE; }

  for (m = 0; m < group->num_items; ++m)
  {
    mtrl_s *mtrl = group->item[m];
    size_t i;

    for (i = 0; mtrl->diffusemap[i]; ++i)
    { if (mtrl->diffusemap[i] == '\\') { mtrl->diffusemap[i] = '/'; } }

    for (i = 0; mtrl->bumpmap[i]; ++i)
    { if (mtrl->bumpmap[i] == '\\') { mtrl->bumpmap[i] = '/'; } }

    for (i = 0; mtrl->alphamap[i]; ++i)
    { if (mtrl->alphamap[i] == '\\') { mtrl->alphamap[i] = '/'; } }
  }

  return LGM_TRUE;
}
