/**
@file
@brief Utilities for handling MD3 tags, MS3D bones, MD5Mesh joints, etc.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "tag.h"

/*
[PUBLIC] Create a new tag-set structure.
*/

mesh_tag_set_s *meshTagSetCreate(lgm_count_t num_tags)
{
  mesh_tag_set_s *tag_set;
  lgm_index_t i;

  if (num_tags < 1) { return NULL; }

  tag_set = (mesh_tag_set_s*)malloc(sizeof(mesh_tag_set_s));
  if (!tag_set) { return NULL; }
  memset(tag_set, 0, sizeof(mesh_tag_set_s));

  tag_set->num_tags = num_tags;

  tag_set->tag = (mesh_tag_s**)malloc(sizeof(mesh_tag_s*) * num_tags);
  if (!tag_set->tag) { return meshTagSetDelete(tag_set); }
  memset(tag_set->tag, 0, sizeof(mesh_tag_s*) * num_tags);

  for (i = 0; i < num_tags; ++i)
  {
    tag_set->tag[i] = (mesh_tag_s*)malloc(sizeof(mesh_tag_s));
    if (!tag_set->tag[i]) { return meshTagSetDelete(tag_set); }
    memset(tag_set->tag[i], 0, sizeof(mesh_tag_s));
    tag_set->tag[i]->parent = -1;
  }

  return tag_set;
}

/*
[PUBLIC] Destroy a previously-created tag-set structure.
*/

mesh_tag_set_s *meshTagSetDelete(mesh_tag_set_s *tag_set)
{
  lgm_index_t i;

  if (!tag_set) { return NULL; }

  if (tag_set->tag)
  {
    for (i = 0; i < tag_set->num_tags; ++i) { free(tag_set->tag[i]); }
    free(tag_set->tag);
  }

  free(tag_set);
  return NULL;
}

/*
[PUBLIC] Convert relatively-positioned tags to be absolutely-positioned.
*/

lgm_bool_t meshTagSetMakeAbsolute(mesh_tag_set_s *tag_set)
{
  lgm_index_t i;

  if (!tag_set) { return LGM_FALSE; }

  for (i = 0; i < tag_set->num_tags; ++i)
  {
    mesh_tag_s *parent;
    mesh_tag_s *child;
    quaternion_t parent_quaternion, result, temp;

    child = tag_set->tag[i];
    if (child->parent < 0 || child->parent >= tag_set->num_tags) { continue; }
    parent = tag_set->tag[child->parent];
    quatFromEuler(parent_quaternion, parent->rotation[0], parent->rotation[1], parent->rotation[2]);

    /* Get the final tag orientation. */
    quatFromEuler(temp, child->rotation[0], child->rotation[1], child->rotation[2]);
    quatMultiply(result, parent_quaternion, temp);
    quatAsEuler(result, &child->rotation[0], &child->rotation[1], &child->rotation[2]);

    /* Get the final tag position. */
    temp[QUAT_W] = 0;
    temp[QUAT_X] = child->position[0];
    temp[QUAT_Y] = child->position[1];
    temp[QUAT_Z] = child->position[2];
    quatRotate(result, temp, parent_quaternion);

    child->position[0] = result[QUAT_X] + parent->position[0];
    child->position[1] = result[QUAT_Y] + parent->position[1];
    child->position[2] = result[QUAT_Z] + parent->position[2];
  }

  return LGM_TRUE;
}
