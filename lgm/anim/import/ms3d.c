/**
@file
@brief Handles import of skeletal animations from MilkShape3D .MS3D files.

<https://web.archive.org/web/20120206055629/http://chumbalum.swissquake.ch/ms3d/ms3dspec.txt>

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/* MS3D constants: */

#define ANIM_MS3D_HEADER_SIZE   14  /**< @brief _writeme_ */
#define ANIM_MS3D_VERTEX_SIZE   15  /**< @brief _writeme_ */
#define ANIM_MS3D_TRIANGLE_SIZE 70  /**< @brief _writeme_ */
#define ANIM_MS3D_GROUP_SIZE    36  /**< @brief _writeme_ */
#define ANIM_MS3D_MATERIAL_SIZE 361 /**< @brief _writeme_ */
#define ANIM_MS3D_JOINT_SIZE    93  /**< @brief _writeme_ */

/**
@brief Convert a time (in seconds) into a frame index.

@todo Move this into the "general" animation code, since it might be useful...

@param secs A time value in seconds.
@param fps The frames-per-second (playback rate) of the animation sequence.
@param num_frames The total number of frames in the animation sequence.
@return An animation frame index from `0` to `num_frames - 1`.
*/

static lgm_index_t animFrameFromTime(float secs, float fps, lgm_count_t num_frames)
{
  lgm_index_t frame_index = fps > 0 ? (lgm_index_t)(secs * fps) : 0;

  if (frame_index < 0)
  {
    fprintf(stderr, "Frame Index is negative - corrected.\n");
    return 0;
  }
  else if (frame_index >= num_frames)
  {
    fprintf(stderr, "Frame Index is too high - corrected.\n");
    return num_frames > 0 ? num_frames - 1 : 0;
  }
  return frame_index;
}

/**
@brief Read a MilkShape3D positions or rotation keyframe.

MS3D "time" values seem to be off by one frame; this function corrects that.

@param num_frames The number of animation frames expected in the MS3D file.
@param data A pointer to a raw byte value buffer.
@param xyz A buffer to output the XYZ position/euler values to.
@return The calculated frame index for the keyframe.
*/

static lgm_index_t animLoadMS3D_frame(lgm_count_t num_frames, float fps, const unsigned char *data, float *xyz)
{
  float frame_time;
  lgm_index_t frame_id;

  memcpy(&frame_time, data, 4);
  if (xyz) { memcpy(xyz, &data[4], 4 * 3); }

  frame_id = animFrameFromTime(frame_time, fps, num_frames + 1);
  if (frame_id < 1)
  {
    fprintf(stderr, "[WARNING] MS3D keyframe index was unexpectedly zero!\n");
    return 0;
  }
  return frame_id - 1;
}

/*
[PUBLIC] Load a skeletal animation sequence from a Milkshape .MS3D file.
*/

lgm_count_t animLoadMS3D(
  wordtree_s *anim_set,
  const char *name,
  size_t length,
  const unsigned char *data
)
{
  anim_s *anim;
  lgm_index_t f;
  unsigned long num_frames, offset;
  unsigned short num_items, i;
  float fps;

  if (!anim_set || !name || length < 12 || !data) { return -1; }
  else if (memcmp(data, "MS3D000000", 10) != 0) { return -1; }
  else if (binGetUI32_LE(&data[10]) != 4) { return -1; }
  offset = ANIM_MS3D_HEADER_SIZE;

  /* Skip past all the other data; we're only interested in the joints: */

  num_items = binGetUI16_LE(&data[offset]);
  offset += 2 + (num_items * ANIM_MS3D_VERTEX_SIZE);
  if (offset + 2 > length) { return -1; }

  num_items = binGetUI16_LE(&data[offset]);
  offset += 2 + (num_items * ANIM_MS3D_TRIANGLE_SIZE);
  if (offset + 2 > length) { return -1; }

  num_items = binGetUI16_LE(&data[offset]);
  for (offset += 2; num_items > 0; --num_items)
  {
    if (offset + ANIM_MS3D_GROUP_SIZE > length) { return -1; }
    offset += ANIM_MS3D_GROUP_SIZE + (binGetUI16_LE(&data[offset + 33]) * 2);
  }
  if (offset + 2 > length) { return -1; }

  num_items = binGetUI16_LE(&data[offset]);
  offset += 2 + (num_items * ANIM_MS3D_MATERIAL_SIZE);
  if (offset + 14 > length) { return -1; }

  /* Animation data starts here... */

  memcpy(&fps, &data[offset + 0], 4);
  num_frames = binGetUI32_LE(&data[offset + 8]);
  num_items = binGetUI16_LE(&data[offset + 12]);
  offset += 14;

  anim = animCreate(anim_set, name, 0, num_frames, num_items, fps);
  if (!anim) { return -1; }

  for (i = 0; i < num_items; ++i)
  {
    unsigned short num_rotations, num_positions, k;

    if (offset + ANIM_MS3D_JOINT_SIZE > length)
    {
      fprintf(stderr, "[WARNING] MS3D file is truncated - stopping early...\n");
      return num_frames;
    }

    num_rotations = binGetUI16_LE(&data[offset + 89]);
    num_positions = binGetUI16_LE(&data[offset + 91]);

    if (offset + ANIM_MS3D_JOINT_SIZE + (num_rotations * 16) + (num_positions * 16) > length)
    {
      fprintf(stderr, "[WARNING] MS3D file is truncated - stopping early...\n");
      return num_frames;
    }

    for (f = 0; f < anim->frame_count; ++f)
    {
      mesh_tag_s *tag = anim->frame[f]->tag[i];
      float tmp[3];

      /* Use the "un-animated" position/orientation of the joint by default: */
      memcpy(tmp, &data[offset + 65], sizeof(float) * 3);
      tag->rotation[0] = tmp[0];
      tag->rotation[1] = tmp[1];
      tag->rotation[2] = tmp[2];
      memcpy(tmp, &data[offset + 77], sizeof(float) * 3);
      tag->position[0] = tmp[0];
      tag->position[1] = tmp[1];
      tag->position[2] = tmp[2];

      /* Figure out the parent of the joint just once, on the first frame: */
      memcpy(tag->name, &data[offset + 1], 32);
      if (f > 0)
        tag->parent = anim->frame[0]->tag[i]->parent;
      else for (k = i; k > 0; --k)
        if (!memcmp(anim->frame[f]->tag[k - 1]->name, &data[offset + 33], 32))
        {
          tag->parent = k - 1;
          break;
        }
    }
    offset += ANIM_MS3D_JOINT_SIZE;

    /* TODO: Interpolate between positions/orientations between "key" frames */

    for (k = 0; k < num_rotations; ++k)
    {
      float xyz[3];
      f = animLoadMS3D_frame(num_frames, fps, &data[offset], xyz);
      anim->frame[f]->tag[i]->rotation[0] += xyz[0];
      anim->frame[f]->tag[i]->rotation[1] += xyz[1];
      anim->frame[f]->tag[i]->rotation[2] += xyz[2];
      offset += sizeof(float) * 4;
    }

    for (k = 0; k < num_positions; ++k)
    {
      float xyz[3];
      f = animLoadMS3D_frame(num_frames, fps, &data[offset], xyz);

      anim->frame[f]->tag[i]->position[0] -= xyz[0];
      anim->frame[f]->tag[i]->position[1] -= xyz[1];
      anim->frame[f]->tag[i]->position[2] -= xyz[2];

      offset += sizeof(float) * 4;
    }
  }

  for (f = 0; f < anim->frame_count; ++f) { meshTagSetMakeAbsolute(anim->frame[f]); }
  return 1;
}
