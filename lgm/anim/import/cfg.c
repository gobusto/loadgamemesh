/**
@file
@brief Handles Quake III .CFG files.

<http://cube.wikispaces.com/Importing+md2+and+md3+files>

Copyright (C) 2008-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "import.h"

/**
@brief Structure used to keep track of state when parsing Quake 3 .CFG files.
*/

typedef struct
{

  lgm_count_t num_anims;  /**< @brief Number of animations loaded so far. */
  wordtree_s *anim_set;   /**< @brief Animation set to add animations to. */

} anim_load_cfg_s;

/**
@brief Per-line callback function for parsing Quake III .cfg files.

@todo The animation name could be more than 512 characters - fix this!

@param line The current line of text.
@param data A pointer to the dictionary of animation sequences.
@return Zero on success, or non-zero on failure.
*/

static int animLoadCFG_line(const char *line, void *data)
{
  anim_load_cfg_s *state = (anim_load_cfg_s*)data;

  char name[512];
  long start, num_frames, temp;
  float fps;

  if      (strncmp(line, "//",          2 ) == 0) { return 0; }
  else if (strncmp(line, "sex ",        4 ) == 0) { return 0; }
  else if (strncmp(line, "fixedtorso",  10) == 0) { return 0; }
  else if (strncmp(line, "fixedlegs",   9 ) == 0) { return 0; }
  else if (strncmp(line, "headoffset ", 11) == 0) { return 0; }
  else if (strncmp(line, "footsteps ",  10) == 0) { return 0; }

  if (sscanf(line, "%ld %ld %ld %f // %[^ ]", &start, &num_frames, &temp, &fps, name) != 5)
  {
    /* If it fails on the first one, then it's most likely not a CFG file... */
    if (state->num_anims > 0)
      fprintf(stderr, "[ERROR] Unknown MD3 animation instruction: %s\n", line);
    return -2;
  }
  else if (!animCreate(state->anim_set, name, start, num_frames, 0, fps))
  {
    fprintf(stderr, "[ERROR] Could not add CFG animation to set: %s\n", line);
    return -3;
  }

  ++state->num_anims;
  return 0;
}

/*
[PUBLIC] Load a set of animation sequences from a Quake 3 .cfg file.
*/

lgm_count_t animLoadCFG(
  wordtree_s *anim_set,
  const char *name,
  const char *text
)
{
  anim_load_cfg_s state;

  if (!anim_set || !text) { return -1; }

  if (name) { /* Silence "unused parameter" warnings... */ }

  memset(&state, 0, sizeof(anim_load_cfg_s));
  state.anim_set = anim_set;
  txtEachLine(text, NULL, animLoadCFG_line, &state);

  return state.num_anims > 0 ? state.num_anims : -1;
}
