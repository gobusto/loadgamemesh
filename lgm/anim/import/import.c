/**
@file
@brief Various functions related to importing animations.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "import.h"

/*
[PUBLIC] Load one or more animation sequences from a text string.
*/

lgm_count_t animLoadText(
  wordtree_s *anim_set,
  const char *name,
  const char *text
)
{
  return animLoadCFG(anim_set, name, text);
}

/*
[PUBLIC] Load one or more animation sequences from a binary data buffer.
*/

lgm_count_t animLoadData(
  wordtree_s *anim_set,
  const char *name,
  size_t length,
  const unsigned char *data
)
{
  return animLoadMS3D(anim_set, name, length, data);
}

/*
[PUBLIC] Load one or more animation sequences from a file.
*/

lgm_count_t animLoadFile(
  wordtree_s *anim_set,
  const char *name,
  const char *file_name
)
{
  lgm_count_t result;
  unsigned char *data;
  long length = 0;

  data = txtLoadFile(file_name, &length);

  result = animLoadData(anim_set, name, (size_t)length, data);
  if (result < 1) { result = animLoadText(anim_set, name, (const char*)data); }

  free(data);
  return result;
}
