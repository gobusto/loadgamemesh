/**
@file
@brief Functions used internally to load animations.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_IMPORT_ANIM_H__
#define __LGM_IMPORT_ANIM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../anim.h"
#include "../../shared/import.h"

/**
@brief Load a set of animation sequences from a Quake 3 .CFG file.

@param anim_set The animation set to add the new animations to.
@param name If no animation "name" is available, use this string instead.
@param text The text buffer to parse.
@return The number of new animations loaded, or -1 on failure.
*/

lgm_count_t animLoadCFG(
  wordtree_s *anim_set,
  const char *name,
  const char *text
);

/**
@brief Load a skeletal animation sequence from a Milkshape .MS3D file.

@param anim_set The animation set to add the new animations to.
@param name If no animation "name" is available, use this string instead.
@param length The length of the data buffer in bytes.
@param data A pointer to a raw byte value buffer.
@return The number of new animations loaded (should be one), or -1 on failure.
*/

lgm_count_t animLoadMS3D(
  wordtree_s *anim_set,
  const char *name,
  size_t length,
  const unsigned char *data
);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_IMPORT_ANIM_H__ */
