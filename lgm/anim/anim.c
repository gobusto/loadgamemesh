/**
@file
@brief General animation functions.

Copyright (C) 2008-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "anim.h"

#include <stdio.h>  /* For debugging only... */

/**
@brief Delete a single animation from memory.

This is used as a callback routine in the animSetDelete() function.

@param name The name "key" used to store this animation within a wordtree.
@param data The anim_s structure representing a single animation.
@param user Not used; ignored.
@return Always returns NULL.
*/

static void *animDelete(const char *name, void *data, void *user)
{
  anim_s *anim = (anim_s*)data;

  if (user) { }
  if (name) { /* fprintf(stderr, "[DEBUG] Deleting animation %s\n", name); */ }

  if (anim->frame)
  {
    lgm_index_t i;
    for (i = 0; i < anim->frame_count; ++i) { meshTagSetDelete(anim->frame[i]); }
    free(anim->frame);
  }

  free(anim);
  return NULL;
}

/*
[PUBLIC] Create a new animation sequence structure and add it to a collection.
*/

anim_s *animCreate(
  wordtree_s *anim_set,
  const char *name,
  lgm_index_t start,
  lgm_count_t num_frames,
  lgm_count_t num_tags,
  float fps
)
{
  anim_s *anim = NULL;

  if (!name || start < 0 || num_frames < 1 || fps < 0.0) { return NULL; }

  /* Create the basic animation structure for a morph- or boned- animation: */
  anim = (anim_s*)malloc(sizeof(anim_s));
  if (!anim) { return NULL; }
  memset(anim, 0, sizeof(anim_s));

  anim->frame_start = start;
  anim->frame_count = num_frames;
  anim->fps = fps;

  /* For skeletal animations, we also need a list of animation frames: */
  if (num_tags > 0)
  {
    lgm_index_t i;

    anim->frame = (mesh_tag_set_s**)malloc(sizeof(mesh_tag_set_s*) * num_frames);
    if (!anim->frame) { return animDelete(NULL, anim, NULL); }

    for (i = 0; i < num_frames; ++i)
    {
      anim->frame[i] = meshTagSetCreate(num_tags);
      if (!anim->frame[i]) { return animDelete(NULL, anim, NULL); }
    }
  }

  /* If an animation set was specified, add this animation to it: */
  if (anim_set)
  {
    void *old = NULL;
    if (!treeInsert(anim_set, name, anim, &old)) { return animDelete(NULL, anim, NULL); }
    if (old) { animDelete(NULL, old, NULL); }
  }

  return anim;
}

/*
[PUBLIC] Delete a previously-created collection of animation sequences.
*/

wordtree_s *animSetDelete(wordtree_s *anim_set)
{ return treeDelete(anim_set, animDelete, NULL); }

/**
@brief Per-line callback for animSetMorphCount

@param name The name of the animation sequence.
@param data The `anim_s` structure representing a single animation.
@param user The counter variable to increment.
@return Always returns the original value of the `data` parameter.
*/

static void *animSetMorphCount_callback(const char *name, void *data, void *user)
{
  const anim_s *anim = (anim_s*)data;
  if (name) { /* Silence "unused parameter" warnings. */ }
  if (animIsMorph(anim)) { *((lgm_count_t*)user) += 1; }
  return data;
}

/*
[PUBLIC] Get the number of skeletal animations in an animation set.
*/

lgm_count_t animSetMorphCount(wordtree_s *anim_set)
{
  lgm_count_t i = 0;
  treeEachItem(anim_set, animSetMorphCount_callback, &i);
  return i;
}

/**
@brief Per-line callback for animSetSkeletalCount

@param name The name of the animation sequence.
@param data The `anim_s` structure representing a single animation.
@param user The counter variable to increment.
@return Always returns the original value of the `data` parameter.
*/

static void *animSetSkeletalCount_callback(const char *name, void *data, void *user)
{
  const anim_s *anim = (anim_s*)data;
  if (name) { /* Silence "unused parameter" warnings. */ }
  if (animIsSkeletal(anim)) { *((lgm_count_t*)user) += 1; }
  return data;
}

/*
[PUBLIC] Get the number of morph-based animations in an animation set.
*/

lgm_count_t animSetSkeletalCount(wordtree_s *anim_set)
{
  lgm_count_t i = 0;
  treeEachItem(anim_set, animSetSkeletalCount_callback, &i);
  return i;
}
