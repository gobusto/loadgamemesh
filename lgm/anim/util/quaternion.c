/**
@file
@brief Provides various utilities for working with quaternions.

Copyright (C) 2013-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <math.h>
#include "quaternion.h"

/* Constants and macros. */

#define SQ(x) (x*x) /**< @brief This is just to make the code more readable. */

/*
[PUBLIC] Get the length of a vector.
*/

float vecLength(int d, const float *v)
{
  float length = 0;
  int i;

  if (d && v)
    for (i = 0; i < d; ++i)
      length += v[i] * v[i];

  return length > 0 ? sqrt(length) : 0;
}

/*
[PUBLIC] Scale a vector so that it is unit length.
*/

float vecNormalize(int d, float *v)
{
  float length = vecLength(d, v);
  int i;

  if (length > 0)
    for (i = 0; i < d; ++i)
      v[i] /= length;

  return length;
}

/*
[PUBLIC] Subtract one 3D vector from another to calculate the difference.
*/

int vecSubtract(vector3_t output, const vector3_t v1, const vector3_t v2)
{
  if (!output || !v1 || !v2) { return 1; }

  output[0] = v1[0] - v2[0];
  output[1] = v1[1] - v2[1];
  output[2] = v1[2] - v2[2];

  return 0;
}

/*
[PUBLIC] Get the dot product of two 3D vectors.
*/

float vecDotProduct(const vector3_t v1, const vector3_t v2)
{
  return v1 && v2 ? v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2] : 0.0f;
}

/*
[PUBLIC] Get the cross product of two 3D vectors.
*/

int vecCrossProduct(vector3_t output, const vector3_t v1, const vector3_t v2)
{
  if (!output || !v1 || !v2) { return 1; }

  output[0] = v1[1] * v2[2] - v1[2] * v2[1];
  output[1] = v1[2] * v2[0] - v1[0] * v2[2];
  output[2] = v1[0] * v2[1] - v1[1] * v2[0];

  return 0;
}

/*
[PUBLIC] Calculate the W-component of a unit-length quaternion.

REFERENCE:

http://tfc.duke.free.fr/coding/md5-specs-en.html
*/

float quatCalculateW(float x, float y, float z)
{
  float t = 1.0 - SQ(x) - SQ(y) - SQ(z);
  return (t < 0) ? 0 : -sqrt(t);
}

/*
[PUBLIC] Initialise a quaternion to an identity state.
*/

int quatIdentity(quaternion_t quaternion)
{
  if (!quaternion) { return -1; }

  quaternion[QUAT_W] = 1.0;
  quaternion[QUAT_X] = 0.0;
  quaternion[QUAT_Y] = 0.0;
  quaternion[QUAT_Z] = 0.0;

  return 0;
}

/*
[PUBLIC] Convert a single euler angle (specified in radians) into a quaternion.

The more general formula is as follows:

qw = cos(angle/2)
qx = ax * sin(angle/2)
qy = ay * sin(angle/2)
qz = az * sin(angle/2)

REFERENCE:

http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
*/

int quatFromAngle(quaternion_t quaternion, axis_t axis, float angle)
{
  if (!quaternion || axis == QUAT_W) { return -1; }

  quaternion[QUAT_W] = cos(angle / 2.0);
  quaternion[QUAT_X] = (axis == QUAT_X) ? sin(angle / 2.0) : 0.0;
  quaternion[QUAT_Y] = (axis == QUAT_Y) ? sin(angle / 2.0) : 0.0;
  quaternion[QUAT_Z] = (axis == QUAT_Z) ? sin(angle / 2.0) : 0.0;

  return 0;
}

/*
[PUBLIC] Convert three XYZ euler angles into a quaternion.

The Z/Y/X quaternion multiplication order is based on the MS3D file format.

REFERENCE:

http://www.flipcode.com/documents/matrfaq.html#Q60

http://content.gpwiki.org/index.php/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation#Quaternion_from_Euler_angles
*/

int quatFromEuler(quaternion_t output, float x, float y, float z)
{
  quaternion_t qx, qy, qz, temp;

  if (!output) { return -1; }

  quatFromAngle(qx, QUAT_X, x);
  quatFromAngle(qy, QUAT_Y, y);
  quatFromAngle(qz, QUAT_Z, z);

  quatMultiply(temp, qz, qy);
  quatMultiply(output, temp, qx);

  return 0;
}

/*
[PUBLIC] Convert a quaternion into three XYZ euler angles.

REFERENCE:

http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/
*/

int quatAsEuler(quaternion_t quat, float *x, float *y, float *z)
{
  float heading, attitude, bank, test;

  if (!quat) { return -1; }

  test = quat[QUAT_X]*quat[QUAT_Y] + quat[QUAT_Z]*quat[QUAT_W];
  if (test >= 0.4999)
  {
    heading = 2 * atan2(quat[QUAT_X], quat[QUAT_W]);
    attitude = QQQ_PI/2;
    bank = 0;
  }
  else if (test <= -0.4999)
  {
    heading = -2 * atan2(quat[QUAT_X], quat[QUAT_W]);
    attitude = QQQ_PI/-2;
    bank = 0;
  }
  else
  {
    heading = atan2(2*quat[QUAT_Y]*quat[QUAT_W]-2*quat[QUAT_X]*quat[QUAT_Z] , 1 - 2*SQ(quat[QUAT_Y]) - 2*SQ(quat[QUAT_Z]));
    attitude = asin(2*test);
    bank = atan2(2*quat[QUAT_X]*quat[QUAT_W]-2*quat[QUAT_Y]*quat[QUAT_Z] , 1 - 2*SQ(quat[QUAT_X]) - 2*SQ(quat[QUAT_Z]));
  }

  if (x) { *x = bank;     }
  if (y) { *y = heading;  }
  if (z) { *z = attitude; }
  return 0;
}

/*
[PUBLIC] Invert a quaternion i.e. negate the X/Y/Z components.
*/

int quatInvert(quaternion_t quaternion)
{
  if (!quaternion) { return -1; }

  quaternion[QUAT_X] *= -1;
  quaternion[QUAT_Y] *= -1;
  quaternion[QUAT_Z] *= -1;

  return 0;
}

/*
[PUBLIC] Multiply one quaternion by another.
*/

int quatMultiply(quaternion_t q3, const quaternion_t q1, const quaternion_t q2)
{
  if (!q3 || !q1 || !q2) { return -1; }

  q3[QUAT_W] = q1[QUAT_W]*q2[QUAT_W] - q1[QUAT_X]*q2[QUAT_X] - q1[QUAT_Y]*q2[QUAT_Y] - q1[QUAT_Z]*q2[QUAT_Z];
  q3[QUAT_X] = q1[QUAT_W]*q2[QUAT_X] + q1[QUAT_X]*q2[QUAT_W] + q1[QUAT_Y]*q2[QUAT_Z] - q1[QUAT_Z]*q2[QUAT_Y];
  q3[QUAT_Y] = q1[QUAT_W]*q2[QUAT_Y] - q1[QUAT_X]*q2[QUAT_Z] + q1[QUAT_Y]*q2[QUAT_W] + q1[QUAT_Z]*q2[QUAT_X];
  q3[QUAT_Z] = q1[QUAT_W]*q2[QUAT_Z] + q1[QUAT_X]*q2[QUAT_Y] - q1[QUAT_Y]*q2[QUAT_X] + q1[QUAT_Z]*q2[QUAT_W];

  return 0;
}

/*
[PUBLIC] Rotate an XYZ position using a quaternion.
*/

int quatRotate(quaternion_t output, const quaternion_t p, quaternion_t r)
{
  quaternion_t temp;

  if (!output || !p || !r) { return -1; }

  quatMultiply(temp, r, p);
  quatInvert(r);
  quatMultiply(output, temp, r);
  quatInvert(r);

  return 0;
}

/*
[PUBLIC] Determine whether a ray intersects the specified triangle.

This function uses the Möller–Trumbore intersection algorithm.

REFERENCE:

https://en.wikipedia.org/wiki/Möller-Trumbore_algorithm
http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/
*/

float rayHitsTriangle(const vector3_t p, const vector3_t dir, const vector3_t v0, const vector3_t v1, const vector3_t v2)
{
  vector3_t edge1, edge2, cross1, cross2, tmp;
  float dot, u, v;

  vecSubtract(edge1, v1, v0);
  vecSubtract(edge2, v2, v0);

  vecCrossProduct(cross1, dir, edge2);
  dot = vecDotProduct(edge1, cross1);
  if (dot > -0.00001 && dot < 0.00001) { return -1; }

  vecSubtract(tmp, p, v0);
  u = (1/dot) * vecDotProduct(tmp, cross1);
  if (u < 0 || u > 1.0) { return -1; }

  vecCrossProduct(cross2, tmp, edge1);
  v = (1/dot) * vecDotProduct(dir, cross2);
  if (v < 0 || u + v > 1.0) { return -1; }

  return (1/dot) * vecDotProduct(edge2, cross2);
}
