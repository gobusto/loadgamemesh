/**
@file
@brief Utilities for handling MD3 tags, MS3D bones, MD5Mesh joints, etc.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __LGM_TAG_H__
#define __LGM_TAG_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../shared/typedefs.h"
#include "util/quaternion.h"

/**
@brief Defines the maximum length of a tag's name string.

@todo Use dynamic memory allocation instead.
*/

#define MESH_MAXTAGNAME 64

/**
@brief Defines a single MD3 tag, MS3D bone, or MD5Mesh joint.

This structure is generally referred to as a "tag" throughout the code for two
reasons: (A) MD3 support was implemented first, and (B) "joint" and "bone" are
sometimes a bit misleading. For example, MilkShape3D "bones" are actually more
like joints most of the time, but "root" (or "parentless") bones aren't, since
they don't connect the "child" to anything else.
*/

typedef struct
{

  char name[MESH_MAXTAGNAME]; /**< @brief A unique name string for this tag. */
  lgm_index_t parent;         /**< @brief The "parent" tag index; -1 = None. */

  vector3_t position; /**< @brief The XYZ position of the tag.              */
  vector3_t rotation; /**< @brief The XYZ euler rotation angles of the tag. */

  /*
  TODO: Perhaps split these out into a separate structure...? They're only used
  for the bind-pose skeleton, so individual skeletal animation frames won't use
  them...
  */

  vector3_t xyzmin; /**< @brief AABB data; defines the minimum XYZ position. */
  vector3_t xyzmax; /**< @brief AABB data; defines the maximum XYZ position. */

} mesh_tag_s;

/**
@brief Stores a list of tag structures.

For skeletal meshes, this can be used to represent a single frame of animation.
*/

typedef struct
{

  lgm_count_t num_tags; /**< @brief The number of tags in the `tag[]` list. */
  mesh_tag_s **tag;     /**< @brief An array of `mesh_tag_s` structures.    */

} mesh_tag_set_s;

/**
@brief Create a new tag-set structure.

@param num_tags The number of tags that the tag-set should contain.
@return A new mesh structure on success, or `NULL` on failure.
*/

mesh_tag_set_s *meshTagSetCreate(lgm_count_t num_tags);

/**
@brief Destroy a previously-created tag-set structure.

@param tag_set The tag-set structure to be freed from memory.
@return Always returns `NULL`.
*/

mesh_tag_set_s *meshTagSetDelete(mesh_tag_set_s *tag_set);

/**
@brief Convert relatively-positioned tags to be absolutely-positioned.

@param tag_set The tag-set structure to update.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t meshTagSetMakeAbsolute(mesh_tag_set_s *tag_set);

#ifdef __cplusplus
}
#endif

#endif /* __LGM_TAG_H__ */
