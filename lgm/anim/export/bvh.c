/**
@file
@brief Handles export of BioVision Hierarchy .BVH files.

<http://www.character-studio.net/bvh_file_specification.htm>

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "export.h"

/**
@brief _writeme_

@param out about
@param indent about
*/

static void animSaveBVH_indent(FILE *out, int indent)
{
  for (; indent; --indent) { fputc('\t', out); }
}

/**
@brief _writeme_

@param out about
@param tag_set about
@param parent_id about
@param indent about
@param motion about
*/

static void animSaveBVH_hierarchy(
  FILE *out,
  mesh_tag_set_s *tag_set,
  lgm_index_t parent_id,
  int indent,
  lgm_bool_t motion
)
{
  const mesh_tag_s *parent = parent_id < 0 ? NULL : tag_set->tag[parent_id];
  lgm_index_t tag_id;
fprintf(stderr, "LOOKING FOR CHILDREN OF TAG %ld\n", parent_id);
  for (tag_id = 0; tag_id < tag_set->num_tags; ++tag_id)
  {
    const mesh_tag_s *tag = tag_set->tag[tag_id];
    vector3_t rot;
    vector3_t pos;

    if (tag->parent != parent_id) { continue; }

    pos[0] = tag->position[0];
    pos[1] = tag->position[1];
    pos[2] = tag->position[2];

    rot[0] = tag->rotation[0];
    rot[1] = tag->rotation[1];
    rot[2] = tag->rotation[2];

    if (parent)
    {
      quaternion_t a, b, c;

      pos[0] -= parent->position[0];
      pos[1] -= parent->position[1];
      pos[2] -= parent->position[2];

      quatFromEuler(a, rot[0], rot[1], rot[2]);
      quatFromEuler(b, parent->rotation[0], parent->rotation[1], parent->rotation[2]);
      quatRotate(c, b, a);
      quatAsEuler(c, &rot[0], &rot[1], &rot[2]);
    }

    if (motion)
    {
      indent = 1; /* <-- Child joints + any other "root" joints need spaces. */
      fprintf(out, "%s%f\t%f\t%f\t%f\t%f\t%f", indent ? "\t" : "",
        pos[0],
        pos[1],
        pos[2],
        rot[0] * RAD2DEG,
        rot[2] * RAD2DEG,
        rot[1] * RAD2DEG
      );
      animSaveBVH_hierarchy(out, tag_set, tag_id, indent, LGM_TRUE);
    }
    else
    {
      animSaveBVH_indent(out, indent);
      fprintf(out, "%s ", parent ? "JOINT" : "ROOT");
      fprintf(out, "%s\n", tag->name && tag->name[0] ? tag->name : "default");
      animSaveBVH_indent(out, indent);
      fprintf(out, "{\n");
      animSaveBVH_indent(out, indent + 1);
      fprintf(out, "OFFSET %f %f %f\n", pos[0], pos[1], pos[2]);
      animSaveBVH_indent(out, indent + 1);
      fprintf(out, "CHANNELS 6 Xposition Yposition Zposition Zrotation Xrotation Yrotation\n");
      animSaveBVH_hierarchy(out, tag_set, tag_id, indent + 1, LGM_FALSE);
      animSaveBVH_indent(out, indent);
      fprintf(out, "}\n");
    }
  }
}

/**
@brief Export a single skeletal animation sequence to a BioVision BVH file.

@param anim The animation sequence to be exported.
@param file_name The name (and path) of the output file.
@return `LGM_TRUE` on success, or `LGM_FALSE` on failure.
*/

lgm_bool_t animSaveBVH(const anim_s *anim, const char *file_name)
{
  FILE *out;
  lgm_index_t f;

  if (!anim || !file_name || !animIsSkeletal(anim)) return LGM_FALSE;
  out = fopen(file_name, "wb");
  if (!out) return LGM_FALSE;

  fprintf(out, "HIERARCHY\n");
  animSaveBVH_hierarchy(out, anim->frame[0], -1, 0, LGM_FALSE);
  fprintf(out, "MOTION\n");
  fprintf(out, "Frames: %ld\n", anim->frame_count);
  fprintf(out, "Frame Time: %f\n", anim->fps > 0 ? 1/anim->fps : 1);

  for (f = 0; f < anim->frame_count; ++f)
  {
    animSaveBVH_hierarchy(out, anim->frame[f], -1, 0, LGM_TRUE);
    fprintf(out, "\n");
  }

  fclose(out);
  return LGM_TRUE;
}
