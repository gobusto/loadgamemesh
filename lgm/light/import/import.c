/**
@file
@brief Various functions related to importing lights.

Copyright (C) 2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "import.h"

/*
[PUBLIC] Load a light set from a text string.
*/

light_set_s *lightLoadText(const char *text) { return lightLoadTXT(text); }

/*
[PUBLIC] Load a light set from a binary data buffer.
*/

light_set_s *lightLoadData(size_t length, const unsigned char *data)
{
  if (length || data) { /* Silence "unused parameter" warnings... */ }
  return NULL;
}

/*
[PUBLIC] Load a light set from a file.
*/

light_set_s *lightLoadFile(const char *file_name)
{
  light_set_s  *light_set = NULL;
  unsigned char *data = NULL;
  long length = 0;

  data = txtLoadFile(file_name, &length);
  light_set = lightLoadData((size_t)length, data);
  if (!light_set) { light_set = lightLoadText((const char*)data); }

  free(data);
  return light_set;
}
