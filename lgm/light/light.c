/**
@file
@brief General light-related functions.

Copyright (C) 2014-2017 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "light.h"

/*
[PUBLIC] Create a new light set structure.
*/

light_set_s *lightSetCreate(lgm_count_t num_items)
{
  light_set_s *light_set = NULL;

  if (num_items < 1) { return NULL; }

  light_set = (light_set_s*)malloc(sizeof(light_set_s));
  if (!light_set) { return NULL; }
  memset(light_set, 0, sizeof(light_set_s));

  light_set->item = (light_s**)malloc(sizeof(light_s*) * num_items);
  if (!light_set->item) { return lightSetDelete(light_set); }
  memset(light_set->item, 0, sizeof(light_s*) * num_items);

  light_set->num_items = num_items;
  return light_set;
}

/*
[PUBLIC] Delete a previously-created light set structure.
*/

light_set_s *lightSetDelete(light_set_s *light_set)
{
  if (!light_set) { return NULL; }

  if (light_set->item)
  {
    lgm_index_t i = 0;
    for (; i < light_set->num_items; ++i) { lightDelete(light_set->item[i]); }
    free(light_set->item);
  }

  free(light_set);
  return NULL;
}

/*
[PUBLIC] Create a new light structure.
*/

light_s *lightCreate(float x, float y, float z, float radius, lgm_count_t num_rgb)
{
  light_s *light = NULL;

  if (radius < 0 || num_rgb < 1) { return NULL; }

  /* Allocate the basic light structure. */

  light = (light_s*)malloc(sizeof(light_s));
  if (!light) { return NULL; }
  memset(light, 0, sizeof(light_s));

  /* Set up various parameters, with reasonable defaults used where required. */

  light->angle    = 45;     /* Default, in case the spotlight flag is used. */
  light->radius   = radius; /* Can't be defaulted; we don't know the units. */
  light->joint_id = -1;     /* As a default, assume lights are independent. */

  light->origin[0] = x;
  light->origin[1] = y;
  light->origin[2] = z;

  light->target[0] = x;
  light->target[1] = y;
  light->target[2] = z - 1;   /* Assuming positive Z = "up", point downwards. */

  light->fps       = 1;       /* Assume the RGB value cycles once per second. */
  light->num_rgb   = num_rgb; /* See below - RGB values are black by default. */

  /* Allocate memory for the various RGB "animation" cycles. */

  light->rgb = (float*)malloc(sizeof(float) * num_rgb * 3);
  if (!light->rgb) { return lightDelete(light); }
  memset(light->rgb, 0, sizeof(float) * num_rgb * 3);

  /* Return the result. */

  return light;
}

/*
[PUBLIC] Delete a previously-created light structure.
*/

light_s *lightDelete(light_s *light)
{
  if (!light) { return NULL; }
  free(light->rgb);
  free(light);
  return NULL;
}
