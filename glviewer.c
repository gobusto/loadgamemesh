/**
@file
@brief A simple model viewing program, using OpenGL 1.x and SDL 2.x

This is not part of the actual mesh loading code; it's for testing purposes.
*/

#ifdef __APPLE__
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
#endif

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lgm/mesh/mesh.h"

/* Global objects and variables. */

const int gfx_w = 640;  /**< @brief The width of the window in pixels.  */
const int gfx_h = 480;  /**< @brief The height of the window in pixels. */

SDL_Window    *sys_window   = NULL; /**< @brief The main SDL program window. */
SDL_GLContext *sys_context  = NULL; /**< @brief An OpenGL rendering context. */
mesh_s        *gfx_mesh     = NULL; /**< @brief The mesh to be drawn.        */
wordtree_s    *gfx_anim_set = NULL; /**< @brief Animation set for the mesh.  */
anim_s        *gfx_anim     = NULL; /**< @brief Current skeletal animation.  */

/**
@brief Per-animation callback to offset the joints in an animation set.

@param name The name of the animation sequence.
@param data The `anim_s` structure representing a single animation.
@param user The `float[3]` XYZ offset.
@return Always returns the original value of the `data` parameter.
*/

static void *offsetAnimation(const char *name, void *data, void *user)
{
  const anim_s *anim = (anim_s*)data;
  const float *xyz = (float*)user;
  lgm_index_t f, t, axis;

  if (name) { /* Silence "unused parameter" warning. */ }

  if (animIsSkeletal(anim))
    for (f = 0; f < anim->frame_count; ++f)
      for (t = 0; t < anim->frame[f]->num_tags; ++t)
        for (axis = 0; axis < 3; ++axis)
          anim->frame[f]->tag[t]->position[axis] -= xyz[axis];

  return data;
}

/**
@brief Calculate the current/next animation frame indices.

@param num_frames The total number of animation frames possible.
@param index1 The current morph-based animation frame.
@param index2 The next morph-based animation frame.
@return Interpolation between the two frames.
*/

static float getFrames(lgm_count_t num_frames, lgm_index_t *a, lgm_index_t *b)
{
  const Uint32 ticks = SDL_GetTicks();
  const long frame_delay = 80;

  if (!a || !b) { return -1.0; }

  *a = (ticks / frame_delay) % num_frames;
  *b = *a + 1;
  if (*b >= num_frames) { *b = 0; }

  return (ticks % frame_delay) / (float)frame_delay;
}

/**
@brief Shut everything down again.

Call this just before the progam ends.
*/

static void sysQuit(void)
{
  if (!SDL_WasInit(0)) { return; }

  animSetDelete(gfx_anim_set);
  meshDelete(gfx_mesh);

  SDL_GL_DeleteContext(sys_context);
  sys_context = NULL;

  SDL_DestroyWindow(sys_window);
  sys_window = NULL;

  SDL_Quit();
}

/**
@brief Initialise everything.

Call this first.

@param argc Length of the argv array.
@param argv List of program argument strings.
@return Zero on success, or non-zero on failure.
*/

static int sysInit(int argc, char *argv[])
{
  float xyz[3];

  if (argc) { /* Silence "unused parameter" warnings. */ }

  sysQuit();

  gfx_mesh = meshLoadFile(argv[1]);
  if (!gfx_mesh)
  {
    SDL_ShowSimpleMessageBox(
      SDL_MESSAGEBOX_ERROR,
      "Error",
      "Couldn't load mesh file.",
      NULL
    );
    return 123;
  }

  gfx_anim_set = animSetCreate();
  if (!gfx_anim_set)
  {
    SDL_ShowSimpleMessageBox(
      SDL_MESSAGEBOX_ERROR,
      "Error",
      "Couldn't create animation set.",
      NULL
    );
    return 123;
  }
  animLoadFile(gfx_anim_set, "default", argv[1]);

  meshGetCenter(gfx_mesh, xyz);
  meshTranslate(gfx_mesh, -xyz[0], -xyz[1], -xyz[2]);
  treeEachItem(gfx_anim_set, offsetAnimation, xyz);

  gfx_anim = animSetFind(gfx_anim_set, "default");

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
  {
    SDL_ShowSimpleMessageBox(
      SDL_MESSAGEBOX_ERROR,
      "Error",
      "Couldn't initialise SDL.",
      NULL
    );
    return -1;
  }

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, SDL_TRUE);

  sys_window = SDL_CreateWindow("glviewer", 40, 40, gfx_w, gfx_h, SDL_WINDOW_OPENGL);
  if (!sys_window)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
        "Error", "Couldn't create a window.", NULL);
    return -2;
  }

  sys_context = SDL_GL_CreateContext(sys_window);
  if (!sys_context)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
        "Error", "Couldn't create an OpenGL context.", NULL);
    return -3;
  }

  glCullFace(GL_FRONT);
  glEnable(GL_LIGHT0);
  glEnable(GL_CULL_FACE);
  glEnable(GL_NORMALIZE);

  return 0;
}

/**
@brief Draw a 3D mesh.

@param index1 The current morph-based animation frame.
@param index2 The next morph-based animation frame.
@param lerp Interpolation between the two frames.
*/

static void gfxDrawMesh(lgm_index_t index1, lgm_index_t index2, float lerp)
{
  lgm_index_t g;

  for (g = 0; g < gfx_mesh->num_groups; ++g)
  {
    const mesh_group_s *group = gfx_mesh->group[g];
    lgm_index_t t;

    /* NOTE: These values are based on the OpenGL defaults: */
    float ambient[4] = { 0.2, 0.2, 0.2, 1.0 };
    float diffuse[4] = { 0.8, 0.8, 0.8, 1.0 };
    float specular[4] = { 0.0, 0.0, 0.0, 1.0 };
    float emission[4] = { 0.0, 0.0, 0.0, 1.0 };
    float shininess = 0.0;

    if (!group) { continue; }

    if (gfx_mesh->mtrl && group->mat_id >= 0 && group->mat_id < gfx_mesh->mtrl->num_items)
    {
      const mtrl_s *m = gfx_mesh->mtrl->item[group->mat_id];

      ambient[0] = m->ambient[0];
      ambient[1] = m->ambient[1];
      ambient[2] = m->ambient[2];

      diffuse[0] = m->diffuse[0];
      diffuse[1] = m->diffuse[1];
      diffuse[2] = m->diffuse[2];

      specular[0] = m->specular[0];
      specular[1] = m->specular[1];
      specular[2] = m->specular[2];

      emission[0] = m->emission[0];
      emission[1] = m->emission[1];
      emission[2] = m->emission[2];

      shininess = m->shininess;
    }

    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
    glMaterialfv(GL_FRONT, GL_EMISSION, emission);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
    glMaterialf(GL_FRONT, GL_SHININESS, shininess);

    glBegin(GL_TRIANGLES);
    for (t = 0; t < group->num_tris; ++t)
    {
      const mesh_tri_s *tri = group->tri[t];
      lgm_index_t v;

      for (v = 0; v < 3; ++v)
      {
        float norm[3];
        float vert[3];
        int i;

        for (i = 0; i < 3; ++i)
        {
          norm[i] = gfx_mesh->normal[index1][(tri->n_id[v] * 3) + i] + (
                    gfx_mesh->normal[index2][(tri->n_id[v] * 3) + i] -
                    gfx_mesh->normal[index1][(tri->n_id[v] * 3) + i]) * lerp;
          vert[i] = gfx_mesh->vertex[index1][(tri->v_id[v] * 3) + i] + (
                    gfx_mesh->vertex[index2][(tri->v_id[v] * 3) + i] -
                    gfx_mesh->vertex[index1][(tri->v_id[v] * 3) + i]) * lerp;
        }

        glNormal3f(norm[0], norm[1], norm[2]);
        glVertex3f(vert[0], vert[1], vert[2]);
      }
    }
    glEnd();
  }
}

/**
@brief Draw a set of skeletal joints.

@param a The first set of skeletal data.
@param b The second set of skeletal data.
@param lerp Interpolation between the two frames.
*/

static void gfxDrawSkeleton(mesh_tag_set_s *a, mesh_tag_set_s *b, float lerp)
{
  lgm_index_t i;

  /* Bones: */
  glLineWidth(2);
  glColor3ub(0, 255, 255);
  glBegin(GL_LINES);
  for (i = 0; i < a->num_tags; ++i)
  {
    lgm_index_t j = a->tag[i]->parent;
    if (j < 0) { continue; }

    glVertex3f(a->tag[i]->position[0], a->tag[i]->position[1], a->tag[i]->position[2]);
    glVertex3f(a->tag[j]->position[0], a->tag[j]->position[1], a->tag[j]->position[2]);
  }
  glEnd();

  /* Joints: */
  glPointSize(4);
  glColor3ub(255, 0, 0);
  glBegin(GL_POINTS);
  for (i = 0; i < a->num_tags; ++i)
  {
    glVertex3f(a->tag[i]->position[0], a->tag[i]->position[1], a->tag[i]->position[2]);
  }
  glEnd();
}

/**
@brief Enter the main game loop.

This will run until a termination event occurs.
*/

static void sysMainLoop(void)
{
  while (SDL_TRUE)
  {
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
      if (event.type == SDL_QUIT) { return; }
    }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(55.0, (float)gfx_w / (float)gfx_h, 1, 2048);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    {
      float distance;
      if (gfx_mesh->xyzmax[0] - gfx_mesh->xyzmin[0] > gfx_mesh->xyzmax[1] - gfx_mesh->xyzmin[1])
        distance = (gfx_mesh->xyzmax[0] - gfx_mesh->xyzmin[0]) * 0.5;
      else
        distance = (gfx_mesh->xyzmax[1] - gfx_mesh->xyzmin[1]) * 0.5;
      glTranslatef(0, 0, -distance * 5.0);
    }

    glRotatef(-90, 1, 0, 0);
    glRotatef(SDL_GetTicks() / 50.0, 0, 0, 1);

    #if 0
    {
      lgm_index_t i;

      glPointSize(1);
      glColor3ub(255, 255, 255);
      glBegin(GL_POINTS);
      for (i = 0; i < gfx_mesh->num_verts; ++i)
      {
        glVertex3f(
          gfx_mesh->vertex[0][(i * 3) + 0],
          gfx_mesh->vertex[0][(i * 3) + 1],
          gfx_mesh->vertex[0][(i * 3) + 2]
        );
      }
      glEnd();
    }
    #else
    {
      lgm_index_t index1, index2;
      float lerp;

      lerp = getFrames(gfx_mesh->num_frames, &index1, &index2);

      if (!gfx_anim)
      {
        char temp[512];
        sprintf(temp, "Morph (%ld/%ld)", index1 + 1, gfx_mesh->num_frames);
        SDL_SetWindowTitle(sys_window, temp);
      }

      glEnable(GL_DEPTH_TEST);
      glEnable(GL_LIGHTING);
      gfxDrawMesh(index1, index2, lerp);
      glDisable(GL_DEPTH_TEST);
      glDisable(GL_LIGHTING);
    }
    #endif

    if (gfx_mesh->tag_list || gfx_anim)
    {
      mesh_tag_set_s *this_frame;
      mesh_tag_set_s *next_frame;
      lgm_index_t index1, index2;
      float lerp;

      if (gfx_anim)
      {
        lerp = getFrames(gfx_anim->frame_count, &index1, &index2);
        this_frame = gfx_anim->frame[index1];
        next_frame = gfx_anim->frame[index2];
      }
      else
      {
        lerp = 0;
        this_frame = gfx_mesh->tag_list;
        next_frame = gfx_mesh->tag_list;
      }

      if (gfx_anim)
      {
        char temp[512];
        sprintf(temp, "Skeletal (%ld/%ld)", index1 + 1, gfx_anim->frame_count);
        SDL_SetWindowTitle(sys_window, temp);
      }

      gfxDrawSkeleton(this_frame, next_frame, lerp);
    }

    SDL_GL_SwapWindow(sys_window);
    SDL_Delay(1); /* <-- Thread sleep (prevents constant 100% CPU usage). */
  }
}

/**
@brief The program entry point.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int main(int argc, char **argv)
{
  if (sysInit(argc, argv) == 0) { sysMainLoop(); }
  sysQuit();
  return 0;
}
