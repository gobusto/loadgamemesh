Generic game model loader
=========================

This code is designed to load various 3D mesh formats into a common structure,
allowing them to be rendered in a uniform way by game engines or similar. It's
been written and restructured over the course of several years for my own game
projects, but has now been split out into a separate project so that it's easy
to keep track of.

All of this code was written from scratch by me - no other libraries are needed
for compilation.

How do I use it?
----------------

This code is mainly aimed at games progammers who just want to get a model file
into their engine somehow, so that they can draw it with Direct3D/OpenGL/other.
Simply add the files to your project and call the `meshLoadFile()` function; it
will return a pointer to a `mesh_s` stucture which describes the vertex/polygon
data in a generic, non-file-format-specific way. Once you're finished with it,
call `meshDelete()` to delete everything again.

Note that this code doesn't draw anything, or load texture image files - you'll
need to sort those out for yourself. The aim is to load the actual *model* data
into a common structure so that you don't need to write separate rendering code
for MD2, OBJ, and 3DS files.

Alternatively, you could simply use it as a conversion tool - the `xxx2obj.cbp`
Code::Blocks project included with the code demonstrates a simple, command-line
interface for loading models in any supported format and exporting them to OBJ.
Usage is as follows:

`thomas@debian-laptop:~/loadgamemesh/bin$ ./xxx2obj TRUCK.BIN out.obj out.mtl`

Supported Import/Export Formats
-------------------------------

The following formats can be loaded:

+ 3D Studio 3DS.
+ Dark Engine BIN (used for models in Thief 1, Thief 2, and System Shock 2).
+ id/Valve BSP. Currently, Quake 1, Quake 2, and Half-Life 1 are supported.
+ Quake 1 MDL, including morph-based animations.
+ Quake 2 MD2, including morph-based animations.
+ Quake 3 MD3, including morph-based animations.
+ Doom 3 MD5mesh. Note that MD5anim and MD5camera are not currently supported.
+ Milkshape3D MS3D, including skeletal data.
+ Wavefront OBJ models.
+ Princeton University OFF models.
+ MikuMikuDance PMD character models (mesh only; no animations).
+ Red Faction 1 RFM (static) and RFC (character) models.
+ Red Faction 1 RFL (map geometry).
+ Binary STL (Stereolithographic triangle) files; ASCII ones aren't supported.

It's also possible to export models in a few formats:

+ Milkshape3D MS3D (no skeletal data yet)
+ Wavefront OBJ
+ Princeton University OFF
+ Stanford University PLY
+ Binary STL

It's also likely that more import/export formats will be added in the future...

Extras
------

In addition to importing/exporting files, it's possible to:

+ Resize or reposition models.
+ Generate (or invert) per-vertex normals.
+ Reverse the winding (or "facing") of triangles.
+ Load/save Wavefront .MTL or Quake III .SKIN files.
+ Load/save Quake III .CFG (animation) files.

There's also some miscellaneous goodies in the `util/` subdirectory for working
with quaternions, or "associative arrays" with string keys and quick look-ups.

Whilst most model formats were written based on specifications created by other
people, several were previously undocumented and were reverse-engineered by me.
In these cases, I've tried to document my discoveries in the `docs/` directory,
though there are a few cases (such as Dark Engine .BIN files) where the code in
the main `mesh/` directory is the only "documentation" available - sorry!

TODO
----

Things I plan to (maybe) get around to at some point...

+ Remove any empty groups from the mesh after it's been loaded.
+ Incorporate some of the additional formats in `docs/` into the main codebase.
+ Implement PLY import (export is already done).
+ Support AC3D: <http://paulbourke.net/dataformats/ac3d/>
+ Support X: <http://paulbourke.net/dataformats/directx/>
+ Support IQM: <http://sauerbraten.org/iqm/iqm.txt>
+ Support IQE: <http://sauerbraten.org/iqm/iqe.txt>
+ Support MM3D: <http://www.misfitcode.com/misfitmodel3d/olh_mm3dformat.html>
+ Support LWO. LWO files have skeleton data, which is useful.
+ Support Quake 3 and Source BSP files + fix texture coords in Quake II files.
+ Support Dark Engine MIS files...?

### Skeletal Animation

Skeletal animation support is still very new (read: buggy and unfinished) right
now; this will hopefully get better in the future...

+ Fix any bugs + ensure that import/export works properly for all formats.
+ Support BVH import (export is already done).
+ Support MS3D export (import is already done).
+ Support Doom 3 *.md5mesh (both import and export).

TODON'T
-------

Things I (probably) *won't* do:

+ Support COLLADA .dae files; they seem too complex to be worth bothering with.
+ Support VRML or X3D files (for much the same reasons as COLLADA files).
+ Support .blend/.max/.c4d/etc. as these are constantly changing.
+ Support import of FBX files (in the main codebase), due to relying on `zlib`.

Anything that would introduce a dependency on a third-party library will not be
implemented (or at least, not in the *main* codebase; utils in `docs/` are OK).

Licensing
---------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
license - you may use or modify it for any purpose, including closed-source and
paid-for projects. All I ask in return is proper attribution (i.e. don't remove
my name from the copyright text in the source code, and maybe include me on the
"credits" screen or whatever equivalent your program has).
