/**
@file
@brief Load one or more files containing animations, and output them as text.

This is not part of the actual mesh loading code; it's for testing purposes.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lgm/anim/anim.h"

/**
@brief Per-animation callback function.

@param name The name of the animation sequence.
@param data The `anim_s` structure representing a single animation.
@param user Not used; always `NULL`.
@return Always returns the original value of the `data` parameter.
*/

static void *animationInfo(const char *name, void *data, void *user)
{
  const anim_s *anim = (anim_s*)data;

  if (user) { /* Silence "unused parameter" warnings... */ }

  printf("'%s' (%ld frames @ %f FPS, starting at %ld)\n", name, anim->frame_count, anim->fps, anim->frame_start);

  if (animIsSkeletal(anim))
  {
    char *file_name;
    lgm_index_t f, t;

    file_name = malloc(strlen(name) + 9);
    if (file_name)
    {
      sprintf(file_name, "%s_out.bvh", name);
      if (animSaveBVH(anim, file_name))
        printf("\t[INFO] Saved as %s\n", file_name);
      else
        printf("\t[WARNING] Could not save to file...\n");
    }
    free(file_name);

    printf("\tThis is a skeletal animation; skeletal information follows...\n");
    for (f = 0; f < anim->frame_count; ++f)
    {
      const mesh_tag_set_s *tag_set = anim->frame[f];
      printf("\tFrame %ld: (%ld tags)\n", f, tag_set->num_tags);
      for (t = 0; t < tag_set->num_tags; ++t)
      {
        const mesh_tag_s *tag = tag_set->tag[t];
        printf("\t\tTag %ld: %s (parent = %ld)\n", t, tag->name, tag->parent);
        printf("\t\t\tPosition: %f, %f, %f\n", tag->position[0], tag->position[1], tag->position[2]);
        printf("\t\t\tRotation: %f, %f, %f\n", tag->rotation[0], tag->rotation[1], tag->rotation[2]);
      }
    }
  }
  else
  {
    printf("\tThis is a morph-based animation; no skeletal data available.\n");
  }

  return data;
}

/**
@brief The program entry point.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int main(int argc, char **argv)
{
  int i;
  wordtree_s *anim_set = animSetCreate();

  if (!anim_set) { fprintf(stderr, "animSetCreate() failed!\n"); return 1; }

  fprintf(stderr, "%d files\n", argc - 1);
  for (i = 1; i < argc; ++i)
  {
    lgm_count_t num_anims = animLoadFile(anim_set, argv[i], argv[i]);

    printf("> %s: ", argv[i]);
    if (num_anims < 0) { printf("Not an animations file...\n"); }
    else               { printf("%ld animations\n", num_anims); }
  }

  printf("Found %ld morph-based animations\n", animSetMorphCount(anim_set));
  printf("Found %ld skeletal animations\n", animSetSkeletalCount(anim_set));

  treeEachItem(anim_set, animationInfo, NULL);

  if (animSetDelete(anim_set)) { fprintf(stderr, "animSetDelete() didn't return NULL!\n"); }
  return 0;
}
