/**
@file
@brief A simple program for converting Red Faction RFC files into OBJ format
*/

#include <stdio.h>
#include <stdlib.h>

/* Constants: */

#define RFC_4CC 0x87128712 /**< @brief Magic number for Volition RFC files. */

#define RFC_CHUNK_BONE 0x424f4e45 /**< @brief Describes the bones hierarchy. */
#define RFC_CHUNK_DUMB 0x44554d42 /**< @brief Not sure. Maybe MD3-like tags? */
#define RFC_CHUNK_DATA 0x87251110 /**< @brief Contains the actual mesh data. */
#define RFC_CHUNK_SKIN 0x11133344 /**< @brief Lists the textures file names. */
#define RFC_CHUNK_CSPH 0x43535048 /**< @brief Collision sphere descriptions. */
#define RFC_CHUNK_NULL 0x00000000 /**< @brief Indicates EOF. Length is zero. */

float read_float32(FILE *src)
{
  float f;
  ((unsigned char*)&f)[0] = fgetc(src);
  ((unsigned char*)&f)[1] = fgetc(src);
  ((unsigned char*)&f)[2] = fgetc(src);
  ((unsigned char*)&f)[3] = fgetc(src);
  return f;
}

unsigned short read_ui16(FILE *src)
{
  unsigned short i = (unsigned short)fgetc(src);
  i += (unsigned short)fgetc(src) << 8;
  return i;
}

long read_si32(FILE *src)
{
  long i = fgetc(src);
  i += fgetc(src) << 8;
  i += fgetc(src) << 16;
  i += fgetc(src) << 24;
  return i;
}

unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  i += (unsigned long)fgetc(src) << 24;
  return i;
}

void describe_CSPH(FILE *src, unsigned long version)
{
  char name[24];
  long parent;
  float data[4];

  if (version) { /* Ignored; this doesn't make any difference... */ }

  fread(name, 1, 24, src);
  parent = read_si32(src);
  data[0] = read_float32(src);
  data[1] = read_float32(src);
  data[2] = read_float32(src);
  data[3] = read_float32(src);

  fprintf(stderr, "CSPH\n\t%s (parent: %ld)\n\t%f %f %f %f\n",
    name, parent, data[0], data[1], data[2], data[3]
  );
}

void describe_DUMB(FILE *src, unsigned long version)
{
  char name[24];
  long parent;
  float data[7];

  fread(name, 1, 24, src);

  if (version <= 1)
  {
    parent = read_si32(src);
    data[0] = read_float32(src);
    data[1] = read_float32(src);
    data[2] = read_float32(src);
    data[3] = read_float32(src);
    data[4] = read_float32(src);
    data[5] = read_float32(src);
    data[6] = read_float32(src);

    fprintf(stderr, "DUMB\n\t%s (parent: %ld)\n\t%f %f %f %f %f %f %f\n",
      name, parent, data[0], data[1], data[2], data[3], data[4], data[5], data[6]
    );
  }
  else if (version == 10)
  {
    int i = 0;
    for (i = 24; i < 96; ++i) { fgetc(src); }
    fprintf(stderr, "DUMB\n\t%s (Other data not yet known...)\n", name);
  }
}

void describe_BONE(FILE *src, unsigned long version)
{
  char name[24];
  long parent;
  float data[7];

  fread(name, 1, 24, src);

  if (version <= 1)
  {
    data[0] = read_float32(src);
    data[1] = read_float32(src);
    data[2] = read_float32(src);
    data[3] = read_float32(src);
    data[4] = read_float32(src);
    data[5] = read_float32(src);
    data[6] = read_float32(src);
    parent = read_si32(src);

    fprintf(stderr, "\t%s (parent: %ld)\n\t%f %f %f %f %f %f %f\n",
      name, parent, data[0], data[1], data[2], data[3], data[4], data[5], data[6]
    );
  }
  else if (version == 10)
  {
    int i = 0;
    for (i = 24; i < 76; ++i) { fgetc(src); }
    fprintf(stderr, "BONE\n\t%s (Other data not yet known...)\n", name);
  }
}

void describe_SKIN(FILE *src, unsigned long version)
{
  if (version <= 1)
  {
    char name[80];
    unsigned long flags;

    fread(name, 1, 80, src);
    flags = read_ui32(src);

    fprintf(stderr, "\t%s (flags: %lu)\n", name, flags);
  }
  else if (version == 10)
  {
    int i, c;
    fprintf(stderr, "SKIN\n");

    /* Skip some unknown data... */
    for (i = 0; i < 20; ++i) { fgetc(src); }

    fprintf(stderr, "\tDiffuse: ");
    do {
      c = fgetc(src);
      fputc(c ? c : '\n', stderr);
      ++i;
    } while(c != 0);

    /* Skip some more unknown data... */
    for (; i < 208; ++i) { fgetc(src); }

    fprintf(stderr, "\tReflect: ");
    do {
      c = fgetc(src);
      fputc(c ? c : '\n', stderr);
      ++i;
    } while(c != 0);

    /* Skip some more unknown data... */
    for (; i < 364; ++i) { fgetc(src); }
  }
}

int main(int argc, char *argv[])
{
  FILE *src = NULL;
  unsigned long version;

  /* Open the file. TODO: This could be tidier! */
  if (argc || argv) {}
  src = fopen(argv[1], "rb");
  if (!src) { return 1; }

  /* Ensure that it's actually an RFC file. */
  if (read_ui32(src) != RFC_4CC)
  {
    fprintf(stderr, "This does not appear to be a valid RFM or RFC file.\n");
    fclose(src);
    return 2;
  }

  version = read_ui32(src);
  if (version == 0 || version == 1)
  {
    unsigned long chunk_type, chunk_size;

    fprintf(stderr, "Red Faction (v%lu) RF%c file.\n", version, version > 0 ? 'C' : 'M');
    fprintf(stderr, "NULL chunks: %lu\n", read_ui32(src));  /* Guess; always 1. */
    fprintf(stderr, "SKIN chunks: %lu\n", read_ui32(src));  /* Guess; always 1. */
    fprintf(stderr, "DATA chunks: %lu\n", read_ui32(src));
    fprintf(stderr, "CSPH chunks: %lu\n", read_ui32(src));
    fprintf(stderr, "DUMB chunks: %lu\n", read_ui32(src));
    fprintf(stderr, "Skins count: %lu\n\n", read_ui32(src)); /* Same as SKIN chunk. */

    do
    {
      chunk_type = read_ui32(src);
      chunk_size = read_ui32(src);
      fprintf(stderr, "%08lx (%lu bytes)\n", chunk_type, chunk_size);

      switch (chunk_type)
      {

        case RFC_CHUNK_CSPH: describe_CSPH(src, version); break;

        case RFC_CHUNK_DUMB: describe_DUMB(src, version); break;

        case RFC_CHUNK_BONE:
        {
          unsigned long num_bones, i;
          num_bones = read_ui32(src);
          fprintf(stderr, "BONE (%lu bones)\n", num_bones);
          for (i = 0; i < num_bones; ++i) { describe_BONE(src, version); }
        }
        break;

        case RFC_CHUNK_SKIN:
        {
          unsigned long num_skins, i;
          num_skins = read_ui32(src);
          fprintf(stderr, "SKIN (%lu skins)\n", num_skins);
          for (i = 0; i < num_skins; ++i) { describe_SKIN(src, version); }
        }
        break;

        case RFC_CHUNK_DATA:
        {
          long data_offs, data_size, last_read_position, num_unknown;
          unsigned short num_xxx, x;
          unsigned long  num_tex, t;

          fprintf(stderr, "DATA\n");

          fprintf(stderr, "\tSubtype: %lx\n", read_ui32(src));  /* 7f7fffff */
          fprintf(stderr, "\tVersion: %lu\n", read_ui32(src));  /* Guess; always 5. */
          fprintf(stderr, "\tFlags: %lu\n", read_ui32(src));

          num_unknown = read_ui32(src);
          fprintf(stderr, "\tUnknown Length: %lu\n", num_unknown);

          fprintf(stderr, "\tB.Box +X: %f\n", read_float32(src));
          fprintf(stderr, "\tB.Box +Y: %f\n", read_float32(src));
          fprintf(stderr, "\tB.Box +Z: %f\n", read_float32(src));
          fprintf(stderr, "\tB.Box -X: %f\n", read_float32(src));
          fprintf(stderr, "\tB.Box -Y: %f\n", read_float32(src));
          fprintf(stderr, "\tB.Box -Z: %f\n", read_float32(src));
          fprintf(stderr, "\tOrigin X: %f\n", read_float32(src));
          fprintf(stderr, "\tOrigin Y: %f\n", read_float32(src));
          fprintf(stderr, "\tOrigin Z: %f\n", read_float32(src));
          fprintf(stderr, "\tRadius:   %f\n", read_float32(src));

          data_size = read_ui32(src);
          data_offs = ftell(src);

          /* Skip past the data; we don't know how to interpret it yet... */
          fseek(src, data_size + data_offs, SEEK_SET);

          num_xxx = read_ui16(src);
          fprintf(stderr, "\tNumber of objects: %u\n", num_xxx);

          last_read_position = data_offs;
          for (x = 0; x < num_xxx; ++x)
          {
            unsigned short num_vertices = read_ui16(src);
            unsigned short num_polygons = read_ui16(src);
            unsigned short vertices_size = read_ui16(src);
            unsigned short polygons_size = read_ui16(src);
            unsigned short property_size = read_ui16(src);
            unsigned short r;

            fprintf(stderr, "\t\tObject %d:\n", x);
            fprintf(stderr, "\t\t > Vertices Count:\t%u\n", num_vertices);
            fprintf(stderr, "\t\t > Polygons Count:\t%u\n", num_polygons);
            fprintf(stderr, "\t\t > Vertices Length:\t%u\n", vertices_size);
            fprintf(stderr, "\t\t > Polygons Length:\t%u\n", polygons_size);
            fprintf(stderr, "\t\t > Property Length:\t%u\n", property_size);
  #if 1
            fseek(src, last_read_position, SEEK_SET);

            /* Actual data size: num_vertices*3 * 4. Padded to next 16-bytes. */
            fprintf(stderr, "\t\t\tVertices:\n");
            for (r = 0; r < vertices_size; r += 4)
            { fprintf(stderr, "\t\t\t\t%d: %f\n", r/4, read_float32(src)); }

            /* Actual data size: num_vertices*3 * 4. Padded to next 16-bytes. */
            fprintf(stderr, "\t\t\tNormals:\n");
            for (r = 0; r < vertices_size; r += 4)
            { fprintf(stderr, "\t\t\t\t%d: %f\n", r/4, read_float32(src)); }

            /* No padding is necessary here, as 80 is cleanly divisible by 16. */
            fprintf(stderr, "\t\t\tPolygons:\n");
            for (r = 0; r < polygons_size; r += 80)
            {
              float f[4];
              unsigned long d[4];

              /*
              Note: Each polygon within a group can reference a different texture,
              so unfortunately we can't assume one-texture-per-group if rendering.
              (Some models really do this, by the way - for example, `grab.rfc`.)
              */

              d[0] = read_ui32(src);
              d[1] = read_ui32(src);
              d[2] = read_ui32(src);
              d[3] = read_ui32(src);
              fprintf(stderr, "\t\t\t\t%d: %lu %lu %lu (Skin: %lu)\n", r/80, d[0], d[1], d[2], d[3]);
              f[0] = read_float32(src);
              f[1] = read_float32(src);
              f[2] = read_float32(src);
              d[3] = read_ui32(src);
              fprintf(stderr, "\t\t\t\t\t%f %f %f (%08lx)\n", f[0], f[1], f[2], d[3]);
              f[0] = read_float32(src);
              f[1] = read_float32(src);
              d[2] = read_ui32(src);
              d[3] = read_ui32(src);
              fprintf(stderr, "\t\t\t\t\t%f %f (%08lx %08lx)\n", f[0], f[1], d[2], d[3]);
              f[0] = read_float32(src);
              f[1] = read_float32(src);
              d[2] = read_ui32(src);
              d[3] = read_ui32(src);
              fprintf(stderr, "\t\t\t\t\t%f %f (%08lx %08lx)\n", f[0], f[1], d[2], d[3]);
              f[0] = read_float32(src);
              f[1] = read_float32(src);
              d[2] = read_ui32(src);
              d[3] = read_ui32(src);
              fprintf(stderr, "\t\t\t\t\t%f %f (%08lx %08lx)\n", f[0], f[1], d[2], d[3]);
            }

            /* Actual data size: num_vertices * 8. Padded to next 16-bytes. */
            fprintf(stderr, "\t\t\tProperties:\n");
            for (r = 0; r < property_size; r += 8)
            {
              unsigned short d[4];
              /* TODO: Figure this out; may be BONE/DUMB/CSPH association data? */
              d[0] = read_ui16(src);
              d[1] = read_ui16(src);
              d[2] = read_ui16(src);
              d[3] = read_ui16(src);
              fprintf(stderr, "\t\t\t\t%d: %04x %04x %04x %04x\n", r/8, d[0], d[1], d[2], d[3]);
            }

            /* Not sure what this is; seems to map vertices to vertices(!?)... */
            if (version == 1)
            {
              fprintf(stderr, "\t\t\tUnknown:\n");
              for (r = 0; r < num_unknown; r += 1)
              { fprintf(stderr, "\t\t\t\t%d: %d\n", r, (signed char)fgetc(src));}
              while (r % 16) { fgetc(src); ++r; }
            }

            last_read_position = ftell(src);
            fseek(src, data_size + data_offs + 2 + ((x+1)*10), SEEK_SET);
  #endif
          }

          /*
          This is the "real" list the gets used by polygons; it's a subset of the
          list in the SKIN chunk. So the SKIN chunk seems to be useless...?
          */
          num_tex = read_ui32(src);
          fprintf(stderr, "\tTexture list: %lu\n", num_tex);
          for (t = num_tex; t > 0; --t)
          {
            int a;
            fputc('\t', stderr);
            fputc('\t', stderr);
            do
            {
              a = fgetc(src);
              fputc(a ? a : '\n', stderr);
            } while (a);
          }
        }
        break;

        case RFC_CHUNK_NULL:
          fprintf(stderr, "NULL - End of file\n");
        break;

        default:
          fprintf(stderr, "Unknown chunk type, skipping...\n");
          fseek(src, chunk_size, SEEK_CUR);
        break;

      }

      fputc('\n', stderr);

    } while (chunk_type != 0);

  }
  else if (version == 10)
  {
    unsigned long num_csph, num_dumb, num_bone, num_skin, i;

    fprintf(stderr, "Red Faction 2 (v%lu) RFC file.\n", version);
    fprintf(stderr, "????: %lu\n", read_ui32(src));
    fprintf(stderr, "????: %lu\n", read_ui32(src));
    num_csph = read_ui32(src);
    fprintf(stderr, "CSPH count: %lu\n", num_csph);
    num_dumb = read_ui32(src);
    fprintf(stderr, "DUMB count: %lu\n", num_dumb);
    num_bone = read_ui32(src);
    fprintf(stderr, "BONE count: %lu\n", num_bone);
    num_skin = read_ui32(src);
    fprintf(stderr, "SKIN count: %lu\n", num_skin);
    fprintf(stderr, "????: %lu\n", read_ui32(src));
    fprintf(stderr, "????: %lu\n", read_ui32(src));
    while (ftell(src) % 64) { fgetc(src); }

    for (i = 0; i < num_csph; ++i) { describe_CSPH(src, version); }
    while (ftell(src) % 64) { fgetc(src); }

    for (i = 0; i < num_dumb; ++i) { describe_DUMB(src, version); }
    while (ftell(src) % 64) { fgetc(src); }

    for (i = 0; i < num_bone; ++i) { describe_BONE(src, version); }
    while (ftell(src) % 64) { fgetc(src); }

    for (i = 0; i < num_skin; ++i) { describe_SKIN(src, version); }
    while (ftell(src) % 64) { fgetc(src); }

    #if 0
      for (i = 0; i < 512; ++i)
      {
        int val = fgetc(src);
        /*
        if (i % 364 == 0) { fprintf(stderr, "===============================\n"); }
        */
        fprintf(stderr, "%lu = %d (%c)\n", i, val, val);
      }
    #endif

  }
  else { fprintf(stderr, "Unrecognised RFC version: %lu\n", version); }

  fclose(src);
  return 0;
}
