/**
@file
@brief A simple program for converting PBL mesh files (used by Choro-Q games).

MPD map files are also supported, since they store data in much the same way.

Tested with Seek & Destroy (Shin Combat Choro Q) and Penny Racers (Choro Q HG);
Road Trip Adventure (Choro Q HG 2) doesn't seem to use the PBL/MPD formats.

@todo This code basically works, but it needs tidying up.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Models: */
#define PBL_4CC 0x004C4250  /**< @brief Spells "PBL" in ASCII */
#define PBL_HEAD_SIZE 16  /**< @brief The MINIMUM size of the header. */

/* Maps: */
#define MPD_4CC 0x0044504D  /**< @brief Spells "MPD" in ASCII */
#define MPD_HEAD_SIZE 16
#define MPD_ITEM_SIZE 16

signed short read_si16(FILE *src)
{
  signed short i = (signed short)fgetc(src);
  i += (signed char)fgetc(src) << 8;
  return i;
}

static unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  return i + ((unsigned long)fgetc(src) << 24);
}

static int save_obj(FILE *src, const char *file_name)
{
  FILE *out;

  unsigned long num_mtrls, num_verts, num_norms, num_coords, num_unknowns, i;
  short num_polys, p;

  if (!src || !file_name) { return -1; }
  out = fopen(file_name, "w");
  if (!out) { return -2; }

  num_mtrls = read_ui32(src);
  num_verts = read_ui32(src);
  num_norms = read_ui32(src);
  num_coords = read_ui32(src);
  num_unknowns = read_ui32(src);

  fprintf(stderr, "\t%lu materials\n", num_mtrls);
  fprintf(stderr, "\t%lu vertices\n", num_verts);
  fprintf(stderr, "\t%lu normals\n", num_norms);
  fprintf(stderr, "\t%lu texcoords\n", num_coords);
  fprintf(stderr, "\t%lu UNKNOWNS\n", num_unknowns);
  fprintf(stderr, "\t%lu ?????????\n", read_ui32(src));
  fprintf(stderr, "\t%lu (padding)\n", read_ui32(src)); /* Always zero? */
  fprintf(stderr, "\t%lu (padding)\n", read_ui32(src)); /* Always zero? */

  if (num_mtrls > 0) { fprintf(stderr, "\n\tMATERIALS:\n\t----------\n"); }
  for (i = 0; i < num_mtrls; ++i)
  {
    char name[16];
    short w, h;
    unsigned long bpp, xxx, yyy;

    w   = read_si16(src);
    h   = read_si16(src);
    bpp = read_ui32(src);
    xxx = read_ui32(src); /* Always zero? */
    yyy = read_ui32(src); /* Always zero? */
    fread(name, 1, 16, src);

    fprintf(stderr, "\t%lu: %s (%dx%d, %lubpp): %lu / %lu\n", i, name, w, h, bpp, xxx, yyy);
  }

  for (i = 0; i < num_verts; ++i)
  {
    float xyzw[4];
    fread(&xyzw, 1, 4*4, src);
    fprintf(out, "v %f %f %f\n", xyzw[0], xyzw[1], xyzw[2]);
  }

  for (i = 0; i < num_norms; ++i)
  {
    float xyzw[4];
    fread(&xyzw, 1, 4*4, src);
    fprintf(out, "vn %f %f %f\n", xyzw[0], xyzw[1], xyzw[2]);
  }

  for (i = 0; i < num_coords; ++i)
  {
    float uv[2];
    fread(&uv, 1, 4*2, src);
    fprintf(out, "vt %f %f\n", uv[0], uv[1]);
  }

  for (i = 0; i < num_unknowns; ++i) { read_ui32(src); }

  num_polys = read_si16(src);
  for (p = 0; p < num_polys; ++p)
  {
    short mtrl_id, num_points, v;
    short v_id[3];
    short n_id[3];
    short t_id[3];

    read_si16(src);
    read_si16(src);
    mtrl_id = read_si16(src);
    read_si16(src);
    read_si16(src);
    read_si16(src);
    read_si16(src);
    num_points = read_si16(src);

    for (v = 0; v < num_points; ++v)
    {
      read_si16(src);
      v_id[2] = read_si16(src);
      n_id[2] = read_si16(src);
      t_id[2] = read_si16(src);

      if (v >= 2)
      {
        int a = v % 2 ? 0 : 2;

        fprintf(out, "f");
        for (; a >= 0 && a <= 2; v % 2 ? ++a : --a)
          if (mtrl_id >= 0)
            fprintf(out, " %d/%d/%d", v_id[a] + 1, t_id[a] + 1, n_id[a] + 1);
          else
            fprintf(out, " %d//%d",   v_id[a] + 1,              n_id[a] + 1);
        fprintf(out, "\n");
      }

      v_id[0] = v_id[1];
      n_id[0] = n_id[1];
      t_id[0] = t_id[1];
      v_id[1] = v_id[2];
      n_id[1] = n_id[2];
      t_id[1] = t_id[2];
    }

  }

  fclose(out);
  return 0;
}

static void read_pbl(FILE *src, long flen)
{
  unsigned long num_items, this_offs, i;

  num_items = read_ui32(src);

  fprintf(stderr, "\nHEADER:\n-------\n");
  fprintf(stderr, "Num. Entries: %lu\n", num_items);

  /* NOTE: This first offset should be a multiple of 16: */
  this_offs = read_ui32(src);
  for (i = 0; i < num_items; ++i)
  {
    char name[512];
    unsigned long next_offs = read_ui32(src);
    unsigned long this_size = next_offs - this_offs;

    long prev_offs = ftell(src);

    fprintf(stderr, "\nItem %lu (Offs: %lu, Size: %lu):\n", i, this_offs, this_size);
    sprintf(name, "model%lu.obj", i);

    fseek(src, this_offs, SEEK_SET);
    save_obj(src, name);
    fseek(src, prev_offs, SEEK_SET);

    this_offs = next_offs;
  }

}

static void read_mpd(FILE *src, long flen)
{
  unsigned long head_size, head_items, head_flags, i;

  head_size = read_ui32(src);
  head_items = read_ui32(src);
  head_flags = read_ui32(src);

  fprintf(stderr, "Items Length: %lu\n", head_size);
  fprintf(stderr, "Header Items: %lu\n", head_items);
  fprintf(stderr, "Header Flags: %08lx\n", head_flags);

  if (head_size != MPD_HEAD_SIZE + (head_items * MPD_ITEM_SIZE))
  {
    fprintf(stderr, "Invalid header size.\n");
    return;
  }
  else if (head_flags != 0x00070001)
  {
    fprintf(stderr, "Invalid header flags.\n");
    return;
  }

  fprintf(stderr, "\nENTRIES:\n--------\n");
  for (i = 0; i < head_items; ++i)
  {
    unsigned long item_flag, item_offs, item_size, item_null;

    item_flag = read_ui32(src);
    item_offs = read_ui32(src);
    item_size = read_ui32(src);
    item_null = read_ui32(src);

    fprintf(stderr, "Item %lu:\n", i);
    fprintf(stderr, "\tFlag: %08lx\n", item_flag);  /* Chunk type? */
    fprintf(stderr, "\tOffs: %lu\n", item_offs);
    fprintf(stderr, "\tSize: %lu\n", item_size);
    fprintf(stderr, "\tNull: %lu\n", item_null);
  }

  fprintf(stderr, "\nPOLYGONS:\n---------\n");
  save_obj(src, "map.obj");

  /* There seems to be more data after this - investigate further! */
}

int main(int argc, char **argv)
{
  FILE *src = NULL;
  long flen;

  if (argc < 2)
  {
    fprintf(stderr, "Usage: %s filename.bin\n", argv[0]);
    return 1;
  }

  src = fopen(argv[1], "rb");
  if (!src)
  {
    fprintf(stderr, "Couldn't open %s\n", argv[1]);
    return 1;
  }

  fseek(src, 0, SEEK_END);
  flen = ftell(src);
  rewind(src);

  fprintf(stderr, "%s (%ld bytes) - ", argv[1], flen);
  if (flen >= 4)
  {
    unsigned long magic_number = read_ui32(src);
    if (magic_number == PBL_4CC)
    {
      fprintf(stderr, "PBL model\n");
      read_pbl(src, flen);
    }
    else if (magic_number == MPD_4CC)
    {
      fprintf(stderr, "MPD map\n");
      read_mpd(src, flen);
    }
    else { fprintf(stderr, "Unknown 4CC: %08lx\n", magic_number); }
  }
  else { fprintf(stderr, "File is too short!\n"); }

  fclose(src);
  return 0;
}
