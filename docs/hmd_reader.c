/**
@file
@brief A simple program for converting Robot Warlords HMD files into OBJ format
*/

#include <stdio.h>
#include <stdlib.h>

signed short read_si16(FILE *src)
{
  signed short i = (signed short)fgetc(src);
  i += (signed char)fgetc(src) << 8;
  return i;
}

unsigned short read_ui16(FILE *src)
{
  unsigned short i = (unsigned short)fgetc(src);
  i += (unsigned short)fgetc(src) << 8;
  return i;
}

signed long read_si32(FILE *src)
{
  signed long i = (signed long)fgetc(src);
  i += (signed long)fgetc(src) << 8;
  i += (signed long)fgetc(src) << 16;
  i += (signed char)fgetc(src) << 24;
  return i;
}

unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  i += (unsigned long)fgetc(src) << 24;
  return i;
}

/**
@brief Read a single list-of-offsets within the list-of-list-of-offsets.

NOTE: This is probably a rather hacky/wrong way to interpret the offset tables,
but hopefully it should be "good enough" for now. Most of the time, there are 4
entries, in this order:

* Offset to the start of the polygon data.
* Offset to the start of the vertex data.
* Offset to the "unknown" data; usually fits between the vertices and polygons.
* Offset to the "unknown" list; near the start of the file, after objects list.

I've noticed that some files include a 5th entry, with different flags; this is
simply ignored for now. Other files use a smaller, two-entry format, but I have
no idea how this is supposed to be used...

@param src The file to be read from.
@param a Optional: Stores the first offset.
@param b Optional: Stores the second offset.
@param c Optional: Stores the third offset.
@param d Optional: Stores the fourth offset.
@return True on success, or false if the offsets list wasn't understood.
*/

static int read_offsets_list(FILE *src, long *a, long *b, long *c, long *d)
{
  long offset_to_list_of_offsets, previous_offset;
  unsigned long number_of_items, values_used, i;

  if (!src) { return 0; }

  /* Get the offset to the list-of-offsets, and then seek to that position: */
  offset_to_list_of_offsets = read_si32(src) * 4;
  previous_offset = ftell(src);
  fseek(src, offset_to_list_of_offsets, SEEK_SET);

  /* Get the number of items within this (sub-)list. */
  number_of_items = read_ui32(src);

  /* Loop through all items in the list, ignoring anything not understood: */
  values_used = 0;
  for (i = 0; i < number_of_items; ++i)
  {
    long offset = (long)(read_ui16(src) * 4);
    unsigned short flags = read_ui16(src);

    if (flags == 0x8000)
    {
      /* There will usually be four of these: */
      switch (values_used)
      {
        case 0: if (a) { *a = offset; } break;
        case 1: if (b) { *b = offset; } break;
        case 2: if (c) { *c = offset; } break;
        case 3: if (d) { *d = offset; } break;
        default: fprintf(stdout, "Too many offsets!\n"); break;
      }
      ++values_used;
    }
  }

  /* Go back to (just after) where we were before this function was called: */
  fseek(src, previous_offset, SEEK_SET);

  /* If we found exactly 4 "known" values, then we should be OK to use them: */
  return values_used == 4;
}

/**
@brief Attempt to read the polygon data.

This is still a work-in-progress, but it seems to get the basics right...

@param src The file to read HMD data from.
@param out The file to write OBJ data to.
@param format The format of the polygon data.
@param count The number of polygons expected.
@param num_local_vertices The number of local vertices within this sub-object.
@param num_total_vertices The total number of vertices for all objects so far.
@return True on success, or false if the polygon data wasn't understood.
*/

static int read_polygons(FILE *src, FILE *out,
    unsigned char format, unsigned char count,
    long *num_local_vertices, long *num_total_vertices)
{
  unsigned char i;

  if (!src) { return 0; }

  for (i = 0; i < count; ++i)
  {
    unsigned long number_of_indices, p;
    unsigned char header[4];
    unsigned short v_id[3];

    /* Each polygon starts with a 4 byte "header" of some kind: */
    header[0] = fgetc(src);
    header[1] = fgetc(src);
    header[2] = fgetc(src);
    header[3] = fgetc(src);

    fprintf(stdout, "\tPolygon %d (", i);
    fprintf(stdout, "%d, ", header[0]);
    fprintf(stdout, "%d, ", header[1]);
    fprintf(stdout, "%d, ", header[2]);
    fprintf(stdout, "%d):", header[3]);

    /*
    These might be flags, but for now we just handle "known" values explicitly.

    Note that the latter four possibilities are equal to the first four options
    plus one: 8 --> 9, 12 --> 13, 16 --> 17, and 20 --> 21
    */

    switch (format)
    {
      case 8:
        if (header[3] != 32) { fprintf(stdout, "Expected 32...\n"); return 0; }
        number_of_indices = 3;
      break;

      case 12:
        if (header[3] != 48) { fprintf(stdout, "Expected 48...\n"); return 0; }
        number_of_indices = 3;
      break;

      case 16:
        if (header[3] != 40) { fprintf(stdout, "Expected 40...\n"); return 0; }
        number_of_indices = 4;
      break;

      case 20:
        if (header[3] != 56) { fprintf(stdout, "Expected 56...\n"); return 0; }
        number_of_indices = 4;
      break;

      case 9:
        number_of_indices = 3;
      break;

      case 13:
        number_of_indices = 3;
      break;

      case 17:
        number_of_indices = 4;
      break;

      case 21:
        number_of_indices = 4;
      break;

      default: return fprintf(stdout, " Unknown type\n");
    }

    /*
    I'm not sure what these values are; they don't seem to be indices, so maybe
    they're explicit texcoord values (read as UI8, UI8) or something?
    */

    if (format % 2)
    {
      unsigned long num_unknowns = 4;
      if (format == 17 || format == 21) { num_unknowns = 5; }

      if (header[3] != 0 && header[3] != 120 && header[3] != 127) { fprintf(stdout, "Expected 120 or 127...\n"); return 0; }

      fprintf(stdout, " {");
      for (p = 0; p < num_unknowns; ++p)
      {
        short unknown = read_si16(src);
        fprintf(stdout, " %d", unknown);
      }
      fprintf(stdout, " }");
    }

    /* This DOES seem to be an index of some kind, but its not a vertex... */

    if (format == 8 || format == 16 || format == 9 || format == 17)
    {
      short unknown = read_si16(src);
      fprintf(stdout, "{ %d }", unknown);
    }

    /* The rest of the data consists (mainly) of vertex indices: */

    for (p = 0; p < number_of_indices; ++p)
    {
      unsigned long a = (p < 2) ? p : 2;

      /* FIXME: There seem to be some extra polygons hidden within this... */
      if (format == 12 || format == 20 || format == 13)
      {
        short unknown = read_si16(src);
        fprintf(stdout, " %d", unknown);
      }

      /* Read the index value into the first "empty" slot: */
      v_id[a] = read_ui16(src);
      fprintf(stdout, " %d", v_id[a]);

      /* FIXME: There seem to be some extra polygons hidden within this... */
      if (format == 21 && p < 3)
      {
        short unknown = read_si16(src);
        fprintf(stdout, " %d", unknown);
      }

      /* Update the number of vertices expected, if necessary: */
      if (num_local_vertices && *num_local_vertices <= v_id[a])
      { *num_local_vertices = v_id[a] + 1; }

      /* Once we have at least three points, we can output triangles: */
      if (a == 2)
      {
        if (out) { fprintf(out, "f %d %d %d\n", v_id[2] + 1, v_id[1] + 1, v_id[0] + 1); }
        /* Use the last two points for the next triangle in the strip: */
        v_id[0] = v_id[2];
      }
    }
    fprintf(stdout, "\n");

    /* Format 16 (and maybe others?) - padding for alignment to 4-bytes: */
    /* if (format == 16) { read_ui16(src); } */
    while (ftell(src) % 4) { fgetc(src); }
  }

  return 1;
}

/**
@brief Try to read an individual object within a file.

@param src The file to read HMD data from.
@param out The file to write OBJ data to.
@param num_total_vertices The total number of vertices for all objects so far.
@return Zero on success, or non-zero if something goes wrong.
*/

static int read_object(FILE *src, FILE *out, long *num_total_vertices)
{
  long poly_offs, vert_offs, num_verts, number_of_items, i;

  if (!src) { return -1; }

  /* All objects seem to start with `-1` for some reason: */
  if (read_si32(src) != -1) { return fprintf(stdout, "Expected -1 here!\n"); }

  /* Next comes the offset to the list-of-offsets: */
  if (!read_offsets_list(src, &poly_offs, &vert_offs, NULL, NULL))
  { return fprintf(stdout, "Couldn't understand the offset table; skipped\n"); }

  /* Then comes the number of items within this object (usually 1, 2, or 3): */
  number_of_items = read_ui16(src);
  fprintf(stdout, "This object contains %ld sub-objects:\n", number_of_items);

  for (num_verts = 0, i = 0; i < number_of_items; ++i)
  {
    unsigned char unknown_a = fgetc(src);
    unsigned char unknown_b = fgetc(src);
    unsigned char data_type = fgetc(src);
    unsigned char unknown_d = fgetc(src);
    unsigned char unknown_e = fgetc(src);
    unsigned char unknown_f = fgetc(src);
    unsigned char unknown_g = fgetc(src);
    unsigned char unknown_h = fgetc(src);
    unsigned char data_count = fgetc(src);
    unsigned char unknown_j = fgetc(src);
    unsigned short data_offset = read_ui16(src);
    long previous_position = ftell(src);

    fprintf(stdout, "Sub-Object #%ld\n", i);
    fprintf(stdout, "\t???????????: %d\n", unknown_a);
    fprintf(stdout, "\t???????????: %d\n", unknown_b);
    fprintf(stdout, "\tData Format: %d\n", data_type);
    fprintf(stdout, "\t???????????: %d\n", unknown_d);
    fprintf(stdout, "\t???????????: %d\n", unknown_e);
    fprintf(stdout, "\t???????????: %d\n", unknown_f);
    fprintf(stdout, "\t???????????: %d\n", unknown_g);
    fprintf(stdout, "\tAlways Zero: %d\n", unknown_h);
    fprintf(stdout, "\tData Length: %d\n", data_count);
    fprintf(stdout, "\t???????????: %d\n", unknown_j);
    fprintf(stdout, "\tData Offset: %d\n", data_offset * 4);

    /* Read the polygons associated with this sub-object: */
    fseek(src, poly_offs + (data_offset * 4), SEEK_SET);
    if (!read_polygons(src, out, data_type, data_count, &num_verts, num_total_vertices)) { return 0; }
    fseek(src, previous_position, SEEK_SET);
  }

  /* Attempt to read the vertex data: */
  fseek(src, vert_offs, SEEK_SET);
  fprintf(stdout, "There appear to be %ld vertices:\n", num_verts);
  for (i = 0; i < num_verts; ++i)
  {
    short x = read_si16(src);
    short y = read_si16(src);
    short z = read_si16(src);
    short w = read_si16(src);

    /* NOTE: We scale things down for export so that the model isn't HUEG: */
    fprintf(stdout, "\tVertex %ld:\t%d\t%d\t%d\t%d\n", i, x, y, z, w);
    if (out) { fprintf(out, "v %f %f %f\n", x/100.0, y/100.0, z/100.0); }
  }

  /* Increase the "global" vertex counter; this is necessary for OBJ export. */
  if (num_total_vertices) { *num_total_vertices += num_verts; }
  return 0;
}

/**
@brief Loop through each "object" within a HMD file, and try to process them.

This function doesn't do much itself; it just finds the important bits, so that
other functions can read them.

@param src The file to read HMD data from.
@param out The file to write OBJ data to.
@return Zero on success, or non-zero if something goes wrong.
*/

static int read_file(FILE *src, FILE *out)
{
  unsigned long num_items, i;
  long length, num_verts;

  if (!src) { return -1; }

  /* Get the file size. */
  fseek(src, 0, SEEK_END);
  length = ftell(src);
  rewind(src);
  fprintf(stdout, "SIZE: %ld bytes\n", length);

  /* Make sure that the file seems to be at least somewhat legitimate: */
  if (length < 216) { return fprintf(stdout, "File is too short\n"); }
  if (read_ui32(src) != 80) { return fprintf(stdout, "Invalid 4CC\n"); }
  if (read_ui32(src) != 0) { return fprintf(stdout, "Invalid version\n"); }

  /*
  Multiplying this value by 4 gives the offset to the list-of-lists-of-offsets.
  However, we can ignore it - individual items in the following list specify an
  offset to the specific "list-of-offsets" (within the list-of-list-of-offsets)
  that they are interested in. (tl;dr = "Sup dawg, I herd u liek offsets...")
  */

  i = read_ui32(src) * 4;
  if (i < 20 || i > length-12) { return fprintf(stdout, "Strange offset\n"); }

  /* This seems to be a list of "sub-objects" within the file: */
  num_items = read_ui32(src);
  fprintf(stdout, "This file contains %lu objects:\n", num_items);
  for (num_verts = 0, i = 0; i < num_items; ++i)
  {
    long offset = read_si32(src) * 4;
    if (offset > 0)
    {
      long oldpos = ftell(src);
      fprintf(stdout, "\nObject #%lu:\n", i);
      fseek(src, offset, SEEK_SET);
      read_object(src, out, &num_verts);
      fseek(src, oldpos, SEEK_SET);
    }
    else
    {
      /* Some items within the list have an offset of zero; ignore these... */
      fprintf(stdout, "\nObject #%lu:\nOffset is %ld, skipped...\n", i, offset);
    }
  }

  fprintf(stdout, "The file contains %ld vertices in total.\n", num_verts);
  return 0;
}

/**
@brief The program entry point.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int main(int argc, char **argv)
{
  int i;
  for (i = 1; i < argc; ++i)
  {
    FILE *src = fopen(argv[i], "rb");
    fprintf(stdout, "NAME: %s\n", argv[i]);
    if (src)
    {
      FILE *out;
      char name[512];

      sprintf(name, "out%d.obj", i);
      out = fopen(name, "w");
      read_file(src, out);
      if (out) { fclose(out); }

      fclose(src);
    }
    else { fprintf(stdout, "Could not open %s\n", argv[i]); }
  }
  return 0;
}
