/**
@file
@brief A simple program for converting Chasm: The Rift 3O files into OBJ format

It's also possible to dump the texture data as a Chasm .OBJ, which can then get
converted into a TGA just as other .OBJ sprite images can.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Constants: */

#define _3O_HEAD_SIZE 0     /**< @brief .3O files don't have any header. */
#define CAR_HEAD_SIZE 102   /**< @brief CAR files use a 102-byte header. */

/* NOTE: These offsets do not include the 102-byte .CAR file header length: */

#define _3O_POLY_OFFS 0     /**< @brief Offset to start of polygon data.    */
#define _3O_VERT_OFFS 12800 /**< @brief Offset to start of vertex data.     */
#define _3O_INFO_OFFS 18432 /**< @brief Offset to mesh information data.    */
#define _3O_SKIN_OFFS 18436 /**< @brief 3O ONLY: Offset to texture data.    */

#define _3O_POLY_SIZE 32    /**< @brief Size of each polygon record.        */
#define _3O_VERT_SIZE 6     /**< @brief Size of each vertex record.         */
#define _3O_INFO_SIZE 4     /**< @brief Size of the mesh information data.  */
#define _3O_SKIN_SIZE 64    /**< @brief Width of each texture row in bytes. */

#define _3O_MAX_POLYS 400   /**< @brief Maximum possible polygons included. */
#define _3O_MAX_VERTS 255   /**< @brief Maximum possible vertices included. */

typedef enum {

  CHASM_FILE_UNKNOWN,
  CHASM_FILE_3O,
  CHASM_FILE_CAR

} chasm_file_t;

signed short read_si16(FILE *src)
{
  signed short i = (signed short)fgetc(src);
  i += (signed char)fgetc(src) << 8;
  return i;
}

/**
@brief Try to figure out if this is a Chasm: The Rift .CAR or .3O file.

3O files start with polygon definitions, so the first three SI16 values ought
to be valid vertex indices (i.e. 0-254), whereas CAR files start with a large
value (such as 93268 for TT_TOWER.CAR) followed by a zero. We can thus make a
fairly decent guess at the type of model based on the values of those first 2
or 3 SI16 values... though of course, it may be some other, non-Chasm file...

@param src The file to check.
@return An enum describing the (probable) file type.
*/

static chasm_file_t chasmFileType(FILE *src)
{
  long len;

  if (!src) { return CHASM_FILE_UNKNOWN; }
  fseek(src, 0, SEEK_END);
  len = ftell(src);
  if (len < 6) { return CHASM_FILE_UNKNOWN; }
  rewind(src);

  if (read_si16(src) >= _3O_MAX_VERTS)
  {
    if (read_si16(src) == 0 && len >= 18538) { return CHASM_FILE_CAR; }
  }
  else if (read_si16(src) < _3O_MAX_VERTS && len >= 18438)
  {
    if (read_si16(src) < _3O_MAX_VERTS) { return CHASM_FILE_3O; }
  }

  return CHASM_FILE_UNKNOWN;
}

int main(int argc, char *argv[])
{
  FILE *src = NULL;
  FILE *out = NULL;
  long header_size;
  short num_polys, num_verts, skin_h, i;
  chasm_file_t file_type;

  /* Attempt to open the input file: */
  src = fopen(argv[1], "rb");
  if (!src)
  {
    fprintf(stderr, "Could not open %s\n", argv[1]);
    return 1;
  }

  file_type = chasmFileType(src);
  switch (file_type)
  {
    case CHASM_FILE_3O:  header_size = _3O_HEAD_SIZE; break;
    case CHASM_FILE_CAR: header_size = CAR_HEAD_SIZE; break;

    case CHASM_FILE_UNKNOWN: default:
      fprintf(stderr, "%s does not appear to be a Chasm: TR model.\n", argv[1]);
      fclose(src);
    return 0;
  }

  fseek(src, header_size + _3O_INFO_OFFS, SEEK_SET);
  num_verts = read_si16(src);
  num_polys = read_si16(src);

  /* Note: A few "empty" model files really do have < 3 verts and 0 polys... */
  if (num_verts < 3 || num_verts > _3O_MAX_VERTS ||
      num_polys < 1 || num_polys > _3O_MAX_POLYS)
  {
    fprintf(stderr, "Invalid header value(s)\n");
    fclose(src);
    return 1;
  }

  /* FIXME: This seems to be stored elsewhere in .CAR files... */
  if (file_type == CHASM_FILE_3O)
  {
    fseek(src, header_size + _3O_SKIN_OFFS, SEEK_SET);
    skin_h = read_si16(src);
    fprintf(stderr, "Texture size: %dx%d:\n", _3O_SKIN_SIZE, skin_h);

    /* The image uses the palette, so output it as a Chasm .OBJ image file. */
    if (argc >= 4)
    {
      out = fopen(argv[3], "w");
      if (out)
      {
        short j;
        /* Number of images: */
        fputc(1, out); fputc(0, out);
        /* Height: */
        fputc((skin_h >> 0) & 0xFF, out); fputc((skin_h >> 8) & 0xFF, out);
        /* Width: */
        fputc(64, out); fputc(0, out);
        /* Unknown: */
        fputc(0, out); fputc(0, out);
        /* Data: */
        for (i = 0; i < skin_h; ++i)
        {
          for (j = 0; j < _3O_SKIN_SIZE; ++j) { fputc(fgetc(src), out); }
        }
        fclose(out);
      }
      else { fprintf(stderr, "[WARNING] Could not write texture file.\n"); }
    }
  }
  else
  {
    fprintf(stderr, "[TODO] .CAR textures aren't implemented yet...\n");
    skin_h = 1;
  }

  /* Attempt to open an output file, if requested. */
  if (argc >= 3)
  {
    out = fopen(argv[2], "w");
    if (out) { fprintf(out, "mtllib default.mtl\ng default\nusemtl skin\n"); }
    else { fprintf(stderr, "[WARNING] Could not write 3D mesh file.\n"); }
  }

  /* Output the vertices. */
  fprintf(stderr, "%d vertices:\n", num_verts);
  fseek(src, header_size + _3O_VERT_OFFS, SEEK_SET);
  for (i = 0; i < num_verts; ++i)
  {
    short x, y, z;

    x = read_si16(src);
    y = read_si16(src);
    z = read_si16(src);
    fprintf(stderr, "\t%d: %d, %d, %d\n", i, x, y, z);

    if (out) { fprintf(out, "v %f %f %f\n", x/256.0, y/256.0, z/256.0); }
  }

  /* Output the polygon data. Each one may be a quad or a triangle. */
  fprintf(stderr, "%d polygons:\n", num_polys);
  fseek(src, header_size + _3O_POLY_OFFS, SEEK_SET);
  for (i = 0; i < num_polys; ++i)
  {
    short value[16];
    int x;

    for (x = 0; x < 16; ++x) { value[x] = read_si16(src); }

    fprintf(stderr, "\tPolygon %d:\n", i);

    /* The last vertex index is sometimes 255 (for triangles). */
    if (value[0] < 0 || value[0] >= num_verts ||
        value[1] < 0 || value[1] >= num_verts ||
        value[2] < 0 || value[2] >= num_verts ||
        value[3] < 0 || (value[3] >= num_verts && value[3] != 255))
    { fprintf(stderr, "[WARNING] Invalid vertex index given!\n"); }
    fprintf(stderr, "\t\tVrts: %d > %d > %d > %d\n", value[0], value[1], value[2], value[3]);

    /* Texture coordinates? */
    fprintf(stderr, "\t\tTC.1:\t%d\t%d\n", value[4 ], value[5 ]);
    fprintf(stderr, "\t\tTC.2:\t%d\t%d\n", value[6 ], value[7 ]);
    fprintf(stderr, "\t\tTC.3:\t%d\t%d\n", value[8 ], value[9 ]);
    fprintf(stderr, "\t\tTC.4:\t%d\t%d\n", value[10], value[11]);

    /* Not sure what these are for... */
    fprintf(stderr, "\t\t????: %d\n", value[12]);
    fprintf(stderr, "\t\t????: %d\n", value[13]);
    fprintf(stderr, "\t\tFlag: %d\n", value[14]);
    /* Texture coordinate vertical offset. */
    fprintf(stderr, "\t\tOffs: %d\n", value[15]);

    if (out)
    {
      int vt = 0;

      for (vt = 0; vt < 4; ++vt)
      {
        fprintf(out, "vt %f %f\n",
            (float)value[4+vt*2] / (float)_3O_SKIN_SIZE,
            (float)(value[5+vt*2]+value[15]) / (float)skin_h);
      }

      fprintf(out, "f");
      for (vt = 0; vt < 4; ++vt)
      {
        if (value[vt] != 255)
        {
          fprintf(out, " %d/%d", value[vt] + 1, (i*4) + vt + 1);
        }
      }
      fprintf(out, "\n");
    }

  }

  /* Close any open file handles and end the program. */
  if (out) { fclose(out); }
  fclose(src);
  return 0;
}
