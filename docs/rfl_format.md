Unofficial Red Faction .RFL file format specification
=====================================================

IMPORTANT: This document is a work-in-progress, and may contain errors.

General notes:

+ Integers are stored as little-endian, twos-complement values, usually 32-bit.
+ Real values are stored as IEEE 32-bit floating point values.
+ Strings seem to be ASCII, but config files use Latin-1, so maybe it's that...
+ Strings are stored as a UINT16 length value followed by the character data.
+ Matrices are stored as a list of nine 32-bit IEEE float values.

I haven't investigated the format used by Red Faction 2 maps yet.

-------------------------------------------------------------------------------

General structure
-----------------

In Red Faction 1, RFL files start with the following header:

+ UINT32 Magic number: 0xD4BADA55 (Note: Red Faction was originally Descent 4)
+ UINT32 Version. May be 156, 157, 158, 174, or (usually) 175.
+ REAL32 ????
+ UINT32 Offset to MATRIX chunk.
+ UINT32 Offset to META chunk.

For v174 and v175 files, there are a few extra fields:

+ UINT32 Number of chunks (excluding the NULL chunk).
+ UINT32 The sum of all chunk "size" fields.
+ STRING Level name.

The rest of the file is divided into chunks, in the following format:

+ UINT32 Chunk type.
+ UINT32 Chunk size in bytes, excluding the 8 bytes used for this and the type.
+ VARIES Chunk data.

Details for each chunk type are outlined below. Unless otherwise specified, the
information describes the RFL version(s) used by the original game.

-------------------------------------------------------------------------------

0x00000100 - Mesh chunk
-----------------------

This chunk describes vertex and polygon data (There's also a separate "multiple
mesh" chunk, which uses a similar format - see `0x02000000` below for details).

It seems that the first 6 bytes are always zero:

+ UINT16 Always zero?
+ UINT32 Always zero?

This is then followed by a list of textures:

+ UINT32 Number of textures.

Each texture is simply a string:

+ STRING Filename, such as "mtl_invisible02.tga"

This is followed by a UINT32 which always appears to be zero:

+ UINT32 Always zero? Possibly the length of a list that is never(?) used...

Then comes "UNKNOWN LIST A":

+ UINT32 Number of items in UNKNOWN LIST A (usually 0 in multi-mesh chunks).

Each item in the list is as follows:

+ SINT32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ UINT32 Flags1
+ UINT32 Flags2
+ REAL32 ????

If `Flags2` has its `1` bit set, some extra fields are included before the next
item in the list:

+ REAL32 ???? (Maybe this is the texture X-scroll speed?)
+ REAL32 ???? (Maybe this is the texture Y-scroll speed?)
+ STRING File name, such as "greenwater01.vbm" or "wtr_mudhoney01.vbm"
+ REAL32 ????
+ UINT32 Always 1?
+ UINT32 ????
+ UINT8  ???? (May be zero or one, possibly other values too?)
+ UINT32 Image width?
+ UINT32 Image height?
+ UINT32 ????
+ UINT32 Always zero?
+ UINT32 Always zero?
+ UINT32 Always zero?

If `Flags2` has its `256` bit set, an extra UINT32 is added on to the end:

+ UINT32 ????

Then comes "UNKNOWN LIST B":

+ UINT32 Number of items in UNKNOWN LIST B (usually 0 in multi-mesh chunks).

Each item in the list varies in length, depending on its number of sub-items:

+ UINT32 ????
+ UINT32 Number of sub-items.

Each sub-item is simply a UINT32:

+ UINT32 ????

A third unknown list follows:

+ UINT32 Number of items in UNKNOWN LIST C (usually 0 in multi-mesh chunks).

Each item in the list has the following format:

+ UINT32 ????
+ UINT32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????

This is then followed by a list of vertices:

+ UINT32 Number of vertices.

Each vertex is simply an XYZ position:

+ REAL32 X position.
+ REAL32 Y position.
+ REAL32 Z position.

Next comes a list of polygons:

+ UINT32 Number of polygons.

Each polygon varies in size, depending on the number of points it has:

+ REAL32 Face normal X. (NOTE: Not present in RFL versions < 174!)
+ REAL32 Face normal Y. (NOTE: Not present in RFL versions < 174!)
+ REAL32 Face normal Z. (NOTE: Not present in RFL versions < 174!)
+ REAL32 ???? (NOTE: Not present in RFL versions < 174!)
+ UINT32 Texture index.
+ SINT32 "Index A" (see below)
+ UINT32 ???? (Possibly a SINT32, index, but always present?)
+ SINT32 Always -1? (TODO: Verify this...)
+ SINT32 Always -1? (TODO: Verify this...)
+ UINT32 Always zero? (TODO: Verify this...)
+ UINT32 Flags?
+ UINT32 ???? (Possibly a SINT32, index, but always present?)
+ SINT32 Always -1? (TODO: Verify this...)
+ UINT32 Number of points that this polygon has.

This is then followed by data for each point:

+ UINT32 Vertex index.
+ REAL32 Possibly a texture U coordinate?
+ REAL32 Possibly a texture V coordinate?
+ REAL32 Possibly a texture U coordinate? (Only if "Index A is >= 0)
+ REAL32 Possibly a texture V coordinate? (Only if "Index A is >= 0)

After the polygon data comes "UNKNOWN LIST D":

+ UINT32 Number of items in UNKNOWN LIST D (Often zero).

Each item in the list is as follows:

+ UINT32 ????
+ UINT32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ UINT32 ????
+ SINT32 ????
+ UINT32 ????
+ REAL32 ????
+ UINT32 ????
+ UINT32 ????
+ UINT32 ????
+ UINT32 ????
+ UINT32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ SINT32 ????

This is followed by "UNKNOWN LIST E":

+ UINT32 Number of items in UNKNOWN LIST E (Often zero).

Each item in the list is as follows:

+ UINT32 Maybe a vertex index?
+ REAL32 Maybe a texture U cooredinate?
+ REAL32 Maybe a texture V cooredinate?

-------------------------------------------------------------------------------

0x00000200 - Matrix list(?) chunk
---------------------------------

This chunk appears to store a list of matrices in the following format:

+ UINT32 Number of items within this chunk.

Then, for each item:

+ UINT32 ????
+ UINT32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ MATRIX ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????

NOTE: The above may very well be incorrect; More investigation is needed here.

-------------------------------------------------------------------------------

0x00000300 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00000400 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00000500 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00000600 - Trigger chunk
--------------------------

This chunk stores a list of "event triggers" (such as level change triggers, or
music start/stop events) in a standard format. It starts with a "count" value:

+ UINT32 Number of items within this chunk.

This is followed by the data for each event, in the following format:

+ STRING Type of event - see the MATRIX note below.
+ REAL32 X position.
+ REAL32 Y position.
+ REAL32 Z position.
+ STRING Description.
+ UINT8  ????
+ REAL32 ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT32 ????
+ REAL32 Seems to be an event "parameter" value.
+ REAL32 Seems to be an event "parameter" value.
+ STRING Seems to be an event "parameter" value.
+ STRING Seems to be an event "parameter" value.
+ UINT32 Length of the UNKNOWN list (see below).

This is followed by the UNKNOWN list:

+ UINT32 ????

Most events end here, but a few specific types add a MATRIX record on the end:

+ "Teleport"
+ "Teleport_Player"
+ "Alarm"
+ "Play_Vclip"

-------------------------------------------------------------------------------

0x00000700 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00000800 - Zero chunk
-----------------------

This chunk always contains exactly 12 zero bytes. Perhaps it's an XYZ position?

-------------------------------------------------------------------------------

0x00000900 - Rock chunk
-----------------------

This chunk has the following format:

+ STRING Rock texture filename (usually "rock02.tga", but not always).
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????
+ UINT8  ????

Perhaps this chunk defines the texture used for holes caused by GeoMod?

-------------------------------------------------------------------------------

0x00000A00 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00000B00 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00000C00 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00000D00 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00000E00 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00000F00 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00001000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00001100 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00001200 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00002000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00003000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00004000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00005000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

List chunks
-----------

The following chunks have the same format:

+ 0x00010000 - AI waypoints?
+ 0x00006000 - Some other kind of path-finding thing? (only used in 5 levels!)

They start with a "count" value:

+ UINT32 Number of items within this chunk.

Then, for each item:

+ STRING Name.
+ UINT32 Number of sub-items.

This is then followed by a sub-list:

+ UINT32 ????

To clarify: The "sub-list" comes *after* its "parent" list item, but before the
next "main" list item.

-------------------------------------------------------------------------------

Filename chunks
---------------

The following chunks have (almost) the same format:

+ 0x00007000 - List of .TGA "effects" images (actually .PEG files).
+ 0x00007001 - List of .VCM character models (actually .RFC files).
+ 0x00007002 - List of .MVF character animations (actually .RFA files).
+ 0x00007003 - List of .V3D (static) models (actually .RFM files).
+ 0x00007004 - List of .VFX visual effects.

They start with a "count" value:

+ UINT32 Number of items within this chunk.

Then, for each item:

+ STRING Filename, such as "glowball.tga"

For all chunk types except the "TGA" one, this is followed by another list with
the same number of items:

+ UINT32 Flags?

-------------------------------------------------------------------------------

0x00020000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00030000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00040000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00050000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00060000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00070000 - Matrix chunk
-------------------------

This is one of the two chunks important enough to be referenced within the file
header (i.e. given an explicit offset); it stores an XYZ position and a matrix:

+ REAL32 X position.
+ REAL32 Y position.
+ REAL32 Z position.
+ MATRIX A standard 3x3 matrix.

This is either some kind of "global transform" applied to everything within the
level, or it defines the position of some very important thing within the map.

-------------------------------------------------------------------------------

0x01000000 - Meta chunk
-----------------------

This is one of the two chunks important enough to be referenced within the file
header (i.e. given an explicit offset) and seems to contain general information
about the file. It starts simply enough:

+ UINT32 Perhaps this is a version - or perhaps it's a "count" that's always 1?
+ STRING Map name. This is the same as the one in the header for v174/175 maps.
+ STRING Author(s). The "Mike test level" maps have this set to "Chris Helvig".
+ STRING Date. Seems to be the "last compiled" date, rather than "first added".

The next field seems to be used for flags. Its size varies by RFL file version:

+ mk_circuits2 (v156) 1 zero byte (also mk_cube / mk_test / m_arena / m_canyon)
+ mk_circuits3 (v157) 1 zero byte (also mk_cube2)
+ mk_wacky     (v158) 1 zero byte
+ mk_circuits4 (v174) 2 zero bytes (also m_spawntest, which is 00 01 instead)
+ mk_cube2     (v175) 2 zero bytes

Why is the second "flags" byte sometimes set to one?

+ All multiplayer levels are v175 and have the flags set to 00 01.
+ All single player levels are v175. The ones I checked are set to 00 00.
+ The two training levels are v175 and have the flags set to 00 00.

Either the second byte is used to flag levels as "multiplayer" maps, or it just
happens to be set on all multiplayer maps (plus the `m_spawntest` test map)...

The rest of the chunk seems to consist of 4 structures in the following format:

+ UINT32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ REAL32 ????
+ MATRIX ????

One exception: The last (4th) structure does not start with a UINT32.

-------------------------------------------------------------------------------

0x02000000 - List of meshes
---------------------------

This chunk describes *multiple* meshes. Each one is essentially the same as the
"single" mesh chunk (see `0x00000100` above), with a few extra fields glued on.

It starts with a "count" value:

+ UINT32 Number of meshes within this chunk.

The data for each mesh follows:

+ UINT32 ????
+ REAL32 X position?
+ REAL32 Y position?
+ REAL32 Z position?
+ MATRIX Standard 3x3 matrix.
+ ...mesh data (in exactly the same format as the "single mesh" chunk)...
+ UINT32 ????
+ SINT32 ????
+ UINT32 ????

-------------------------------------------------------------------------------

0x03000000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x04000000 - ????
-----------------

Unknown.

-------------------------------------------------------------------------------

0x00000000 - Null chunk
-----------------------

This chunk does not contain any data; it simply indicates the end of the file.

-------------------------------------------------------------------------------

DISCLAIMER: The information in this document is based on reverse-engineering of
RFC files extracted from the VPP archives on the PlayStation 2 PAL (SLES-50277)
version of Red Faction. It is completely unofficial, and is neither endorsed by
nor affiliated with Volition Software in any way. Other versions of the product
(such as the PC or N-GAGE ports) may differ from the details described here.

Red Faction II data is based on the PlayStation 2 PAL (SLES-51133) version.
