File Format Notes + Sample Code
===============================

This directory contains various small programs for handling specific file types
that have not been integrated into the main code base because either (A) I just
haven't got around to it yet, or (B) they're "awkward" in some way - maybe they
aren't fully understood, or maybe they would require some special handling that
would complicate things.

Feel free to use any of the information given here however you wish.

Licensing
---------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
license - you may use or modify it for any purpose, including closed-source and
paid-for projects. All I ask in return is proper attribution (i.e. don't remove
my name from the copyright text in the source code, and maybe include me on the
"credits" screen or whatever equivalent your program has).
