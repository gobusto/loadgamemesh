/**
@file
@brief A simple program for converting BCV .AGO files into OBJ format

BCV (Battle Construction Vehicles) uses something called the "Agatha Engine", so
it's likely that AGO stands for "Agatha Game Object"; perhaps other Agatha-based
games can also be converted by this code too?
*/

#include <stdlib.h>
#include <stdio.h>

/* =============================================================================
CONSTANTS
============================================================================= */

#define AGO_BASIC_HEAD_SIZE 48  /**< @brief Header size, without extra items. */
#define AGO_ENTRY_HEAD_SIZE 16  /**< @brief Size of each "extra" header item. */

#define AGO_CHUNK_DATA 0xD0000000 /**< @brief Indicates an "offset" follows. */
#define AGO_CHUNK_STOP 0x60000000 /**< @brief Indicates "no more offsets".   */

#define AGO_BLOCK_DATA 0x1000 /**< @brief Indicates graphics data follows.   */
#define AGO_BLOCK_STOP 0x6000 /**< @brief Indicates "no more graphics data". */

#define AGO_DATA_TEXCOORDS  0x64  /**< @brief Texture coordinate data.      */
#define AGO_DATA_VERTICES   0x68  /**< @brief Vertex coordinate data.       */
#define AGO_DATA_UNKNOWNA   0x6C  /**< @brief Unknown...                    */
#define AGO_DATA_UNKNOWNB   0x6D  /**< @brief Unknown...                    */
#define AGO_DATA_UNKNOWNC   0x6E  /**< @brief Unknown; Maybe RGBA data?     */
#define AGO_DATA_END        0x14  /**< @brief Indicates the end of a block. */

/* =============================================================================
HELPER FUNCTIONS
============================================================================= */

static float read_float32(FILE *src)
{
  float f;
  fread(&f, 4, 1, src);
  return f;
}

static unsigned short read_ui16(FILE *src)
{
  unsigned short i = (unsigned short)fgetc(src);
  i += (unsigned short)fgetc(src) << 8;
  return i;
}

static signed long read_si32(FILE *src)
{
  signed long i = (signed long)fgetc(src);
  i += (signed long)fgetc(src) << 8;
  i += (signed long)fgetc(src) << 16;
  i += (signed char)fgetc(src) << 24;
  return i;
}

static unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  i += (unsigned long)fgetc(src) << 24;
  return i;
}

/* =============================================================================
FILE READING STUFF
============================================================================= */

static long read_ago_header(FILE *src)
{
  long num_items, i;

  /* The first 40 bytes of AGO files are always the same - an exception is... */
  unsigned long header[10] = {
    0x50000000, 0xFFFFFFFF, 0x00000000, 0x00000000, 0x60000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000010, 0x00000001
  };

  /* ...the second field gives the total header size, meaning that it varies: */
  const int AGO_HEAD_SIZE_FIELD = 1;

  for (i = 0; i < 10; ++i)
  {
    unsigned long value = read_ui32(src);
    if (i == AGO_HEAD_SIZE_FIELD)
    {
      header[i] = value;
    }
    else if (header[i] != value)
    {
      fprintf(stdout, "Expected %lu...\n", header[i]);
      return -1;
    }
  }

  /* This value always seems to be zero... */
  if (read_ui16(src) != 0)
  {
    fprintf(stdout, "Expected zero!\n");
    return -1;
  }

  /* This always seems to match the "header size" value given above: */
  if (read_ui16(src) != header[AGO_HEAD_SIZE_FIELD])
  {
    fprintf(stdout, "Expected %lu!\n", header[AGO_HEAD_SIZE_FIELD]);
    return -1;
  }

  /* Get the number of "extra" header items; note that this CAN be zero! */
  num_items = read_ui32(src);

  if (header[AGO_HEAD_SIZE_FIELD] != AGO_BASIC_HEAD_SIZE + (num_items * AGO_ENTRY_HEAD_SIZE))
  {
    fprintf(stdout, "Header size doesn't match number of items!\n");
    return -1;
  }

  return num_items;
}

/* NOTE: Not every "sub-block" is supported at the moment, but polygons are. */

static long read_ago_data(FILE *src, long length, FILE *out, long num_points)
{
  while (0x1337)
  {
    unsigned char flags = fgetc(src);
    unsigned char unknown = fgetc(src);   /* Always seems to be 0x80? */
    unsigned char num_items = fgetc(src);
    unsigned char data_type = fgetc(src);
    unsigned char i;

    fprintf(stdout, "\t[%02x][%02x] %d ", unknown, flags, num_items);

    switch (data_type)
    {
      case AGO_DATA_TEXCOORDS:
        fprintf(stdout, "texcoords:\n");
        for (i = 0; i < num_items; ++i)
        {
          float u = read_float32(src);
          float v = read_float32(src);
          fprintf(stdout, "\t\t%d: %f, %f\n", i, u, v);
          if (out) { fprintf(out, "vt %f %f\n", u, v); }
        }
      break;

      case AGO_DATA_VERTICES:
        fprintf(stdout, "vertices:\n");
        for (i = 0; i < num_items; ++i)
        {
          float x = read_float32(src);
          float y = read_float32(src);
          float z = read_float32(src);
          fprintf(stdout, "\t\t%d: %f, %f, %f\n", i, x, y, z);
          if (out) { fprintf(out, "v %f %f %f\n", x, y, z); }
        }

        if (out)
        {
          /* Triangles form triangle strips: */
          for (i = 2; i < num_items; ++i)
          {
            int p;
            fprintf(out, "f");
            /* Every other triangle needs to use a reverse triangle winding: */
            for (p = (i%2) ? 0 : 2; (i%2) ? p < 3 : p >= 0; (i%2) ? ++p : --p)
            {
              long q = (i-p) + num_points + 1;
              fprintf(out, " %ld/%ld", q, q);
            }
            fprintf(out, "\n");
          }

        }
        num_points += num_items;
      break;

      case AGO_DATA_UNKNOWNA:
        fprintf(stdout, "XXXX A:\n");
        for (i = 0; i < num_items; ++i)
        {
          unsigned long a = read_ui32(src);
          unsigned long b = read_ui32(src);
          unsigned long c = read_ui32(src);
          unsigned long d = read_ui32(src);
          fprintf(stdout, "\t\t%d: %08lx %08lx %08lx %08lx\n", i, a, b, c, d);
        }
      break;

      case AGO_DATA_UNKNOWNB:
        fprintf(stdout, "XXXX B:\n");
        for (i = 0; i < num_items; ++i)
        {
          unsigned long a = read_ui32(src);
          unsigned long b = read_ui32(src);
          fprintf(stdout, "\t\t%d: %08lx %08lx\n", i, a, b);
        }
      break;

      case AGO_DATA_UNKNOWNC:
        fprintf(stdout, "XXXX C:\n");
        for (i = 0; i < num_items; ++i)
        {
          unsigned long a = read_ui32(src);
          fprintf(stdout, "\t\t%d: %08lx\n", i, a);
        }
      break;

      case AGO_DATA_END:
        fprintf(stdout, "(end of data)\n");
      return num_points;

      default:
        /*
        Some blocks start with a 0x00 data type; I'm not sure what this is for,
        but I think that it should be safe to ignore it.
        */
        fprintf(stdout, "Unknown 0x%02x (skipped)\n", data_type);
      return num_points;
    }
  }
}

/* This may be called just once or multiple times, depending on the format: */

static long read_ago_object(FILE *src, long length, FILE *out, long num_points)
{
  unsigned short count, flags;

  fprintf(stdout, "========================================================\n");
  fprintf(stdout, "START OF OBJECT (%ld bytes)\n", length);
  fprintf(stdout, "========================================================\n");

  do
  {
    unsigned long x, y, z;

    /*
    The "count" is the number of 16-byte "blocks" used by the data; the "flags"
    indicates whether there is actually any more data to read. In other words:

    * If count is > 0 and flags = 0x1000, keep reading.
    * If count is = 0 and flags = 0x6000, there is no more data.

    I'm not sure that the other bits are for yet, but they don't seem important.
    */

    count = read_ui16(src);
    flags = read_ui16(src);

    x = read_ui32(src);
    y = read_ui32(src);
    z = read_ui32(src);

    /* If this block contains graphics data, try to interpret it: */
    if (flags == AGO_BLOCK_DATA)
    {
      long next_offs = ftell(src) + (count * 16);

      fprintf(stdout, "%d bytes of data (%08lx | %08lx | %08lx):\n", count * 16, x, y, z);
      num_points = read_ago_data(src, count * 16, out, num_points);

      fseek(src, next_offs, SEEK_SET);
    }
    else if (flags != AGO_BLOCK_STOP)
    {
      fprintf(stdout, "Unexpected block flags type: %04x\n", flags);
      break;
    }

  } while(flags != AGO_BLOCK_STOP);

  return num_points;
}

/* =============================================================================
PROGRAM ENTRY POINT
============================================================================= */

int main(int argc, char **argv)
{
  FILE *src;
  long length, num_items;

  FILE *out;
  long num_points = 0;

  if (!argv[1]) { fprintf(stdout, "Usage: %s filename\n", argv[0]); return 0; }
  src = fopen(argv[1], "rb");
  if (!src) { fprintf(stdout, "Could not open %s\n", argv[1]); return 0; }

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  rewind(src);

  fprintf(stdout, "========================================================\n");
  fprintf(stdout, "%s (%ld bytes)\n", argv[1], length);
  fprintf(stdout, "========================================================\n");

  if (length < AGO_BASIC_HEAD_SIZE)
  {
    fprintf(stdout, "File is too short\n");
    fclose(src);
    return 0;
  }

  out = fopen("out.obj", "w");

  num_items = read_ago_header(src);
  if (num_items > 0)
  {
    long i;

    /* Files with a positive "number of items" value start with an item list: */
    fprintf(stdout, "This file has %lu item(s):\n", num_items);

    for (i = 0; i < num_items; ++i)
    {
      char          name[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
      unsigned char data[8];

      fread(name, 1, 8, src);
      fread(data, 1, 8, src);

      fprintf(stdout, "\t%ld: %s -", i, name);
      fprintf(stdout, " %02x", data[0]);
      fprintf(stdout, " %02x", data[1]);
      fprintf(stdout, " %02x", data[2]);
      fprintf(stdout, " %02x", data[3]);
      fprintf(stdout, " %02x", data[4]);
      fprintf(stdout, " %02x", data[5]);
      fprintf(stdout, " %02x", data[6]);
      fprintf(stdout, " %02x", data[7]);
      fprintf(stdout, "\n");
    }

    /* After this comes the actual data: */
    num_points = read_ago_object(src, length - ftell(src), out, num_points);
  }
  else if (num_items == 0)
  {
    unsigned long chunk_flags;
    long offset;
    long previous = 0;

    /* Some files claim to have ZERO items - they use a different format: */
    fprintf(stdout, "No items listed; using 'multiple entry' format...\n");

    do
    {
      unsigned long dummy1, dummy2;

      chunk_flags = read_ui32(src);
      offset = read_si32(src);
      dummy1 = read_ui32(src);
      dummy2 = read_ui32(src);

      if (dummy1 != 0) { fprintf(stdout, "Unexpected dummy1: %lu\n", dummy1); }
      if (dummy2 != 0) { fprintf(stdout, "Unexpected dummy2: %lu\n", dummy2); }

      if (chunk_flags == AGO_CHUNK_STOP)
      {
        if (offset != 0) { fprintf(stdout, "Unexpected value: %lu\n", offset); }
        offset = length;  /* The previous item runs until the end of the file */
      }
      else if (chunk_flags != AGO_CHUNK_DATA)
      {
        fprintf(stdout, "Unknown flagset: %08lx\n", chunk_flags);
      }

      /* Try to read the PREVIOUS chunk, now that we know its length: */
      if (previous)
      {
        const long current_position = ftell(src);
        fseek(src, previous, SEEK_SET);
        num_points = read_ago_object(src, offset - previous, out, num_points);
        fseek(src, current_position, SEEK_SET);
      }
      previous = offset;

    } while (chunk_flags != AGO_CHUNK_STOP);

  }
  else
  {
    /* A negative value indicates an error of some kind: */
    fprintf(stdout, "This does not appear to be a valid .AGO file.\n");
  }

  fprintf(stdout, "========================================================\n");

  fclose(out);
  fclose(src);
  return 0;
}
