/**
@file
@brief A simple program for converting Half-Life 1 MDL files into OBJ format

It's also possible to dump the texture data as in TGA format.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Constants: */

#define HL1MDL_IDST 0x54534449  /**< @brief Spells "IDST" as ASCII text. */
#define HL1MDL_IDSQ 0x51534449  /**< @brief Spells "IDSQ" as ASCII text. */

#define HL1MDL_VER 10 /**< @brief MDL version used by Half Life 1. */

#define HL1MDL_HEAD_SIZE 244
#define HL1MDL_BONE_SIZE 112
#define HL1MDL_SKIN_SIZE 80
#define HL1MDL_XXXX_SIZE 4
#define HL1MDL_PART_SIZE 76   /**< @brief Size of each "main" bodypart item. */
#define HL1MDL_ALTS_SIZE 112  /**< @brief Size of each bodypart alternative. */
#define HL1MDL_SGRP_SIZE 20   /**< @brief Size of a group in each alt. part. */

#define HL1MDL_NAME_TINY 32
#define HL1MDL_NAME_LONG 64

static float read_float32(FILE *src)
{
  float f;
  fread(&f, 4, 1, src);
  return f;
}

static unsigned short read_ui16(FILE *src)
{
  unsigned short result;
  if (!src) { return 0; }
  result =  (unsigned short)fgetc(src) << 0;
  result += (unsigned short)fgetc(src) << 8;
  return result;
}

signed short read_si16(FILE *src)
{
  signed short i = (signed short)fgetc(src);
  i += (signed char)fgetc(src) << 8;
  return i;
}

static unsigned long read_ui32(FILE *src)
{
  unsigned long result;
  if (!src) { return 0; }
  result =  (unsigned long)fgetc(src) << 0;
  result += (unsigned long)fgetc(src) << 8;
  result += (unsigned long)fgetc(src) << 16;
  result += (unsigned long)fgetc(src) << 24;
  return result;
}

signed long read_si32(FILE *src)
{
  signed long i = (signed long)fgetc(src);
  i += (signed long)fgetc(src) << 8;
  i += (signed long)fgetc(src) << 16;
  i += (signed char)fgetc(src) << 24;
  return i;
}

int hl1mdlSaveImage(const char *name, unsigned long w, unsigned long h, FILE *src)
{
  FILE *out;
  unsigned long i;
  unsigned char palette[256*3];

  out = fopen(name, "wb");
  if (!out) { return 0; }

  fputc(0, out);
  fputc(0, out);
  fputc(2, out); /* Uncompressed RGB(A). */
  fputc(0, out); fputc(0, out);
  fputc(0, out); fputc(0, out);
  fputc(0, out);
  fputc(0, out); fputc(0, out); /* X origin. */
  fputc(0, out); fputc(0, out); /* Y origin. */
  fputc((w >> 0) & 0xFF, out); fputc((w >> 8) & 0xFF, out);
  fputc((h >> 0) & 0xFF, out); fputc((h >> 8) & 0xFF, out);
  fputc(24, out); /* Bits-per-pixel. */
  fputc(1 << 5, out);  /* Inverted origin. */

  /* The palette comes AFTER the image data... */
  {
    long old_pos = ftell(src);
    fseek(src, old_pos + w*h, SEEK_SET);
    fread(palette, 1, 256*3, src);
    fseek(src, old_pos, SEEK_SET);
  }

  for (i = 0; i < w*h; ++i)
  {
    unsigned char pal_index = fgetc(src);
    fputc(palette[pal_index*3 + 2], out);
    fputc(palette[pal_index*3 + 1], out);
    fputc(palette[pal_index*3 + 0], out);
  }

  fclose(out);
  return 1;
}

int main(int argc, char *argv[])
{
  FILE *src;
  long length, i;

  unsigned long mdl_4cc, mdl_ver;

  unsigned long num_bones, bone_offs;
  unsigned long num_aaaa, aaaa_offs;
  unsigned long num_bbbb, bbbb_offs;
  unsigned long num_cccc, cccc_offs;
  unsigned long num_dddd, dddd_offs;
  unsigned long num_image_info, image_info_offs;
  unsigned long num_image_data, image_data_offs;
  unsigned long num_image_xxxx, image_xxxx_offs;
  unsigned long num_bodyparts, bodypart_offs;
  unsigned long num_gggg, gggg_offs;
  unsigned long num_hhhh, hhhh_offs;
  unsigned long num_iiii, iiii_offs;
  unsigned long num_jjjj, jjjj_offs;

  src = fopen(argv[1], "rb");
  if (!src)
  {
    fprintf(stderr, "Could not open input file.\n");
    return 1;
  }

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  rewind(src);

  if (length < HL1MDL_HEAD_SIZE)
  {
    fprintf(stderr, "Could not open input file.\n");
    goto end_program;
  }

  mdl_4cc = read_ui32(src);
  mdl_ver = read_ui32(src);

  if (mdl_4cc != HL1MDL_IDST && mdl_4cc != HL1MDL_IDSQ)
  {
    fprintf(stderr, "Invalid 4CC: %08lx\n", mdl_4cc);
    goto end_program;
  }
  else if (mdl_ver != HL1MDL_VER)
  {
    fprintf(stderr, "Invalid version: %lu\n", mdl_ver);
    goto end_program;
  }

  /* Expected name.size vs. internal name/size. */
  fprintf(stderr, "EXTERNAL: %s (%ld bytes)\n", argv[1], length);
  fprintf(stderr, "INTERNAL: ");
  for (i = 0; i < HL1MDL_NAME_LONG; ++i)
  {
    int val = fgetc(src);
    if (val) { fprintf(stderr, "%c", val); }
  }
  fprintf(stderr, " (%lu bytes)\n", read_ui32(src));

  /* FIXME: Figure out how these models (such as scientist01.mdl) work... */
  if (mdl_4cc == HL1MDL_IDSQ)
  {
    fprintf(stderr, "Sorry, IDPQ-type MDLs aren't handled yet!\n");
    goto end_program;
  }

  /* Initial header stuff: */
  fprintf(stderr, "\nHEADER\n------\n");
  /* Eye X/Y/X position, as per Quake 1? */
  fprintf(stderr, "Eye XYZ: ");
  fprintf(stderr, "%f, ", read_float32(src));
  fprintf(stderr, "%f, ", read_float32(src));
  fprintf(stderr, "%f\n", read_float32(src));
  /* Seems to be the minimum XYZ for the bounding box, if present. */
  fprintf(stderr, "Min XYZ: ");
  fprintf(stderr, "%f, ", read_float32(src));
  fprintf(stderr, "%f, ", read_float32(src));
  fprintf(stderr, "%f\n", read_float32(src));
  /* Seems to be the maximum XYZ for the bounding box, if present. */
  fprintf(stderr, "Max XYZ: ");
  fprintf(stderr, "%f, ", read_float32(src));
  fprintf(stderr, "%f, ", read_float32(src));
  fprintf(stderr, "%f\n", read_float32(src));
  /* Always [0,0,0]? */
  fprintf(stderr, "-------: ");
  fprintf(stderr, "%f, ", read_float32(src));
  fprintf(stderr, "%f, ", read_float32(src));
  fprintf(stderr, "%f\n", read_float32(src));
  /* Always [0,0,0]? */
  fprintf(stderr, "-------: ");
  fprintf(stderr, "%f, ", read_float32(src));
  fprintf(stderr, "%f, ", read_float32(src));
  fprintf(stderr, "%f\n", read_float32(src));
  /* Flags? */
  fprintf(stderr, "FlagSet: %lu\n", read_ui32(src));

  /* List of data counts/offsets: */
  fprintf(stderr, "\nCHUNKS\n------\n");
  /* Skeleton information. */
  num_bones = read_ui32(src);
  bone_offs = read_ui32(src);
  fprintf(stderr, "Bone List: %lu\t%lu\n", num_bones, bone_offs);

  num_aaaa = read_ui32(src);
  aaaa_offs = read_ui32(src);
  fprintf(stderr, "---------: %lu\t%lu\n", num_aaaa, aaaa_offs);

  num_bbbb = read_ui32(src);
  bbbb_offs = read_ui32(src);
  fprintf(stderr, "---------: %lu\t%lu\n", num_bbbb, bbbb_offs);

  num_cccc = read_ui32(src);
  cccc_offs = read_ui32(src);
  fprintf(stderr, "---------: %lu\t%lu\n", num_cccc, cccc_offs);

  num_dddd = read_ui32(src);
  dddd_offs = read_ui32(src);
  fprintf(stderr, "---------: %lu\t%lu\n", num_dddd, dddd_offs);
  /* List of internal skins. This list describes the name/size/data offset. */
  num_image_info = read_ui32(src);
  image_info_offs = read_ui32(src);
  fprintf(stderr, "Skin Info: %lu\t%lu\n", num_image_info, image_info_offs);
  /* List of internal skins. This chunk contains the data referenced above. */
  image_data_offs = read_ui32(src);
  num_image_data = read_ui32(src);
  fprintf(stderr, "Skin Data: %lu\t%lu\n", num_image_data, image_data_offs);
  /* Not exactly sure what this does, but it's only present if skins exist. */
  num_image_xxxx = read_ui32(src);
  image_xxxx_offs = read_ui32(src);
  fprintf(stderr, "Skin XXXX: %lu\t%lu\n", num_image_xxxx, image_xxxx_offs);

  num_bodyparts = read_ui32(src);
  bodypart_offs = read_ui32(src);
  fprintf(stderr, "Bodyparts: %lu\t%lu\n", num_bodyparts, bodypart_offs);

  num_gggg = read_ui32(src);
  gggg_offs = read_ui32(src);
  fprintf(stderr, "---------: %lu\t%lu\n", num_gggg, gggg_offs);

  num_hhhh = read_ui32(src);
  hhhh_offs = read_ui32(src);
  fprintf(stderr, "---------: %lu\t%lu\n", num_hhhh, hhhh_offs);

  num_iiii = read_ui32(src);
  iiii_offs = read_ui32(src);
  fprintf(stderr, "---------: %lu\t%lu\n", num_iiii, iiii_offs);

  num_jjjj = read_ui32(src);
  jjjj_offs = read_ui32(src);
  fprintf(stderr, "---------: %lu\t%lu\n", num_jjjj, jjjj_offs);
#if 0
  /* Skeleton data. */
  if (num_bones)
  {
    fprintf(stderr, "\nSKELETON DATA\n-------------\n");
    fseek(src, bone_offs, SEEK_SET);
  }

  for (i = 0; i < num_bones; ++i)
  {
    int c;

    /* The name is only 32 character long, not 64 as it is elsewhere: */
    fprintf(stderr, "Bone %lu: ", i);
    for (c = 0; c < HL1MDL_NAME_TINY; ++c)
    {
      int val = fgetc(src);
      if (val) { fprintf(stderr, "%c", val); }
    }
    fprintf(stderr, "\n");

    fprintf(stderr, "\tParent: %ld\n", read_si32(src));
    fprintf(stderr, "\t??????: %ld\n", read_si32(src)); /* Flags? Always 0? */

    /* These always appear to be -1... */
    fprintf(stderr, "\t??????: %ld\n", read_si32(src));
    fprintf(stderr, "\t??????: %ld\n", read_si32(src));
    fprintf(stderr, "\t??????: %ld\n", read_si32(src));
    fprintf(stderr, "\t??????: %ld\n", read_si32(src));
    fprintf(stderr, "\t??????: %ld\n", read_si32(src));
    fprintf(stderr, "\t??????: %ld\n", read_si32(src));

    /* Not sure how to interpret these yet... */
    fprintf(stderr, "\t???????: ");
    fprintf(stderr, "%f, ", read_float32(src));
    fprintf(stderr, "%f, ", read_float32(src));
    fprintf(stderr, "%f\n", read_float32(src));

    fprintf(stderr, "\t???????: ");
    fprintf(stderr, "%f, ", read_float32(src));
    fprintf(stderr, "%f, ", read_float32(src));
    fprintf(stderr, "%f\n", read_float32(src));

    fprintf(stderr, "\t???????: ");
    fprintf(stderr, "%f, ", read_float32(src));
    fprintf(stderr, "%f, ", read_float32(src));
    fprintf(stderr, "%f\n", read_float32(src));

    fprintf(stderr, "\t???????: ");
    fprintf(stderr, "%f, ", read_float32(src));
    fprintf(stderr, "%f, ", read_float32(src));
    fprintf(stderr, "%f\n", read_float32(src));
  }
#endif
  /* Texture data. */
  if (num_image_info) { fprintf(stderr, "\nSKIN DATA\n---------\n"); }

  for (i = 0; i < num_image_info; ++i)
  {
    char name[HL1MDL_NAME_LONG + 5];
    unsigned long flag, w, h, offs;

    fseek(src, image_info_offs + (i * HL1MDL_SKIN_SIZE), SEEK_SET);

    fread(name, 1, HL1MDL_NAME_LONG, src);
    name[HL1MDL_NAME_LONG] = 0;

    flag = read_ui32(src);  /* Are these actually flags? Are they always 0? */
    w    = read_ui32(src);
    h    = read_ui32(src);
    offs = read_ui32(src);

    fprintf(stderr, "Skin %lu: %s\n", i, name);
    fprintf(stderr, "\tFlags?: %lu\n", flag);
    fprintf(stderr, "\tImageW: %lu\n", w);
    fprintf(stderr, "\tImageH: %lu\n", h);
    fprintf(stderr, "\tOffset: %lu\n", offs);

    fseek(src, offs, SEEK_SET);
    strcat(name, ".tga");
    if (!hl1mdlSaveImage(name, w, h, src)) { fprintf(stderr, "[WARNING] Could not save %s\n", name); }

  }

  /* Not sure what this is; there only ever seems to be 1, when skins exist. */
  if (num_image_xxxx)
  {
    fprintf(stderr, "\nSKIN XXXX\n---------\n");
    fseek(src, image_xxxx_offs, SEEK_SET);
  }

  for (i = 0; i < num_image_xxxx; ++i) { fprintf(stderr, "XXXX %lu: %lu\n", i, read_ui32(src)); }

  /* The model data is split up so that alternative heads/etc. are possible. */
  if (num_bodyparts) { fprintf(stderr, "\nBODYPART DATA\n-------------\n"); }

  for (i = 0; i < num_bodyparts; ++i)
  {
    unsigned long num_variants, unknown, variant_offs, v;

    fseek(src, bodypart_offs + (i * HL1MDL_PART_SIZE), SEEK_SET);

    fprintf(stderr, "Bodypart %lu: ", i);
    for (v = 0; v < HL1MDL_NAME_LONG; ++v)
    {
      int val = fgetc(src);
      if (val) { fprintf(stderr, "%c", val); }
    }
    fprintf(stderr, "\n");

    num_variants = read_ui32(src);
    unknown      = read_ui32(src); /* Flags/Some kind of count? */
    variant_offs = read_ui32(src);

    fprintf(stderr, "\tNum. Variants: %lu\n", num_variants);
    fprintf(stderr, "\t?????????????: %lu\n", unknown);
    fprintf(stderr, "\tVariants Offs: %lu\n", variant_offs);

    for (v = 0; v < num_variants; ++v)
    {
      unsigned long c;

      unsigned long num_groups, group_offs;
      unsigned long num_verts, vert_xxxx, vert_offs;
      unsigned long num_norms, norm_xxxx, norm_offs;

      fseek(src, variant_offs + (v * HL1MDL_ALTS_SIZE), SEEK_SET);
      fprintf(stderr, "\t\tVariant %lu: ", v);
      for (c = 0; c < HL1MDL_NAME_LONG; ++c)
      {
        int val = fgetc(src);
        if (val) { fprintf(stderr, "%c", val); }
      }
      fprintf(stderr, "\n");

      /* No idea what this is for; possibly always 0/0? */
      fprintf(stderr, "\t\t\t---------: %lu\n", read_ui32(src));
      fprintf(stderr, "\t\t\t---------: %lu\n", read_ui32(src));

      num_groups = read_ui32(src);
      group_offs = read_ui32(src);
      fprintf(stderr, "\t\t\tNum groups: %lu\n", num_groups);
      fprintf(stderr, "\t\t\tGroup offs: %lu\n", group_offs);

      num_verts = read_ui32(src);
      vert_xxxx = read_ui32(src);
      vert_offs = read_ui32(src);
      fprintf(stderr, "\t\t\tNum verts: %lu\n", num_verts);
      fprintf(stderr, "\t\t\tVert ????: %lu\n", vert_xxxx); /* Bone IDs? */
      fprintf(stderr, "\t\t\tVert offs: %lu\n", vert_offs);

      num_norms = read_ui32(src);
      norm_xxxx = read_ui32(src);
      norm_offs = read_ui32(src);
      fprintf(stderr, "\t\t\tNum norms: %lu\n", num_norms);
      fprintf(stderr, "\t\t\tNorm ????: %lu\n", norm_xxxx); /* Bone IDs? */
      fprintf(stderr, "\t\t\tNorm offs: %lu\n", norm_offs);

      /* No idea what this is for; possibly always 0/0? */
      fprintf(stderr, "\t\t\t---------: %lu\n", read_ui32(src));
      fprintf(stderr, "\t\t\t---------: %lu\n", read_ui32(src));
#if 0
      fseek(src, vert_offs, SEEK_SET);
      for (c = 0; c < num_verts; ++c)
      {
        fprintf(stderr, "\t\t\t\tVertex %lu: ", c);
        fprintf(stderr, "%f, ", read_float32(src));
        fprintf(stderr, "%f, ", read_float32(src));
        fprintf(stderr, "%f\n", read_float32(src));
      }

      fseek(src, norm_offs, SEEK_SET);
      for (c = 0; c < num_norms; ++c)
      {
        fprintf(stderr, "\t\t\t\tNormal %lu: ", c);
        fprintf(stderr, "%f, ", read_float32(src));
        fprintf(stderr, "%f, ", read_float32(src));
        fprintf(stderr, "%f\n", read_float32(src));
      }
#endif
      for (c = 0; c < num_groups; ++c)
      {
        unsigned long num_tris, tri_offs, skin_id, norms_used, flags;
        unsigned long total_triangles, p;

        fseek(src, group_offs + (c * HL1MDL_SGRP_SIZE), SEEK_SET);
        fprintf(stderr, "\t\t\t\tGroup %lu:\n", c);

        num_tris   = read_ui32(src);
        tri_offs   = read_ui32(src);
        skin_id    = read_ui32(src);  /* TODO: Check that this is correct... */
        norms_used = read_ui32(src);
        flags      = read_ui32(src);  /* Not completely sure what this is... */

        fprintf(stderr, "\t\t\t\t\tTris: %lu\n", num_tris);
        fprintf(stderr, "\t\t\t\t\tOffs: %lu\n", tri_offs);
        fprintf(stderr, "\t\t\t\t\tSkin: %lu\n", skin_id);
        fprintf(stderr, "\t\t\t\t\tNorm: %lu\n", norms_used);
        fprintf(stderr, "\t\t\t\t\tFlag: %lu\n", flags);

        /*
        NOTE: The last polygon in a group may be followed by padding bytes, for
        alignment. I'm guessing that the "polygon lists" are supposed to use 16
        byte alignment since a few files I've found have four zero bytes at the
        end of the list, so it's >4-byte alignment (and 8-byte seems unlikely).
        */
#if 0
        fseek(src, tri_offs, SEEK_SET);
        for (total_triangles = 0, p = 0; total_triangles < num_tris; ++p)
        {
          short num_points, q;

          num_points = read_si16(src);
          fprintf(stderr, "\t\t\t\t\t\tPolygon %lu: %d points\n", p, num_points);

          /* I presume that this indicates reverse-windings? */
          if (num_points < 0) { num_points = 0 - num_points; }
          /* Each polygon may have multiple triangles. */
          total_triangles += num_points - 2;

          for (q = 0; q < num_points; ++q)
          {
            unsigned short vertex_id, normal_id, texture_u, texture_v;

            vertex_id = read_ui16(src);
            normal_id = read_ui16(src);
            texture_u = read_ui16(src);
            texture_v = read_ui16(src);

            fprintf(stderr, "\t\t\t\t\t\t\tPoint %d:\n", q);
            fprintf(stderr, "\t\t\t\t\t\t\t\tVertex ID: %d\n", vertex_id);
            fprintf(stderr, "\t\t\t\t\t\t\t\tNormal ID: %d\n", normal_id);
            fprintf(stderr, "\t\t\t\t\t\t\t\tTexture U: %d\n", texture_u);
            fprintf(stderr, "\t\t\t\t\t\t\t\tTexture V: %d\n", texture_v);
          }
        }
#endif
      }

    }

  }

  end_program:
  fclose(src);
  return 0;
}
