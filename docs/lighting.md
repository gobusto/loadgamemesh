Light Data File Formats
=======================

This document describes 3D file formats which can contain light data, either in
addition to basic mesh data or as part of a standalone file. Note that "lights"
refers to point, spot, or directional lights, rather than lightmaps or similar.

3D Studio .3ds + .ase
---------------------

3DS files are capable of storing lights via the OBJ_LIGHT (0x4600) chunk:

<http://paulbourke.net/dataformats/3ds/>

ASE files are also able to store lights, using a LIGHTOBJECT directive:

<http://wiki.beyondunreal.com/Legacy:ASE_File_Format>

Unfortunately, I haven't been able to find any editor (or Blender scripts) that
include lights when exporting to either of those formats, presumably because it
isn't something that most games require.

AC3D .ac
--------

AC3D files include lights, but few editors support them. A basic description of
the AC3D format can be found here:

<http://paulbourke.net/dataformats/ac3d/>

Sample .ac files can be found here:

<http://www.cs.jhu.edu/~cohen/VW2000/gallery.html>

Autodesk .vue
-------------

VUE files support animated lights, and are documented on the Autodesk website:

<http://download.autodesk.com/us/3dsmax/2012help/files/GUID-BEADCF00-3BBA-4722-9D7D-C07C15F8A33-2278.htm>

A .vue export script for Blender is available here:

<http://wiki.blender.org/index.php/Extensions:2.6/Py/Scripts/Import-Export/VueCameraExport>

Blitz3D.b3d
-----------

Support for lights was/is planned, but wasn't/isn't implemented:

> There are many more 'kind' chunks coming, including camera, light, sprite,
> plane etc. For now, the use of a Pivot in cases where the node kind is
> unknown will allow for backward compatibility.

<http://www.blitzbasic.com/sdkspecs/sdkspecs/b3dfile_specs.txt>

Cinema4D .c4d
-------------

C4D files can describe lights. A specification is available here:

<http://paulbourke.net/dataformats/cinema4d>

The downside is that .c4d files aren't widely supported by anything other than
Cinema4D itself. Additionally, new versions of the file format are not publicly
documented:

<http://www.plugincafe.com/forum/forum_posts.asp?TID=740>

Dark Engine .bin
----------------

According to the Open Dark Engine source code, mesh files can contain lights:

<http://sourceforge.net/p/opde/code/HEAD/tree/trunk/src/tools/meshconvert.h>

However, this file format is not widely supported; as far as I'm aware, custom
.BIN files are usually made by running a .3DS file through a conversion progam.

Darkplaces .lights + .rtlights
------------------------------

The format of .lights and .rtlights files isn't documented anywhere, but error
messages within the Darkplaces console explain how they work. These errors can
be found in the `r_shadow.c` source code file:

<https://gitorious.org/nexuiz/darkplaces/source/71b12f4105264dc0bb0f307c8c06de09ecb44cc3:r_shadow.c>

The following error message is shown if an invalid `.lights` file is specified:

`Con_Printf("invalid lights file, found %d parameters on line %i, should be 14 parameters (origin[0] origin[1] origin[2] falloff light[0] light[1] light[2] subtract spotdir[0] spotdir[1] spotdir[2] spotcone distancebias style)\n", a, n + 1);`

For `.rtlights` files, a different message is shown:

`Con_Printf("found %d parameters on line %i, should be 8 or more parameters (origin[0] origin[1] origin[2] radius color[0] color[1] color[2] style \"cubemapname\" corona angles[0] angles[1] angles[2] coronasizescale ambientscale diffusescale specularscale flags)\n", a, n + 1);`

id/Valve .bsp + .ent
--------------------

Quake and Half-Life BSP maps include lights within their entity text string:

<http://www.gamers.org/dEngine/quake2/Q2DP/Q2DP_Map/Q2DP_Map-3.html>

It is also possible to store lights (and other things) in a separate .ent file:

<http://quakeone.com/forums/quake-help/quake-clients/8336-tut-add-particle-effects-models-sounds-into-quake-via-ent-files-darkplaces.html>

Panda3D .egg
------------

The `PointLight` tag allows lights to be specified:

<http://panda3d.cvs.sourceforge.net/panda3d/panda/src/doc/eggSyntax.txt?view=markup>

Others
------

Collada .dae and Extensible3D .x3d files can both describe lights, but they're
rather complex XML-based formats, which could mean that they'd be tricky to do
correctly. The Collada specification can be found here:

<http://paulbourke.net/dataformats/collada/>

FBX files are another option, but they don't seem to be publicly documented.
