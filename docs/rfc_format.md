Unofficial Red Faction .RFC/.RFM file format specification
==========================================================

Red Faction uses various custom file formats for its data:

+ VPP (Volition Project Package?) files are archives (like Quake PAK files).
+ RFM (Red Faction Model) files store static (non-animated) models.
+ RFC (Red Faction Character) files store skeletal models.
+ RFA (Red Faction Animation) files store animation data for skeletal models.
+ PEG files store textures for models, and probably other "general" images too.

...plus a few others. For now, this document only covers RFC and RFM files.

General notes:

+ Integers are stored as little-endian, twos-complement values, usually 32-bit.
+ Real values are stored as IEEE 32-bit floating point values.
+ Strings seem to be ASCII, but config files use Latin-1, so maybe it's that...

-------------------------------------------------------------------------------

General structure
-----------------

In Red Faction 1, RFM and RFC files start with a 32-byte header:

+ UINT32 Magic number: 0x87128712
+ UINT32 Version: 0 for RFM files, or 1 for RFC files.
+ UINT32 Always 1? Possibly the number of NULL chunks.
+ UINT32 Always 1? Possibly the number of SKIN chunks.
+ UINT32 Number of DATA chunks. Usually 1, but sometimes more.
+ UINT32 Number of CSPH chunks.
+ UINT32 Number of DUMB chunks.
+ UINT32 Number of textures in the SKIN chunk.

The rest of the file is divided into chunks, in the following format:

+ UINT32 Chunk type. Sometimes a 4-character ASCII string, but not always.
+ UINT32 Chunk size in bytes, excluding the 8 bytes used for this and the type.
+ VARIES Chunk data.

Red Faction 2 uses a somewhat different format. The RFC header is 64 bytes:

+ UINT32 Magic number: 0x87128712
+ UINT32 Version. Always 10.
+ UINT32 ???
+ UINT32 ???
+ UINT32 Number of CSPH chunks.
+ UINT32 Number of DUMB chunks.
+ UINT32 Number of BONE chunks.
+ UINT32 Number of SKIN chunks.
+ UINT32 ???
+ UINT32 ???
+ UINT32 Padding; ignore.
+ UINT32 Padding; ignore.
+ UINT32 Padding; ignore.
+ UINT32 Padding; ignore.
+ UINT32 Padding; ignore.
+ UINT32 Padding; ignore.

Chunks no longer seem to have headers; instead, they appear in a set order, and
the "counters" in the header define how many of them there are:

+ RFC Header.
+ ...padding to next 64-byte boundary...
+ All CSPH chunks.
+ ...padding to next 64-byte boundary...
+ All DUMB chunks.
+ ...padding to next 64-byte boundary...
+ All BONE chunks.
+ ...padding to next 64-byte boundary...
+ All SKIN chunks.
+ ...padding to next 64-byte boundary...
+ Unknown - Not yet researched.

I haven't investigated the RFM format used by Red Faction 2 yet.

Details for each chunk type are outlined below. Unless otherwise specified, the
information describes the version 1 format used by the original game.

-------------------------------------------------------------------------------

CSPH chunk
----------

These chunks describe a collision sphere. They aren't present in all files.

+ UINT32 Chunk type: 43535048 (spells "CSPH" if interpreted as ASCII text).
+ UINT32 Chunk size. Always exactly 44 bytes.
+ CHAR24 Tag name.
+ SINT32 Seems to be a parent bone ID. May be -1, as with BONE and DUMB chunks.
+ REAL32 X coordinate.
+ REAL32 Y coordinate.
+ REAL32 Z coordinate.
+ REAL32 Radius.

Except for the lack of a chunk header, these seem to be the same in Red Faction
2 (version 10) files.

-------------------------------------------------------------------------------

DUMB chunk
----------

I'm not completely sure what these chunks are for. They are similar to the bone
data in the BONE chunk (although the "parent" index is in a different place) so
perhaps they're used to attach models to other models, like Quake III MD3 tags?

These are present in a lot of files, but aren't always included (see fish.rfc).

+ UINT32 Chunk type: 0x44554d42 (spells "DUMB" if interpreted as ASCII text).
+ UINT32 Chunk size. Always exactly 56 bytes.
+ CHAR24 Tag name.
+ SINT32 Parent tag index. Can be -1 for the "root" tag.
+ REAL32 Possibly the quanternion W component?
+ REAL32 Possibly the quanternion X component?
+ REAL32 Possibly the quanternion Y component?
+ REAL32 Possibly the quanternion Z component?
+ REAL32 Possibly the X position?
+ REAL32 Possibly the Y position?
+ REAL32 Possibly the Z position?

In Red Faction 2, these chunks are 96 bytes long, so the format differs:

+ CHAR24 Tag name.
+ BYTE72 Unknown - Not yet researched.

-------------------------------------------------------------------------------

BONE chunk
----------

This chunk describes the bone data used to skeletally animate the mesh.

+ UINT32 Chunk type: 0x424f4e45 (spells "BONE" if interpreted as ASCII text).
+ UINT32 Chunk size.
+ UINT32 Number of bones.

Then, for each bone, there are 56-bytes of data:

+ CHAR24 Bone name
+ REAL32 Possibly the quanternion W component?
+ REAL32 Possibly the quanternion X component?
+ REAL32 Possibly the quanternion Y component?
+ REAL32 Possibly the quanternion Z component?
+ REAL32 Possibly the X position?
+ REAL32 Possibly the Y position?
+ REAL32 Possibly the Z position?
+ SINT32 Parent bone index. Can be -1 for the "root" bone.

Since Red Faction 2 no longer uses chunks, each bone is simply listed in order.
The format has changed, too - they're now 76 bytes long:

+ CHAR24 Bone name.
+ BYTE52 Unknown - Not yet researched.

-------------------------------------------------------------------------------

DATA chunk
----------

These chunks contain all of the vertex, normal, and texture-coordinate data. In
some files, more than one of these is present - each represents a different LOD
(level of detail) for the same model. It *seems* that they are arranged so that
the highest-quality version is first, and the lowest-quality version is last.

NOTE: No information about this chunk is available for Red Faction 2 files yet;
the following details are based on the v0 and v1 files used by the first game.

+ UINT32 Chunk type: 0x87251110
+ UINT32 Chunk size.
+ UINT32 Indicates the level-of-detail (higher = worse) - often 0x7f7fffff.
+ UINT32 Always 5.
+ UINT32 Flags?
+ UINT32 Length of UNKNOWN array (see below).
+ REAL32 Bounding Box - max X coordinate.
+ REAL32 Bounding Box - max Y coordinate.
+ REAL32 Bounding Box - max Z coordinate.
+ REAL32 Bounding Box - min X coordinate.
+ REAL32 Bounding Box - min Y coordinate.
+ REAL32 Bounding Box - min Z coordinate.
+ REAL32 Origin X coordinate.
+ REAL32 Origin Y coordinate.
+ REAL32 Origin Z coordinate.
+ REAL32 Radius of bounding sphere.
+ UINT32 Length of the data blob.

After this comes a blob of data. To make sense of it, skip past it and read the
"table of contents" that follows:

+ UINT16 Number of objects.

Then, for each object:

+ UINT16 Number of vertices.
+ UINT16 Number of triangles.
+ UINT16 Length of vertex data.
+ UINT16 Length of triangle data.
+ UINT16 Length of property data.

Finally, there comes another UINT32 value:

+ UINT32 Number of textures.

This is followed by a list of NULL-terminated texture file name strings:

+ CHAR?? Texture file name, terminated with a zero byte.

This list is a subset of the one in the SKIN chunk. Oddly, this seems to be the
"real" texture list used by triangles to describe which texture they have... so
I have no idea why the SKIN chunk exists!

Now that we know how the data blob is structured, we can go back and load it.

For each object, there are 5 lists of data, in the following order:

+ Object 1 Vertices
+ Object 1 Normals
+ Object 1 Triangles
+ Object 1 Properties
+ Object 1 UNKNOWN (Only in RFC files - absent from RFM models)
+ Object 2 Vertices
+ Object 2 Normals
+ etc.

### Vertices

Each vertex is represented as three IEEE 32-bit floating point values:

+ REAL32 - Vertex X position.
+ REAL32 - Vertex Y position.
+ REAL32 - Vertex Z position.

The data may be followed by a few zero bytes, to pad it out to the next 16-byte
boundary. This is why the object "header" has a separate "vertex length" value,
in addition to the "vertex count" we're interested in.

### Normals

There is one normal for each vertex, so the vertex count/length values are also
used to describe the normal data.

+ REAL32 - Normal X value.
+ REAL32 - Normal Y value.
+ REAL32 - Normal Z value.

As with the vertex data above, the normal data may be padded with zeroes.

### Triangles

Each triangle is represented as an 80-byte structure:

+ UINT32 - 1st vertex/normal index.
+ UINT32 - 2nd vertex/normal index.
+ UINT32 - 3rd vertex/normal index.
+ UINT32 - Texture index (from the list described above, NOT the SKIN chunk).
+ REAL32 - Face normal X value.
+ REAL32 - Face normal Y value.
+ REAL32 - Face normal Z value.
+ UINT32 - Flags?
+ REAL32 - 1st texture coordinate U value.
+ REAL32 - 1st texture coordinate V value.
+ REAL32 - Flags?
+ UINT32 - More flags?
+ REAL32 - 2nd texture coordinate U value.
+ REAL32 - 2nd texture coordinate V value.
+ REAL32 - Flags?
+ UINT32 - More flags?
+ REAL32 - 2nd texture coordinate U value.
+ REAL32 - 2nd texture coordinate V value.
+ REAL32 - Flags?
+ UINT32 - More flags?

Note that textures are assignable per-triangle, not per-object, and some models
really do have differently-textured triangles within a single object; you can't
assume that a group can be drawn all-at-once with a single texture, sadly...

Since 80 is cleanly divisible by 16, no padding is necessary after this list.

### Properties

The list has `num_vertices` items. Its exact purpose is unknown.

Each item is 8 bytes long, so the list may be padded with zeroes to meet the 16
byte boundary requirement.

### UNKNOWN

The purpose of this list isn't known; it's the same size for each object (based
on the "UNKNOWN length" field at the start of the DATA chunk) and simply stores
a list of signed, 8-bit values.

It's only present in RFC files, so it's likely that it associates vertices with
bones somehow.

This list may need to be padded with zeroes in order to ensure that it finishes
on a 16-byte boundary, but there is no explicit size field for it in the object
"header" list; you will need to round it up to the next-highest divisible-by-16
value yourself.

-------------------------------------------------------------------------------

SKIN chunk
----------

In Red Faction, this chunk simply contains a list of texture file names.

+ UINT32 Chunk type: 0x11133344
+ UINT32 Chunk size.
+ UINT32 Number of textures.

Then, for each texture:

+ CHAR80 File name ("hand1st.tga" or similar).
+ UINT32 Flags? (Usually zero, but sometimes one...)

Red Faction 2 is radically different; each skin is 364 bytes in size:

+ unknown data until `offset_to_start_of_skin` + 20
+ CHAR?? - File name. Zero-terminated, but I'm not sure about the "real" size.
+ unknown data until `offset_to_start_of_skin` + 208
+ CHAR?? - Reflection map file name. Seems to be 32-bytes in size...?
+ unknown data until `offset_to_start_of_skin` + 364

Note that not all skins will have a reflection map, in which case the file name
string is zero characters long (i.e. the first character is a zero).

-------------------------------------------------------------------------------

NULL chunk
----------

This chunk does not contain any data; it simply indicates the end of the file.

+ UINT32 Chunk type: 0x00000000
+ UINT32 Chunk size. Always zero.

Obviously this doesn't apply to Red Faction 2, since it doesn't use chunks.

-------------------------------------------------------------------------------

DISCLAIMER: The information in this document is based on reverse-engineering of
RFC files extracted from the VPP archives on the PlayStation 2 PAL (SLES-50277)
version of Red Faction. It is completely unofficial, and is neither endorsed by
nor affiliated with Volition Software in any way. Other versions of the product
(such as the PC or N-GAGE ports) may differ from the details described here.

Red Faction II data is based on the PlayStation 2 PAL (SLES-51133) version.
