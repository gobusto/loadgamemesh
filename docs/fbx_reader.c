/**
@brief A simple program to read binary FBX files. **CURRENTLY INCOMPLETE!**

See <https://code.blender.org/2013/08/fbx-binary-file-format-specification/>
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

float read_float32(FILE *src)
{
  float f;
  fread(&f, 4, 1, src);
  return f;
}

float read_float64(FILE *src)
{
  double f;
  fread(&f, 8, 1, src);
  return f;
}

signed short read_si16(FILE *src)
{
  signed short i = (signed short)fgetc(src);
  i += (signed char)fgetc(src) << 8;
  return i;
}

unsigned short read_ui16(FILE *src)
{
  unsigned short i = (unsigned short)fgetc(src);
  i += (unsigned short)fgetc(src) << 8;
  return i;
}

signed long read_si32(FILE *src)
{
  signed long i = (signed long)fgetc(src);
  i += (signed long)fgetc(src) << 8;
  i += (signed long)fgetc(src) << 16;
  i += (signed char)fgetc(src) << 24;
  return i;
}

unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  i += (unsigned long)fgetc(src) << 24;
  return i;
}

#define fbxScalar(x) (\
  (x) == FBX_BOOL  || (x) == FBX_FLOAT32 || (x) == FBX_FLOAT64 ||\
  (x) == FBX_INT16 || (x) == FBX_INT32   || (x) == FBX_INT64\
)

#define fbxBuffer(x) ((x) == FBX_STRING || (x) == FBX_BINARY)

#define fbxArray(x) (!fbxScalar(x) && !fbxBuffer(x))

typedef enum
{

  FBX_BOOL = 'C',
  FBX_INT16 = 'Y',
  FBX_INT32 = 'I',
  FBX_INT64 = 'L',
  FBX_FLOAT32 = 'F',
  FBX_FLOAT64 = 'D',

  FBX_BOOL_ARRAY = 'b',
  /* ...INT16 array...? */
  FBX_INT32_ARRAY = 'i',
  FBX_INT64_ARRAY = 'l',
  FBX_FLOAT32_ARRAY = 'f',
  FBX_FLOAT64_ARRAY = 'd',

  FBX_STRING = 'S',
  FBX_BINARY = 'R'

} fbx_data_t;

int fbxDataTypeSize(fbx_data_t data_type)
{
  switch (data_type)
  {
    case FBX_BOOL:    case FBX_BOOL_ARRAY:    return 1;
    case FBX_INT16:                           return 2;
    case FBX_INT32:   case FBX_INT32_ARRAY:   return 4;
    case FBX_INT64:   case FBX_INT64_ARRAY:   return 8;
    case FBX_FLOAT32: case FBX_FLOAT32_ARRAY: return 4;
    case FBX_FLOAT64: case FBX_FLOAT64_ARRAY: return 8;
    case FBX_STRING:  case FBX_BINARY:        return 0; /* lol idk */
    default: break;
  }
  return 0;
}

void fbxReadProperty(FILE *src, fbx_data_t data_type)
{
  switch (data_type)
  {
    case FBX_BOOL: case FBX_BOOL_ARRAY:
      fprintf(stderr, "%d\n", fgetc(src));
    break;

    case FBX_INT16:
      fprintf(stderr, "%u\n", read_ui16(src));
    break;

    case FBX_INT32: case FBX_INT32_ARRAY:
      fprintf(stderr, "%lu\n", read_ui32(src));
    break;

    case FBX_INT64: case FBX_INT64_ARRAY:
      fprintf(stderr, "0x%08lx%08lx\n", read_ui32(src), read_ui32(src));
    break;

    case FBX_FLOAT32: case FBX_FLOAT32_ARRAY:
      fprintf(stderr, "%f\n", read_float32(src));
    break;

    case FBX_FLOAT64: case FBX_FLOAT64_ARRAY:
      fprintf(stderr, "%f\n", read_float64(src));
    break;

    case FBX_STRING: case FBX_BINARY: break;
    default: break;
  }
}

int readRecord(FILE *src, long length, unsigned long indent)
{
  unsigned long i, p;

  unsigned long next_offs;

  unsigned long prop_count;
  unsigned long prop_bytes;
  unsigned long prop_offs;

  unsigned long list_bytes;
  unsigned long list_offs;

  unsigned char name_bytes;
  char name[256];

  memset(name, 0, 256);

  /* Read the 13-byte record header (may be all zeroes for "NULL" records): */
  next_offs = read_ui32(src);
  prop_count = read_ui32(src);
  prop_bytes = read_ui32(src);
  name_bytes = fgetc(src);

  /* If the next offset doesn't make sense, stop here: */
  if (next_offs < 13 || next_offs > length) { return 0; }

  if (name_bytes > 0) { fread(name, 1, name_bytes, src); }

  prop_offs = ftell(src);
  list_offs = prop_offs + prop_bytes;
  list_bytes = next_offs - list_offs;

  for (i = 0; i < indent; ++i) { fputc('\t', stderr); }
  fprintf(stderr, "RECORD: '%s': ", name);
  fprintf(stderr, "%lu properties (%lu bytes)", prop_count, prop_bytes);
  fprintf(stderr, "%s\n", list_bytes > 0 ? " with a nested list" : "");

  /* Read properties: */
  fseek(src, prop_offs, SEEK_SET);
  for (p = 0; p < prop_count; ++p)
  {
    unsigned char prop_type = fgetc(src);

    for (i = 0; i < indent + 1; ++i) { fputc('\t', stderr); }
    fprintf(stderr, "PROPERTY #%lu is a '%c': ", p, prop_type);

    if (fbxScalar(prop_type))
    {
      fbxReadProperty(src, prop_type);
    }
    else if (fbxArray(prop_type))
    {
      unsigned long number_of_items, compression_type, compressed_size;

      number_of_items = read_ui32(src);
      compression_type = read_ui32(src);
      compressed_size = read_ui32(src);

      fprintf(stderr, "%lu items, %lu bytes", number_of_items, compressed_size);

      if (compression_type == 0)
      {
        unsigned long x;
        fprintf(stderr, " (Uncompressed)\n");
        for (x = 0; x < number_of_items; ++x) { fbxReadProperty(src, prop_type); }
      }
      else
      {
        fprintf(stderr, " (ZLIB compressed)\n");
        fseek(src, compressed_size, SEEK_CUR);
      }
    }
    else if (fbxBuffer(prop_type))
    {
      unsigned long data_length, x;
      data_length = read_ui32(src);

      fprintf(stderr, "'");
      for (x = 0; x < data_length; ++x)
      {
        int value = fgetc(src);
        if (prop_type == FBX_STRING) { fputc(value, stderr); }
      }
      if (prop_type == FBX_BINARY) { fprintf(stderr, "<binary>"); }
      fprintf(stderr, "' (%lu bytes)\n", data_length);
    }
  }

  /* Read sub-chunks: */
  fseek(src, list_offs, SEEK_SET);
  while (ftell(src) < next_offs && ftell(src) < length)
  {
    int val = readRecord(src, length, indent + 1);
    if (val != 0) { return val; }
  }

  fseek(src, next_offs, SEEK_SET);
  return 0;
}

int read_file(FILE *src)
{
  char file_type[21];
  unsigned long version;
  long length;

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  rewind(src);

  fprintf(stdout, "SIZE: %ld bytes\n", length);
  if (length < 27 + 13)
  { return fprintf(stderr, "File is too short\n"); }

  fread(file_type, 1, 21, src);
  if (memcmp(file_type, "Kaydara FBX Binary  ", 21) != 0)
  { return fprintf(stderr, "Not a binary FBX file\n"); }

  if (read_ui16(src) != 0x001A)
  { return fprintf(stderr, "Unknown FBX type.\n"); }

  /* Should be 7400, but of course there may be other versions too... */
  version = read_ui32(src);
  fprintf(stderr, "VERSION: %lu\n", version);

  while (ftell(src) < length)
  {
    int val = readRecord(src, length, 0);
    if (val != 0) { return val; }
  }

  return 0;
}

int main(int argc, char **argv)
{
  int i;
  for (i = 1; i < argc; ++i)
  {
    FILE *src = fopen(argv[i], "rb");
    fprintf(stdout, "NAME: %s\n", argv[i]);
    if (src) { read_file(src); fclose(src); }
    else     { fprintf(stderr, "Could not open %s\n", argv[i]);  }
  }
  return 0;
}
