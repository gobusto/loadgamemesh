/**
@file
@brief A simple program for converting Red Faction RFL files into OBJ format
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* The full output is rather long, so undefine this to make things quieter... */
/*
#define TELL_ME_EVERYTHING
*/
/* Constants: */

#define RFL_4CC 0xD4BADA55 /**< @brief Magic number for Volition RFL files. */

#define RFL_CHUNK_MESH        0x00000100  /**< @brief Contains a single mesh. */
#define RFL_CHUNK_MATRIX_LIST 0x00000200  /**< @brief List of matrices...?   */
#define RFL_CHUNK_A_03        0x00000300  /**< @brief ??? */
#define RFL_CHUNK_A_04        0x00000400  /**< @brief ??? */
#define RFL_CHUNK_A_05        0x00000500  /**< @brief ??? */
#define RFL_CHUNK_TRIGGER     0x00000600  /**< @brief Map change events/etc. */
#define RFL_CHUNK_A_07        0x00000700  /**< @brief ??? */
#define RFL_CHUNK_ZERO        0x00000800  /**< @brief Always 12 zero bytes.  */
#define RFL_CHUNK_ROCK        0x00000900  /**< @brief GeoMod rock texture..? */
#define RFL_CHUNK_A_0A        0x00000a00  /**< @brief ??? */
#define RFL_CHUNK_A_0B        0x00000b00  /**< @brief ??? */
#define RFL_CHUNK_A_0C        0x00000c00  /**< @brief ??? */
#define RFL_CHUNK_A_0D        0x00000d00  /**< @brief ??? */
#define RFL_CHUNK_A_0E        0x00000e00  /**< @brief ??? */
#define RFL_CHUNK_A_0F        0x00000f00  /**< @brief ??? */

#define RFL_CHUNK_B_10  0x00001000 /**< @brief ??? */
#define RFL_CHUNK_B_11  0x00001100 /**< @brief ??? */
#define RFL_CHUNK_B_12  0x00001200 /**< @brief ??? */
#define RFL_CHUNK_B_20  0x00002000 /**< @brief ??? */
#define RFL_CHUNK_B_30  0x00003000 /**< @brief ??? */
#define RFL_CHUNK_B_40  0x00004000 /**< @brief ??? */
#define RFL_CHUNK_B_50  0x00005000 /**< @brief ??? */
#define RFL_CHUNK_LIST1 0x00006000 /**< @brief Lists of lists of offsets? */

#define RFL_CHUNK_TGAS 0x00007000 /**< @brief List of .TGA "effects" images. */
#define RFL_CHUNK_VCMS 0x00007001 /**< @brief List of .VCM character models. */
#define RFL_CHUNK_MVFS 0x00007002 /**< @brief List of .MVF character anim's. */
#define RFL_CHUNK_V3DS 0x00007003 /**< @brief List of .V3D (static) models.  */
#define RFL_CHUNK_VFXS 0x00007004 /**< @brief List of .VFX visual effects.   */

#define RFL_CHUNK_LIST2  0x00010000 /**< @brief Lists of lists of offsets? */
#define RFL_CHUNK_D_02   0x00020000 /**< @brief ??? */
#define RFL_CHUNK_D_03   0x00030000 /**< @brief ??? */
#define RFL_CHUNK_D_04   0x00040000 /**< @brief ??? */
#define RFL_CHUNK_D_05   0x00050000 /**< @brief ??? */
#define RFL_CHUNK_D_06   0x00060000 /**< @brief ??? */
#define RFL_CHUNK_MATRIX 0x00070000 /**< @brief XYZ position + 3x3 matrix? */

#define RFL_CHUNK_META 0x01000000 /**< @brief Level name/authors/other info. */
#define RFL_CHUNK_MESH_LIST 0x02000000  /**< @brief Contains several meshes. */
#define RFL_CHUNK_E_03 0x03000000 /**< @brief ??? */
#define RFL_CHUNK_E_04 0x04000000 /**< @brief ??? */

#define RFL_CHUNK_NULL 0x00000000 /**< @brief Indicates end-of-file. */

/* Helper functions */

float read_float32(FILE *src)
{
  float f;
  fread(&f, 4, 1, src);
  return f;
}

signed short read_si16(FILE *src)
{
  signed short i = (signed short)fgetc(src);
  i += (signed char)fgetc(src) << 8;
  return i;
}

unsigned short read_ui16(FILE *src)
{
  unsigned short i = (unsigned short)fgetc(src);
  i += (unsigned short)fgetc(src) << 8;
  return i;
}

signed long read_si32(FILE *src)
{
  signed long i = (signed long)fgetc(src);
  i += (signed long)fgetc(src) << 8;
  i += (signed long)fgetc(src) << 16;
  i += (signed char)fgetc(src) << 24;
  return i;
}

unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  i += (unsigned long)fgetc(src) << 24;
  return i;
}

unsigned short read_string(FILE *src, char *out)
{
  unsigned short count = read_ui16(src);
  unsigned short i;
  for (i = 0; i < count; ++i)
  {
    int value = fgetc(src);
    if (out) { out[i] = value; }
    fputc(value, stderr);
  }
  if (out) { out[i] = 0; }
  return count + 2;
}

void read_xyz(FILE *src)
{
  float x, y, z;

  x = read_float32(src);
  y = read_float32(src);
  z = read_float32(src);

  fprintf(stderr, "%f, %f, %f\n", x, y, z);
}

void read_matrix(FILE *src)
{
  int j;
  for (j = 0; j < 3*3; ++j)
  {
    if (j % 3 == 0) { fprintf(stderr, "\t\t"); }
    fprintf(stderr, "%f", read_float32(src));
    fputc(j % 3 == 2 ? '\n' : '\t', stderr);
  }
}

void data_dump(FILE *src, unsigned long count)
{
  unsigned long i = 0;
  float         f = 0.0;

  for (i = 0; i < count; ++i)
  {
    int value = fgetc(src);
    fprintf(stderr, "%lu: %d\t0x%02x\t%c\n", i, value, value, value);

    ((unsigned char*)&f)[i%4] = (unsigned char)value;

    if ((i % 4) == 3)
    {
      long x;
      fseek(src, -4, SEEK_CUR);
      x = read_si32(src);
      fprintf(stderr, "\tSINT32: %ld | FLOAT: %f\n\n", x, f);
    }
  }
}

void describe_group(FILE *src, unsigned long version)
{
  unsigned long num_textures;
  unsigned long num_aaaa;
  unsigned long num_bbbb;
  unsigned long num_cccc;
  unsigned long num_vertices;
  unsigned long num_polys;
  unsigned long num_dddd;
  unsigned long num_eeee;
  unsigned long j;

  j = read_ui16(src);
  fprintf(stderr, "\tZero? %lu\n", j);
  if (j) { fprintf(stderr, "UNEXPECTED: %lu\n", j); exit(123); }

  j = read_ui16(src);
  fprintf(stderr, "\tZero? %lu\n", j);
  if (j) { fprintf(stderr, "UNEXPECTED: %lu\n", j); exit(123); }

  j = read_ui16(src);
  fprintf(stderr, "\tZero? %lu\n", j);
  if (j) { fprintf(stderr, "UNEXPECTED: %lu\n", j); exit(123); }

  num_textures = read_ui32(src);
  fprintf(stderr, "\t%lu textures...\n", num_textures);
  for (j = 0; j < num_textures; ++j)
  {
    fprintf(stderr, "\t\t%lu: ", j);
    read_string(src, NULL);
    fprintf(stderr, "\n");
  }

  j = read_ui32(src);
  fprintf(stderr, "\tZero? %lu\n", j);
  if (j) { fprintf(stderr, "UNEXPECTED: %lu\n", j); exit(123); }

  num_aaaa = read_ui32(src);
  fprintf(stderr, "\t%lu items in UNKNOWN LIST A...\n", num_aaaa);
  for (j = 0; j < num_aaaa; ++j)
  {
    unsigned long flags_a, flags_b;

    fprintf(stderr, "\t\tItem %lu:\n", j);

    fprintf(stderr, "\t\t\t???????: %ld\n", read_si32(src));

    fprintf(stderr, "\t\t\t???????: %f\n",  read_float32(src));
    fprintf(stderr, "\t\t\t???????: %f\n",  read_float32(src));
    fprintf(stderr, "\t\t\t???????: %f\n",  read_float32(src));

    fprintf(stderr, "\t\t\t???????: %f\n",  read_float32(src));
    fprintf(stderr, "\t\t\t???????: %f\n",  read_float32(src));
    fprintf(stderr, "\t\t\t???????: %f\n",  read_float32(src));

    flags_a = read_ui32(src);
    fprintf(stderr, "\t\t\tFlags A: %lu\n", flags_a);

    flags_b = read_ui32(src);
    fprintf(stderr, "\t\t\tFlags B: %lu\n", flags_b);

    fprintf(stderr, "\t\t\t???????: %f\n",  read_float32(src));

    if (flags_b & 1)
    {

      fprintf(stderr, "\t\t\t\t???????: %f\n",  read_float32(src));
      fprintf(stderr, "\t\t\t\t???????: %f\n",  read_float32(src));

      fprintf(stderr, "\t\t\t\tTexture: ");
      read_string(src, NULL);
      fputc('\n', stderr);

      fprintf(stderr, "\t\t\t\t???????: %f\n",  read_float32(src));

      fprintf(stderr, "\t\t\t\t1?       %lu\n",  read_ui32(src));
      fprintf(stderr, "\t\t\t\t???????: %lu\n",  read_ui32(src));

      /* Note: These labels are complete guesses; they may be wrong! */
      fprintf(stderr, "\t\t\t\tImage Z: %d\n",  fgetc(src));
      fprintf(stderr, "\t\t\t\tImage W: %lu\n",  read_ui32(src));
      fprintf(stderr, "\t\t\t\tImage H: %lu\n",  read_ui32(src));

      fprintf(stderr, "\t\t\t\t???????: %lu\n",  read_ui32(src));
      fprintf(stderr, "\t\t\t\tZero?    %lu\n",  read_ui32(src));
      fprintf(stderr, "\t\t\t\tZero?    %lu\n",  read_ui32(src));
      fprintf(stderr, "\t\t\t\tZero?    %lu\n",  read_ui32(src));

    }

    if (flags_b & 256) { fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src)); }

  }

  num_bbbb = read_ui32(src);
  fprintf(stderr, "\t%lu items in UNKNOWN LIST B...\n", num_bbbb);
  for (j = 0; j < num_bbbb; ++j)
  {
    unsigned long num_others, k;
    fprintf(stderr, "\t\tItem %lu: %lu", j, read_ui32(src));
    num_others = read_ui32(src);
    fprintf(stderr, " (%lu items)\n", num_others);
    for (k = 0; k < num_others; ++k) { fprintf(stderr, "\t\t\t%lu: %lu\n", k, read_ui32(src)); }
  }

  num_cccc = read_ui32(src);
  fprintf(stderr, "\t%lu items in UNKNOWN LIST C...\n", num_cccc);
  for (j = 0; j < num_cccc; ++j)
  {
    fprintf(stderr, "\t\tItem %lu:\n", j);
    fprintf(stderr, "\t\t\t????: %lu:\n", read_ui32(src));
    fprintf(stderr, "\t\t\t????: %lu:\n", read_ui32(src));
    read_xyz(src);
    read_xyz(src);
  }

  num_vertices = read_ui32(src);
  fprintf(stderr, "\t%lu vertices...\n", num_vertices);
  for (j = 0 ; j < num_vertices; ++j)
  {
    fprintf(stderr, "\t\tVertex %lu: ", j);
    read_xyz(src);
  }

  num_polys = read_ui32(src);
  fprintf(stderr, "\t%lu polygons...\n", num_polys);
  for (j = 0; j < num_polys; ++j)
  {
    unsigned long num_points, k;
    long temp;

    fprintf(stderr, "\t\tPolygon %lu:\n", j);

    if (version >= 174)
    {
      fprintf(stderr, "\t\t\tNormal: ");
      read_xyz(src);
      fprintf(stderr, "\t\t\t???????: %f\n",  read_float32(src));
    }

    fprintf(stderr, "\t\t\tTexture: %lu\n", read_ui32(src));

    temp = read_si32(src);
    fprintf(stderr, "\t\t\tIndex A: %ld\n", temp);

    fprintf(stderr, "\t\t\t???????: %lu\n", read_ui32(src));
    fprintf(stderr, "\t\t\t-1?      %ld\n", read_si32(src));
    fprintf(stderr, "\t\t\t-1?      %ld\n", read_si32(src));

    fprintf(stderr, "\t\t\tZero?    %lu\n", read_ui32(src));
    fprintf(stderr, "\t\t\t???????: %lu\n", read_ui32(src));
    fprintf(stderr, "\t\t\t???????: %lu\n", read_ui32(src));
    fprintf(stderr, "\t\t\t???????: %ld\n", read_si32(src));

    num_points = read_ui32(src);
    fprintf(stderr, "\t\t\t%lu points for this face...\n", num_points);
    for (k = 0; k < num_points; ++k)
    {
      fprintf(stderr, "\t\t\t\tPoint %lu:\n", k);

      fprintf(stderr, "\t\t\t\t\tVertex: %lu\n", read_ui32(src));

      if (temp >= 0)
      {
        fprintf(stderr, "\t\t\t\t\t");
        fprintf(stderr, "(%f, ", read_float32(src));/* Texture S? */
        fprintf(stderr, "%f)\n", read_float32(src));/* Texture T? */
      }

      fprintf(stderr, "\t\t\t\t\t");
      fprintf(stderr, "(%f, ", read_float32(src));/* Texture S? */
      fprintf(stderr, "%f)\n", read_float32(src));/* Texture T? */

    }
  }

  num_dddd = read_ui32(src);
  fprintf(stderr, "\t%lu items in UNKNOWN LIST D...\n", num_dddd);
  for (j = 0; j < num_dddd; ++j)
  {

    fprintf(stderr, "\t\t\tItem %lu:\n", j);

    fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));
fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));

    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));
    fprintf(stderr, "\t\t\t\t????: %ld\n", read_si32(src));
fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));
fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));
    fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));
    fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));
fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %f\n", read_float32(src));
    fprintf(stderr, "\t\t\t\t????: %ld\n", read_si32(src));

  }

  num_eeee = read_ui32(src);
  fprintf(stderr, "\t%lu items in UNKNOWN LIST E...\n", num_eeee);
  for (j = 0; j < num_eeee; ++j)
  {
    fprintf(stderr, "\t\t\t%lu: ", j);

    fprintf(stderr, "%lu\n", read_ui32(src));   /* Vertex ID? */
    fprintf(stderr, "(%f, ", read_float32(src));/* Texture S? */
    fprintf(stderr, "%f)\n", read_float32(src));/* Texture T? */
  }

}

int main(int argc, char *argv[])
{
  FILE *src = NULL;
  unsigned long chunk_type, chunk_size, version;

  src = fopen(argv[1], "rb");
  if (!src) { return 1; }

  fseek(src, 0, SEEK_END);
  fprintf(stderr, "%s (%ld bytes)\n\n", argv[1], ftell(src));
  rewind(src);

  /* The first 4 bytes should form a UINT32 value: 0xD4BADA55 */
  version = read_ui32(src);
  if (version != RFL_4CC)
  {
    fprintf(stderr, "Not a valid RFL file!\n");
    fclose(src);
    return 2;
  }

  fprintf(stderr, "D4 = Badass: %08lx\n", version);
  version = read_ui32(src);
  fprintf(stderr, "RFL Version: %lu\n", version); /* RF1: 156/157/158/174/175 */
  fprintf(stderr, "???????????: %f\n",  read_float32(src));  /* ???? */
  fprintf(stderr, "Mat. Offset: %lu\n", read_ui32(src));  /* 0x00070000 offs */
  fprintf(stderr, "Meta Offset: %lu\n", read_ui32(src));  /* 0x01000000 offs */

  if (version >= 174)
  {
    fprintf(stderr, "Num. Chunks: %lu\n",  read_ui32(src)); /* Excluding NULL */
    fprintf(stderr, "Data Length: %lu\n",  read_ui32(src)); /* Sum of lengths */

    fprintf(stderr, "Description: ");
    read_string(src, NULL);
    fputc('\n', stderr);
  }

  do
  {
    const unsigned long start_of_chunk = (unsigned long)ftell(src);

    chunk_type = read_ui32(src);
    chunk_size = read_ui32(src);

    fprintf(stderr, "\n%08lx (%lu bytes)\n", chunk_type, chunk_size);

    switch (chunk_type)
    {
      case RFL_CHUNK_TGAS:
      case RFL_CHUNK_V3DS:
      case RFL_CHUNK_VCMS:
      case RFL_CHUNK_VFXS:
      case RFL_CHUNK_MVFS:
      {
        unsigned long count, i;

        if      (chunk_type == RFL_CHUNK_TGAS) { fprintf(stderr, "TGAS"); }
        else if (chunk_type == RFL_CHUNK_V3DS) { fprintf(stderr, "V3DS"); }
        else if (chunk_type == RFL_CHUNK_VCMS) { fprintf(stderr, "VCMS"); }
        else if (chunk_type == RFL_CHUNK_VFXS) { fprintf(stderr, "VFXS"); }
        else if (chunk_type == RFL_CHUNK_MVFS) { fprintf(stderr, "MVFS"); }

        count = read_ui32(src);
        fprintf(stderr, " (%lu items)\n", count);
#ifdef TELL_ME_EVERYTHING
        for (i = 0; i < count; ++i)
        {
          fprintf(stderr, "\tFile %lu = ", i);
          read_string(src, NULL);
          fputc('\n', stderr);
        }

        if (chunk_type != RFL_CHUNK_TGAS)
        {
          for (i = 0; i < count; ++i)
          {
            fprintf(stderr, "\tFlag %lu = %lu\n", i, read_ui32(src));
          }
        }
#endif
      }
      break;

      case RFL_CHUNK_ROCK:
      {
        unsigned short i;
        fprintf(stderr, "ROCK\n\tFile: ");
        read_string(src, NULL);
        fprintf(stderr, "\n\tData: ");
        for (i = 0; i < 21; ++i)
        { fprintf(stderr, "%02x%c", fgetc(src), i == 20 ? '\n' : ' '); }
      }
      break;

      case RFL_CHUNK_ZERO:
        fprintf(stderr, "ZERO (");
        fprintf(stderr, "%lu ", read_ui32(src));  /* Always 0. May be float? */
        fprintf(stderr, "%lu ", read_ui32(src));  /* Always 0. May be float? */
        fprintf(stderr, "%lu)", read_ui32(src));  /* Always 0. May be float? */
        fputc('\n', stderr);
      break;

      case RFL_CHUNK_MATRIX:
      {
        fprintf(stderr, "MATRIX\n");
        read_xyz(src);
        read_matrix(src);
      }
      break;

      case RFL_CHUNK_LIST2:
      /* Only used in L6S3.rfl/ L7S4.rfl / L14S3.rfl / L17S4.rfl / L20S2.rfl */
      case RFL_CHUNK_LIST1:
      {
        unsigned long num_items, i;

        if      (chunk_type == RFL_CHUNK_LIST1) { fprintf(stderr, "LIST #1"); }
        else if (chunk_type == RFL_CHUNK_LIST2) { fprintf(stderr, "LIST #2"); }

        num_items = read_ui32(src);
        fprintf(stderr, " (%lu items)\n", num_items);
#ifdef TELL_ME_EVERYTHING
        for (i = 0; i < num_items; ++i)
        {
          unsigned long num_sub_items, j;
          fprintf(stderr, "\tItem %lu = ", i);
          read_string(src, NULL);
          num_sub_items = read_ui32(src);
          fprintf(stderr, " (%lu sub-items)\n", num_sub_items);
          for (j = 0; j < num_sub_items; ++j)
          {
            fprintf(stderr, "\t\t%lu = %lu\n", j, read_ui32(src));
          }
        }
#endif
      }
      break;

      case RFL_CHUNK_MATRIX_LIST:
      {
        unsigned long num_items, i;

        num_items = read_ui32(src);
        fprintf(stderr, "MATRIX LIST (%lu items)\n", num_items);
#ifdef TELL_ME_EVERYTHING
        /* This may not be completely correct... */
        for (i = 0; i < num_items; ++i)
        {
          fprintf(stderr, "\tItem %lu ", i);
          fprintf(stderr, "(%08lx",    read_ui32(src));
          fprintf(stderr, " %08lx)\n", read_ui32(src));

          read_xyz(src);
          read_matrix(src);
          read_xyz(src);
        }
#endif
      }
      break;

      case RFL_CHUNK_TRIGGER:
      {
        unsigned long num_items, i;

        num_items = read_ui32(src);
        fprintf(stderr, "TRIGGERS (%lu items)\n", num_items);
#ifdef TELL_ME_EVERYTHING
        for (i = 0; i < num_items; ++i)
        {
          char kind[512];
          unsigned long num_unknowns, j;

          fprintf(stderr, "\tItem %lu (%lu)\t", i, read_ui32(src));
          read_string(src, kind);

          fprintf(stderr, "\n\t\tX/Y/Z:\t%f", read_float32(src));
          fprintf(stderr, " %f ", read_float32(src));

          fprintf(stderr, "%f\n\t\tAbout:\t", read_float32(src));
          read_string(src, NULL);

          /* May be zero or one; doesn't seem to affect anything... */
          fprintf(stderr, "\n\t\t?????:\t%d\n", fgetc(src));

          fprintf(stderr, "\t\t?????:\t%f\n", read_float32(src));

          /* May be zero or one; don't seem to affect anything... */
          fprintf(stderr, "\t\t?????:\t");
          for (j = 0; j < 6; ++j)
          { fprintf(stderr, "%02x%c", fgetc(src), j == 5 ? '\n' : ' '); }

          fprintf(stderr, "\t\t?????:\t%lu\n", read_ui32(src));

          fprintf(stderr, "\t\tFloatA:\t%f\n", read_float32(src));
          fprintf(stderr, "\t\tFloatB:\t%f\n", read_float32(src));

          fprintf(stderr, "\t\tParamA:\t");
          read_string(src, NULL);
          fprintf(stderr, "\n");

          fprintf(stderr, "\t\tParamB:\t");
          read_string(src, NULL);
          fprintf(stderr, "\n");

          num_unknowns = read_ui32(src);
          fprintf(stderr, "\t\tCount:\t%lu\n", num_unknowns);

          for (j = 0; j < num_unknowns; ++j)
          { fprintf(stderr, "\t\t\tUNKNOWN %lu:\t%lu\n", j, read_ui32(src)); }

          if (strcmp(kind, "Teleport") == 0 || strcmp(kind, "Teleport_Player") == 0 ||
              strcmp(kind, "Alarm") == 0 || strcmp(kind, "Play_Vclip") == 0)
          { read_matrix(src); }

        }
 #endif
      }
      break;

      case RFL_CHUNK_META:
      {
        int i = 0;

        fprintf(stderr, "META (%lu)\n", read_ui32(src));

        fprintf(stderr, "\tName: ");
        read_string(src, NULL);
        fprintf(stderr, "\n");

        fprintf(stderr, "\tAuthors: ");
        read_string(src, NULL);
        fprintf(stderr, "\n");

        fprintf(stderr, "\tDate: ");
        read_string(src, NULL);
        fprintf(stderr, "\n");
#ifdef TELL_ME_EVERYTHING
        fprintf(stderr, "\tFlagA: %d\n", fgetc(src));
        if (version >= 174) { fprintf(stderr, "\tFlagB: %d\n", fgetc(src)); }

        for (i = 0; i < 4; ++i)
        {
          if (i < 3) { fprintf(stderr, "\t????: %lu\n", read_ui32(src)); }
          fprintf(stderr, "\t????: %f\n", read_float32(src));
          read_xyz(src);
          read_matrix(src);
        }
#endif
      }
      break;

      /* I think that this is the actual "model" chunk... */
      case RFL_CHUNK_MESH_LIST:
      {
        unsigned long num_items, i;

        num_items = read_ui32(src);
        fprintf(stderr, "MESH LIST (%lu items)\n", num_items);
#ifdef TELL_ME_EVERYTHING
        for (i = 0; i < num_items; ++i)
        {
          fprintf(stderr, "\tItem %lu (%lu)\n", i, read_ui32(src));
          fprintf(stderr, "\t\t");
          read_xyz(src);
          read_matrix(src);

          describe_group(src, version);

          fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));
          fprintf(stderr, "\t\t\t\t????: %ld\n", read_si32(src));
          fprintf(stderr, "\t\t\t\t????: %lu\n", read_ui32(src));

        }
#endif
      }
      break;

      case RFL_CHUNK_MESH:
        fprintf(stderr, "MESH\n");
#if 1
        describe_group(src, version);
#endif
      break;

      case RFL_CHUNK_NULL:
        fprintf(stderr, "NULL\n");
      break;

      default:
        fseek(src, chunk_size, SEEK_CUR);
      break;
    }

    fseek(src, start_of_chunk + 8 + chunk_size, SEEK_SET);
  } while (chunk_type != 0);

  fclose(src);
  return 0;
}
