Test Cases
==========

This directory contains test-cases for mesh/animation/material/light import.

Note that not all formats supported by LoadGameMesh can have test cases in this
directory; models for partially reverse-engineered formats (such as Red Faction
RFL) can only really be tested with the original (copyright-protected) files.
