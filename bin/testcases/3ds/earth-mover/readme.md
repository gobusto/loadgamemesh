# Earth Mover
## Cuzco
### CC0 (Public Domain)
<https://opengameart.org/content/earth-mover>

Earth movers do exactly as their name suggest; they move
dirt/rocks/trees/whatever. They are designed to lessen the number of loads
needed, therby speeding up the construction process.

There are no textures needed with this model, as it is colored with 9 different
hues. There are three formats available, .x, .3ds and .obj  (the 7z dowwnload
has all three formats).

The model is not rigged, but it should be fairly easy to rig it.

Enjoy.
