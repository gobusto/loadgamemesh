# Toy helipcopter
## Thomas Glyn Dennis
### CC0 (Public Domain)
<https://opengameart.org/content/cartoon-helicopter>

A very-low-poly helicopter, complete with spinning. I've included the MS3D work
files in addition to the final MD3 version.
