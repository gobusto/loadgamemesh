# Ragdoll
## Thomas Glyn Dennis
### CC0 (Public Domain)
<https://gitlab.com/gobusto/rbembed>

A basic skeletal mesh for testing ragdoll physics in game engines.
