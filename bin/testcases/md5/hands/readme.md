# Playerhands
## sandsound
### CC0 (Public Domain)
<https://opengameart.org/content/playerhands>

Basic playerhand with bones and a simple animation.

I have attached .blend. md5-mesh/anim and md5.cfg

UPDATE:

I have uploaded a new version, build from scratch, and this time it also works
in MD5 :-)
