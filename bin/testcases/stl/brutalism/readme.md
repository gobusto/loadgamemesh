# Brutalist architecture models
## claudeb
### CC0 (Public Domain)
<http://opengameart.org/content/3rd-person-blender-sci-fi-pack>

The popularity of reinforced concrete as a construction material peaked in the
sixties and seventies, coinciding with a massive development boom all over
Europe -- on both sides of the Iron Curtain. This gave rise to Brutalism, a late
offshoot of the Modernist style that made interwar architecture so distinct.
Criticized by many, especially in Eastern Europe where it's associated with the
Communist disdain for humanity, Brutalist buildings nevertheless have a
distinctive personality that makes them as memorable as they are photogenic.

In an attempt to recapture some of that magic, I created a collection of 3D
models depicting buildings from various categories, but all in the same style.
They are untextured; they shouldn't need texture -- that's part of the point
with Brutalism. They're also stylized, to capture the essence without too much
effort. Imperfect, too, but hopefully you'll find them useful anyway.

Enjoy, and thank you.
