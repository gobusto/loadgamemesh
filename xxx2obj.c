/**
@file
@brief A simple xxx-to-OBJ conversion program.

This is not part of the actual mesh loading code; it's for testing purposes.
*/

#include <stdio.h>
#include <stdlib.h>
#include "lgm/mesh/mesh.h"

/**
@brief The program entry point.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int main(int argc, char **argv)
{
  mesh_s *mesh = NULL;
  int status = 0;

  /* The input and output mesh file paths must be provided as a minimum. */
  if (argc < 3 || argc > 5)
  { printf("Usage: xxx2obj input.md2 output.obj [mtlfile.mtl] [animinfo.cfg]\n"); return 1; }

  /* Attempt to load the input mesh file. */
  if (!(mesh = meshLoadFile(argv[1])))
  { printf("ERROR: Could not load %s\n", argv[1]); return 2; }

  /* Save the first frame as a .OBJ file, and optionally create a .MTL too. */
  if (!meshSaveOBJ(mesh, 0, argv[2], argc >= 4 ? argv[3] : NULL))
  { printf("ERROR: Could not save %s\n", argv[2]); status = 3; }

  /* If requested, dump a list of animation data to a Quake III .CFG file. */
  if (argc >= 5)
  {
    wordtree_s *anim_set = animFromMesh(mesh, 30);

    if (animSetSaveCFG(anim_set, argv[4]) < 1)
    { printf("ERROR: Could not save %s\n", argv[4]); status = 4; }

    animSetDelete(anim_set);
  }

  /* Free the mesh file data and end the program. */
  meshDelete(mesh);
  return status;
}
